/* 
 #Class name: About
 #Class description: shows the details of the app
 */

package com.myanmar.travel.planner;

import java.io.File;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;

public class About extends Activity {

	ActionBar mActionBar;
	ProgressDialog dialog;
	File file;
	DatabaseMain databaseMain;
	Agency agency;
	Calender calender;
	Routes routes;
	StopTimes stopTimes;
	Stops stops;
	Trips trips;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);

		mActionBar = getActionBar();
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle(getResources().getString(R.string.about));
		databaseMain = new DatabaseMain(getApplicationContext());
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		int itemId = item.getItemId();
		switch (itemId) {
		case android.R.id.home:
			onBackPressed();
			break;
		}
		return true;
	}
}
