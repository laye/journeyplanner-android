/* 
 #Class name: Agency
 #Class description: class to hold agency_item
 */
package com.myanmar.travel.planner;

public class Agency {
	String agency_id;
	String agency_name;
	String agency_url;
	String agency_timezone;
	String agency_lang;
	String agency_phone;
	String agency_fare_url;
	String agency_email;
	String agency_address;

	public Agency(String agency_id, String agency_name, String agency_url,
			String agency_timezone, String agency_lang, String agency_phone,
			String agency_fare_url, String agency_email, String agency_address) {
		this.agency_id = agency_id;
		this.agency_name = agency_name;
		this.agency_url = agency_url;
		this.agency_timezone = agency_timezone;
		this.agency_lang = agency_lang;
		this.agency_phone = agency_phone;
		this.agency_fare_url = agency_fare_url;
		this.agency_email = agency_email;
		this.agency_address = agency_address;
	}

	public Agency() {
	}

	public String getAgency_id() {
		return agency_id;
	}

	public void setAgency_id(String agency_id) {
		this.agency_id = agency_id;
	}

	public String getAgency_name() {
		return this.agency_name;
	}

	public void setAgency_name(String agency_name) {
		this.agency_name = agency_name;
	}

	public String getAgency_url() {
		return this.agency_url;
	}

	public void setAgency_url(String agency_url) {
		this.agency_url = agency_url;
	}

	public String getAgency_timezone() {
		return this.agency_timezone;
	}

	public void setAgency_timezone(String agency_timezone) {
		this.agency_timezone = agency_timezone;
	}

	public String getAgency_lang() {
		return this.agency_lang;
	}

	public void setAgency_lang(String agency_lang) {
		this.agency_lang = agency_lang;
	}

	public String getAgency_phone() {
		return agency_phone;
	}

	public void setAgency_phone(String agency_phone) {
		this.agency_phone = agency_phone;
	}

	public String getAgency_fare_url() {
		return agency_fare_url;
	}

	public void setAgency_fare_url(String agency_fare_url) {
		this.agency_fare_url = agency_fare_url;
	}

	public String getAgency_email() {
		return agency_email;
	}

	public void setAgency_email(String agency_email) {
		this.agency_email = agency_email;
	}

	public String getAgency_address() {
		return agency_address;
	}

	public void setAgency_address(String agency_address) {
		this.agency_address = agency_address;
	}
}
