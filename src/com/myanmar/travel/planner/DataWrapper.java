/* 
 #Class name: DataWrapper
 #Class description: class to serialize the dataList
 */

package com.myanmar.travel.planner;

import java.io.Serializable;
import java.util.ArrayList;

public class DataWrapper implements Serializable {
	private static final long serialVersionUID = 1L;
	private ArrayList<RouteList> routeList;

	public DataWrapper(ArrayList<RouteList> data) {
		this.routeList = data;
	}

	public ArrayList<RouteList> getList() {
		return this.routeList;
	}

}
