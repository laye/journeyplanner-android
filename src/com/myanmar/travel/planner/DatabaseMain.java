package com.myanmar.travel.planner;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

public class DatabaseMain extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 5;
	private static final String DATABASE_NAME = "myanmardatabaseinfo302";

	private static final String TABLE_AGENCY = "agency";
	private static final String TABLE_ROUTES = "routes";
	private static final String TABLE_STOPS = "stops";
	private static final String TABLE_CALENDER = "calender";
	private static final String TABLE_STOP_TIMES = "stop_times";
	private static final String TABLE_TRIPS = "trips";
	private static final String TABLE_FAV_STOPS = "fav";

	private static final String KEY_AGENCY_ID = "agency_id";
	private static final String KEY_AGENCY_NAME = "agency_name";
	private static final String KEY_AGENCY_URL = "agency_url";
	private static final String KEY_AGENCY_TIMEZONE = "agency_timezone";
	private static final String KEY_AGENCY_LANG = "agency_lang";
	private static final String KEY_AGENCY_PHONE = "agency_phone";
	private static final String KEY_AGENCY_FARE_URL = "agency_fare_url";
	private static final String KEY_AGENCY_EMAIL = "agency_email";
	private static final String KEY_AGENCY_ADDRESS = "agency_address";

	private static final String KEY_ROUTE_ID = "route_id";
	private static final String KEY_ROUTE_SHORT_NAME = "route_short_name";
	private static final String KEY_ROUTE_LONG_NAME = "route_long_name";
	private static final String KEY_ROUTE_DESC = "route_desc";
	private static final String KEY_ROUTE_TYPE = "route_type";
	private static final String KEY_ROUTE_URL = "route_url";
	private static final String KEY_ROUTE_COLOR = "route_color";
	private static final String KEY_ROUTE_TEXT_COLOR = "route_text_color";

	private static final String KEY_STOP_ID = "stop_id";
	private static final String KEY_STOP_CODE = "stop_code";
	private static final String KEY_STOP_NAME = "stop_name";
	private static final String KEY_STOP_DESC = "stop_desc";
	private static final String KEY_STOP_LAT = "stop_lat";
	private static final String KEY_STOP_LON = "stop_lon";
	private static final String KEY_STOP_URL = "stop_url";
	private static final String KEY_LOCATION_TYPE = "locatopn_type";
	private static final String KEY_PARENT_STATION = "parent_station";
	private static final String KEY_ZONE_ID = "zone_id";
	private static final String KEY_STOP_TIMEZONE = "stop_timezone";
	private static final String KEY_WHEELCHAIR_BOARDING = "wheelchair_boarding";

	private static final String KEY_SERVICE_ID = "service_id";
	private static final String KEY_MONDAY = "monday";
	private static final String KEY_TUESDAY = "tuesday";
	private static final String KEY_WEDNESDAY = "wednesday";
	private static final String KEY_THURSDAY = "thursday";
	private static final String KEY_FRIDAY = "friday";
	private static final String KEY_SATAURDAY = "saturday";
	private static final String KEY_SUNDAY = "sunday";
	private static final String KEY_START_DATE = "start_date";
	private static final String KEY_END_DATE = "end_date";

	private static final String KEY_TRIP_ID = "trip_id";
	private static final String KEY_ARRIVAL_TIME = "arrival_time";
	private static final String KEY_DEPARTURE_TIME = "departure_time";
	private static final String KEY_STOP_SEQUENCE = "stop_sequence";
	private static final String KEY_STOP_HEADSIGN = "stop_headsign";
	private static final String KEY_PICKUP_TYPE = "pickup_type";
	private static final String KEY_DROP_OFF_TYPE = "drop_off_type";
	private static final String KEY_SHAPE_DIST_TRAVELED = "shape_dist_traveled";

	private static final String KEY_TRIP_HEADSIGN = "trip_headsign";
	private static final String KEY_DIRECTION_ID = "direction_id";

	private static final String KEY_FAV_ID = "fav_id";
	private static final String KEY_SOURCE_STOP_ID = "fav_source_stop_id";
	private static final String KEY_DESTINATION_STOP_ID = "fav_destination_stop_id";
	private static final String KEY_SOURCE_STOP_NAME = "fav_source_stop_name";
	private static final String KEY_DESTINATION_STOP_NAME = "fav_destination_stop_name";
	private static final String KEY_SOURCE_DEPARTURE_TIME = "fav_source_departure_time";
	private static final String KEY_DESTINATION_ARRIVAL_TIME = "fav_destination_arrival_time";
	private static final String KEY_SOURCE_STOP_SEQ = "fav_source_stop_sequence";
	private static final String KEY_DESTINATION_STOP_SEQ = "fav_destination_stop_sequence";
	private static final String KEY_IS_AFTER_DATE = "is_after_date";
	private static final String KEY_DATE = "fav_date";
	ArrayList<StopTimes> dataList;
	ArrayList<StopTimes> dataListCopy;
	Context context;

	public DatabaseMain(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		/* create tables */
		String CREATE_USER_TABLE_AGENCY = "CREATE TABLE " + TABLE_AGENCY + "("
				+ KEY_AGENCY_ID + " TEXT NOT NULL, " + KEY_AGENCY_NAME
				+ " TEXT NOT NULL, " + KEY_AGENCY_URL + " TEXT NOT NULL, "
				+ KEY_AGENCY_TIMEZONE + " TEXT NOT NULL, " + KEY_AGENCY_LANG
				+ " TEXT NOT NULL, " + KEY_AGENCY_PHONE + " TEXT NOT NULL, "
				+ KEY_AGENCY_FARE_URL + " TEXT NOT NULL, " + KEY_AGENCY_EMAIL
				+ " TEXT NOT NULL, " + KEY_AGENCY_ADDRESS + " TEXT NOT NULL"
				+ ")";
		db.execSQL(CREATE_USER_TABLE_AGENCY);

		String CREATE_USER_TABLE_ROUTES = "CREATE TABLE " + TABLE_ROUTES + "("
				+ KEY_ROUTE_ID + " TEXT NOT NULL ," + KEY_AGENCY_ID
				+ " TEXT NOT NULL ," + KEY_ROUTE_SHORT_NAME
				+ " TEXT NOT NULL, " + KEY_ROUTE_LONG_NAME + " TEXT NOT NULL, "
				+ KEY_ROUTE_DESC + " TEXT NOT NULL, " + KEY_ROUTE_TYPE
				+ " INTEGER NOT NULL, " + KEY_ROUTE_URL + " TEXT NOT NULL, "
				+ KEY_ROUTE_COLOR + " TEXT NOT NULL, " + KEY_ROUTE_TEXT_COLOR
				+ " TEXT NOT NULL, " + "FOREIGN KEY (" + KEY_ROUTE_ID
				+ ") REFERENCES " + TABLE_AGENCY + "(" + KEY_ROUTE_ID + ")"
				+ ")";
		db.execSQL(CREATE_USER_TABLE_ROUTES);

		String CREATE_USER_TABLE_STOPS = "CREATE TABLE " + TABLE_STOPS + "("
				+ KEY_STOP_ID + " TEXT NOT NULL, " + KEY_STOP_CODE + " TEXT, "
				+ KEY_STOP_NAME + " TEXT, " + KEY_STOP_DESC + " TEXT, "
				+ KEY_STOP_LAT + " REAL, " + KEY_STOP_LON + " REAL, "
				+ KEY_ZONE_ID + " TEXT, " + KEY_STOP_URL + " TEXT, "
				+ KEY_LOCATION_TYPE + " TEXT, " + KEY_PARENT_STATION
				+ " TEXT, " + KEY_STOP_TIMEZONE + " TEXT, "
				+ KEY_WHEELCHAIR_BOARDING + " TEXT" + ")";
		// Log.d(TABLE_STOPS + "created", TABLE_STOPS);
		db.execSQL(CREATE_USER_TABLE_STOPS);

		String CREATE_USER_TABLE_CALENDER = "CREATE TABLE " + TABLE_CALENDER
				+ "(" + KEY_SERVICE_ID + " TEXT NOT NULL, " + KEY_MONDAY
				+ " INTEGER, " + KEY_TUESDAY + " INTEGER, " + KEY_WEDNESDAY
				+ " INTEGER, " + KEY_THURSDAY + " INTEGER, " + KEY_FRIDAY
				+ " INTEGER, " + KEY_SATAURDAY + " INTEGER, " + KEY_SUNDAY
				+ " INTEGER, " + KEY_START_DATE + " TEXT NOT NULL, "
				+ KEY_END_DATE + " TEXT NOT NULL" + ")";
		db.execSQL(CREATE_USER_TABLE_CALENDER);

		String CREATE_USER_TABLE_STOP_TIMES = "CREATE TABLE "
				+ TABLE_STOP_TIMES + "(" + KEY_TRIP_ID + " TEXT NOT NULL, "
				+ KEY_ARRIVAL_TIME + " TEXT, " + KEY_DEPARTURE_TIME + " TEXT, "
				+ KEY_STOP_ID + " TEXT, " + KEY_STOP_SEQUENCE + " TEXT,"
				+ KEY_STOP_HEADSIGN + " TEXT," + KEY_PICKUP_TYPE + " TEXT,"
				+ KEY_DROP_OFF_TYPE + " TEXT," + KEY_SHAPE_DIST_TRAVELED
				+ " TEXT," + "FOREIGN KEY (" + KEY_STOP_ID + ") REFERENCES "
				+ TABLE_STOPS + "(" + KEY_STOP_ID + ")" + ")";
		db.execSQL(CREATE_USER_TABLE_STOP_TIMES);

		String CREATE_USER_TABLE_TRIPS = "CREATE TABLE " + TABLE_TRIPS + "("
				+ KEY_ROUTE_ID + " TEXT NOT NULL, " + KEY_SERVICE_ID
				+ " TEXT, " + KEY_TRIP_ID + " TEXT, " + KEY_TRIP_HEADSIGN
				+ " TEXT, " + KEY_DIRECTION_ID + " TEXT" + ")";
		db.execSQL(CREATE_USER_TABLE_TRIPS);

		String CREATE_FAV_TABLE_STOPS = "CREATE TABLE " + TABLE_FAV_STOPS + "("
				+ KEY_FAV_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
				+ KEY_SOURCE_STOP_ID + " TEXT NOT NULL, "
				+ KEY_DESTINATION_STOP_ID + " TEXT NOT NULL, "
				+ KEY_SOURCE_STOP_NAME + " TEXT NOT NULL, "
				+ KEY_DESTINATION_STOP_NAME + " TEXT NOT NULL, "
				+ KEY_SOURCE_DEPARTURE_TIME + " TEXT NOT NULL, "
				+ KEY_DESTINATION_ARRIVAL_TIME + " TEXT NOT NULL, "
				+ KEY_SERVICE_ID + " TEXT NOT NULL, " + KEY_TRIP_ID
				+ " TEXT NOT NULL, " + KEY_SOURCE_STOP_SEQ + " TEXT NOT NULL, "
				+ KEY_DESTINATION_STOP_SEQ + " TEXT NOT NULL, "
				+ KEY_IS_AFTER_DATE + " INTEGER , " + KEY_DATE
				+ " TEXT NOT NULL" + ")";
		try {
			db.execSQL(CREATE_FAV_TABLE_STOPS);
		} catch (SQLException e) {
		}
		// Log.d(TABLE_FAV_STOPS + "created", TABLE_FAV_STOPS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		/* upgrade tables */
		if (newVersion > oldVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_AGENCY);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROUTES);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_STOPS);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_CALENDER);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_STOP_TIMES);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRIPS);
		}
		// db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAV_STOPS);
		onCreate(db);
	}

	void addAgency(Agency agency) {
		/* add agency */
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_AGENCY_ID, agency.getAgency_id());
		values.put(KEY_AGENCY_NAME, agency.getAgency_name());
		values.put(KEY_AGENCY_URL, agency.getAgency_url());
		values.put(KEY_AGENCY_TIMEZONE, agency.getAgency_timezone());
		values.put(KEY_AGENCY_LANG, agency.getAgency_lang());
		values.put(KEY_AGENCY_PHONE, agency.getAgency_phone());
		values.put(KEY_AGENCY_FARE_URL, agency.getAgency_fare_url());
		values.put(KEY_AGENCY_EMAIL, agency.getAgency_email());
		values.put(KEY_AGENCY_ADDRESS, agency.getAgency_address());
		db.insert(TABLE_AGENCY, null, values);

	}

	public void addAllAgency(ArrayList<Agency> agencyList) {
		/* add agency */
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "INSERT INTO " + TABLE_AGENCY + "(" + KEY_AGENCY_ID
				+ " , " + KEY_AGENCY_NAME + " , " + KEY_AGENCY_URL + " , "
				+ KEY_AGENCY_TIMEZONE + " , " + KEY_AGENCY_LANG + " , "
				+ KEY_AGENCY_PHONE + " , " + KEY_AGENCY_FARE_URL + " , "
				+ KEY_AGENCY_EMAIL + " , " + KEY_AGENCY_ADDRESS
				+ " ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		db.beginTransaction();
		SQLiteStatement stmt = db.compileStatement(sql);
		for (int i = 0; i < agencyList.size(); i++) {
			stmt.bindString(1, agencyList.get(i).getAgency_id());
			stmt.bindString(2, agencyList.get(i).getAgency_name());
			stmt.bindString(3, agencyList.get(i).getAgency_url());
			stmt.bindString(4, agencyList.get(i).getAgency_timezone());
			stmt.bindString(5, agencyList.get(i).getAgency_lang());
			stmt.bindString(6, agencyList.get(i).getAgency_phone());
			stmt.bindString(7, agencyList.get(i).getAgency_fare_url());
			stmt.bindString(8, agencyList.get(i).getAgency_email());
			stmt.bindString(9, agencyList.get(i).getAgency_address());
			stmt.execute();
			stmt.clearBindings();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		db.close();
	}

	public ArrayList<Agency> getAllAgency() {
		/* get all agencies */
		ArrayList<Agency> dataList = new ArrayList<Agency>();
		String selectQuery = "SELECT  * FROM " + TABLE_AGENCY + " ORDER BY "
				+ KEY_AGENCY_NAME;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Agency agency = new Agency();
				agency.setAgency_id(cursor.getString(0));
				agency.setAgency_name(cursor.getString(1));
				agency.setAgency_url(cursor.getString(2));
				agency.setAgency_timezone(cursor.getString(3));
				agency.setAgency_lang(cursor.getString(4));
				agency.setAgency_phone(cursor.getString(5));
				agency.setAgency_fare_url(cursor.getString(6));
				agency.setAgency_email(cursor.getString(7));
				agency.setAgency_address(cursor.getString(8));
				dataList.add(agency);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		return dataList;
	}

	void addRoutes(Routes routes) {
		/* add routes */
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_AGENCY_ID, routes.getAgency_id());
		values.put(KEY_ROUTE_ID, routes.getRoute_id());
		values.put(KEY_ROUTE_SHORT_NAME, routes.getRoute_short_name());
		values.put(KEY_ROUTE_LONG_NAME, routes.getRoute_long_name());
		values.put(KEY_ROUTE_DESC, routes.getRoute_desc());
		values.put(KEY_ROUTE_TYPE, routes.getRoute_type());
		values.put(KEY_ROUTE_URL, routes.getRoute_url());
		values.put(KEY_ROUTE_COLOR, routes.getRoute_color());
		values.put(KEY_ROUTE_TEXT_COLOR, routes.getRoute_text_color());
		db.insert(TABLE_ROUTES, null, values);
		db.close();
	}

	public void addAllRoutes(ArrayList<Routes> routesList) {
		/* add routes */
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "INSERT INTO " + TABLE_ROUTES + "(" + KEY_AGENCY_ID
				+ " , " + KEY_ROUTE_ID + " , " + KEY_ROUTE_SHORT_NAME + " , "
				+ KEY_ROUTE_LONG_NAME + " , " + KEY_ROUTE_DESC + " , "
				+ KEY_ROUTE_TYPE + " , " + KEY_ROUTE_URL + " , "
				+ KEY_ROUTE_COLOR + " , " + KEY_ROUTE_TEXT_COLOR
				+ " ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		db.beginTransaction();
		SQLiteStatement stmt = db.compileStatement(sql);
		for (int i = 0; i < routesList.size(); i++) {
			stmt.bindString(1, routesList.get(i).getAgency_id());
			stmt.bindString(2, routesList.get(i).getRoute_id());
			stmt.bindString(3, routesList.get(i).getRoute_short_name());
			stmt.bindString(4, routesList.get(i).getRoute_long_name());
			stmt.bindString(5, routesList.get(i).getRoute_desc());
			stmt.bindLong(6, routesList.get(i).getRoute_type());
			stmt.bindString(7, routesList.get(i).getRoute_url());
			stmt.bindString(8, routesList.get(i).getRoute_color());
			stmt.bindString(9, routesList.get(i).getRoute_text_color());
			stmt.execute();
			stmt.clearBindings();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		db.close();
	}

	public ArrayList<Routes> getAllRoutes() {
		/* get all routes */
		ArrayList<Routes> dataList = new ArrayList<Routes>();
		String selectQuery = "SELECT  * FROM " + TABLE_ROUTES;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Routes routes = new Routes();
				routes.setRoute_id(cursor.getString(0));
				routes.setAgency_id(cursor.getString(1));
				routes.setRoute_short_name(cursor.getString(2));
				routes.setRoute_long_name(cursor.getString(3));
				routes.setRoute_desc(cursor.getString(4));
				routes.setRoute_type(cursor.getInt(5));
				routes.setRoute_url(cursor.getString(6));
				routes.setRoute_color(cursor.getString(7));
				routes.setRoute_text_color(cursor.getString(8));
				dataList.add(routes);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		return dataList;
	}

	void addStops(Stops stops) {
		/* add stops */
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_STOP_ID, stops.getStop_id());
		values.put(KEY_STOP_CODE, stops.getStop_code());
		values.put(KEY_STOP_NAME, stops.getStop_name());
		values.put(KEY_STOP_DESC, stops.getStop_desc());
		values.put(KEY_STOP_LAT, stops.getStop_lat());
		values.put(KEY_STOP_LON, stops.getStop_lon());
		values.put(KEY_ZONE_ID, stops.getZone_id());
		values.put(KEY_STOP_URL, stops.getStop_url());
		values.put(KEY_LOCATION_TYPE, stops.getLocation_type());
		values.put(KEY_PARENT_STATION, stops.getParent_station());
		values.put(KEY_STOP_TIMEZONE, stops.getStop_timezone());
		values.put(KEY_WHEELCHAIR_BOARDING, stops.getWheelchair_boarding());
		db.insert(TABLE_STOPS, null, values);

		db.close();
	}

	public void addAllStops(ArrayList<Stops> stopsList) {
		/* add stops */
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "INSERT INTO " + TABLE_STOPS + "(" + KEY_STOP_ID + " , "
				+ KEY_STOP_CODE + " , " + KEY_STOP_NAME + " , " + KEY_STOP_DESC
				+ " , " + KEY_STOP_LAT + " , " + KEY_STOP_LON + " , "
				+ KEY_ZONE_ID + " , " + KEY_STOP_URL + " , "
				+ KEY_LOCATION_TYPE + " , " + KEY_PARENT_STATION + " , "
				+ KEY_STOP_TIMEZONE + " , " + KEY_WHEELCHAIR_BOARDING
				+ " ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		db.beginTransaction();
		SQLiteStatement stmt = db.compileStatement(sql);
		for (int i = 0; i < stopsList.size(); i++) {
			stmt.bindString(1, stopsList.get(i).getStop_id());
			stmt.bindString(2, stopsList.get(i).getStop_code());
			stmt.bindString(3, stopsList.get(i).getStop_name());
			stmt.bindString(4, stopsList.get(i).getStop_desc());
			stmt.bindDouble(5, stopsList.get(i).getStop_lat());
			stmt.bindDouble(6, stopsList.get(i).getStop_lon());
			stmt.bindString(7, stopsList.get(i).getZone_id());
			stmt.bindString(8, stopsList.get(i).getStop_url());
			stmt.bindLong(9, stopsList.get(i).getLocation_type());
			stmt.bindString(10, stopsList.get(i).getParent_station());
			stmt.bindString(11, stopsList.get(i).getStop_timezone());
			stmt.bindString(12, stopsList.get(i).getWheelchair_boarding());
			stmt.execute();
			stmt.clearBindings();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		db.close();
	}

	public ArrayList<Stops> getAllStops() {
		/* get all stops */
		ArrayList<Stops> dataList = new ArrayList<Stops>();
		String selectQuery = "SELECT * FROM " + TABLE_STOPS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Stops stops = new Stops();
				stops.setStop_id(cursor.getString(0));
				stops.setStop_code(cursor.getString(1));
				stops.setStop_name(cursor.getString(2));
				stops.setStop_desc(cursor.getString(3));
				stops.setStop_lat(cursor.getFloat(4));
				stops.setStop_lon(cursor.getFloat(5));
				stops.setZone_id(cursor.getString(6));
				stops.setStop_url(cursor.getString(7));
				stops.setLocation_type(cursor.getInt(8));
				stops.setParent_station(cursor.getString(9));
				stops.setStop_timezone(cursor.getString(10));
				stops.setWheelchair_boarding(cursor.getString(11));
				dataList.add(stops);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		return dataList;
	}

	void addCalender(Calender calender) {
		/* add calender */
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_SERVICE_ID, calender.getService_id());
		values.put(KEY_MONDAY, calender.getMonday());
		values.put(KEY_TUESDAY, calender.getTuesday());
		values.put(KEY_WEDNESDAY, calender.getWednesday());
		values.put(KEY_THURSDAY, calender.getThursday());
		values.put(KEY_FRIDAY, calender.getFriday());
		values.put(KEY_SATAURDAY, calender.getSaturday());
		values.put(KEY_SUNDAY, calender.getSunday());
		values.put(KEY_START_DATE, calender.getStart_date());
		values.put(KEY_END_DATE, calender.getEnd_date());
		db.insert(TABLE_CALENDER, null, values);
		db.close();
	}

	public void addAllCalender(ArrayList<Calender> calenderList) {
		/* add calender */
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "INSERT INTO " + TABLE_CALENDER + "(" + KEY_SERVICE_ID
				+ " , " + KEY_MONDAY + " , " + KEY_TUESDAY + " , "
				+ KEY_WEDNESDAY + " , " + KEY_THURSDAY + " , " + KEY_FRIDAY
				+ " , " + KEY_SATAURDAY + " , " + KEY_SUNDAY + " , "
				+ KEY_START_DATE + " , " + KEY_END_DATE
				+ " ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		db.beginTransaction();
		SQLiteStatement stmt = db.compileStatement(sql);
		for (int i = 0; i < calenderList.size(); i++) {
			stmt.bindString(1, calenderList.get(i).getService_id());
			stmt.bindLong(2, calenderList.get(i).getMonday());
			stmt.bindLong(3, calenderList.get(i).getTuesday());
			stmt.bindLong(4, calenderList.get(i).getWednesday());
			stmt.bindLong(5, calenderList.get(i).getThursday());
			stmt.bindLong(6, calenderList.get(i).getFriday());
			stmt.bindLong(7, calenderList.get(i).getSaturday());
			stmt.bindLong(8, calenderList.get(i).getSunday());
			stmt.bindString(9, calenderList.get(i).getStart_date());
			stmt.bindString(10, calenderList.get(i).getEnd_date());
			stmt.execute();
			stmt.clearBindings();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		db.close();
		db.close();
	}

	public ArrayList<Calender> getAllCalender() {
		/* get all calender */
		ArrayList<Calender> dataList = new ArrayList<Calender>();
		String selectQuery = "SELECT  * FROM " + TABLE_CALENDER;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Calender calender = new Calender();
				calender.setService_id(cursor.getString(0));
				calender.setMonday(cursor.getInt(1));
				calender.setTuesday(cursor.getInt(2));
				calender.setWednesday(cursor.getInt(3));
				calender.setThursday(cursor.getInt(4));
				calender.setFriday(cursor.getInt(5));
				calender.setSaturday(cursor.getInt(6));
				calender.setSunday(cursor.getInt(7));
				calender.setStart_date(cursor.getString(8));
				calender.setEnd_date(cursor.getString(9));
				dataList.add(calender);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		return dataList;
	}

	void addStopTime(StopTimes stopTime) {
		/* add stoptime */
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_TRIP_ID, stopTime.getTrip_id());
		values.put(KEY_ARRIVAL_TIME, stopTime.getArrival_time());
		values.put(KEY_DEPARTURE_TIME, stopTime.getDeparture_time());
		values.put(KEY_STOP_ID, stopTime.getStop_id());
		values.put(KEY_STOP_SEQUENCE, stopTime.getStop_sequence());
		values.put(KEY_STOP_HEADSIGN, stopTime.getStop_headsign());
		values.put(KEY_PICKUP_TYPE, stopTime.getPickup_type());
		values.put(KEY_DROP_OFF_TYPE, stopTime.getDrop_off_type());
		values.put(KEY_SHAPE_DIST_TRAVELED, stopTime.getShape_dist_traveled());
		db.insert(TABLE_STOP_TIMES, null, values);
		db.close();
	}

	public void addAllStopTime(ArrayList<StopTimes> stopTimesList) {
		/* add stoptime */
		SQLiteDatabase db = this.getWritableDatabase();

		String sql = "INSERT INTO " + TABLE_STOP_TIMES + "(" + KEY_TRIP_ID
				+ " , " + KEY_ARRIVAL_TIME + " , " + KEY_DEPARTURE_TIME + " , "
				+ KEY_STOP_ID + " , " + KEY_STOP_SEQUENCE + " , "
				+ KEY_STOP_HEADSIGN + " , " + KEY_PICKUP_TYPE + " , "
				+ KEY_DROP_OFF_TYPE + " , " + KEY_SHAPE_DIST_TRAVELED
				+ " ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		db.beginTransaction();
		SQLiteStatement stmt = db.compileStatement(sql);
		for (int i = 0; i < stopTimesList.size(); i++) {
			stmt.bindString(1, stopTimesList.get(i).getTrip_id());
			stmt.bindString(2, stopTimesList.get(i).getArrival_time());
			stmt.bindString(3, stopTimesList.get(i).getDeparture_time());
			stmt.bindString(4, stopTimesList.get(i).getStop_id());
			stmt.bindLong(5, stopTimesList.get(i).getStop_sequence());
			stmt.bindString(6, stopTimesList.get(i).getStop_headsign());
			stmt.bindString(7, stopTimesList.get(i).getPickup_type());
			stmt.bindString(8, stopTimesList.get(i).getDrop_off_type());
			stmt.bindString(9, stopTimesList.get(i).getShape_dist_traveled());
			stmt.execute();
			stmt.clearBindings();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		db.close();
		db.close();
	}

	public ArrayList<StopTimes> getAllStopTime() {
		/* get all stoptime */
		ArrayList<StopTimes> dataList = new ArrayList<StopTimes>();
		String selectQuery = "SELECT  * FROM " + TABLE_STOP_TIMES;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				StopTimes stopTime = new StopTimes();
				stopTime.setTrip_id(cursor.getString(0));
				stopTime.setArrival_time(cursor.getString(1));
				stopTime.setDeparture_time(cursor.getString(2));
				stopTime.setStop_id(cursor.getString(3));
				stopTime.setStop_sequence(cursor.getInt(4));
				stopTime.setStop_headsign(cursor.getString(5));
				stopTime.setPickup_time(cursor.getString(6));
				stopTime.setDrop_off_type(cursor.getString(7));
				stopTime.setShape_dist_traveled(cursor.getString(8));
				dataList.add(stopTime);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		return dataList;
	}

	void addTrips(Trips trips) {
		/* add trips */
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_ROUTE_ID, trips.getRoute_id());
		values.put(KEY_SERVICE_ID, trips.getService_id());
		values.put(KEY_TRIP_ID, trips.getTrip_id());
		values.put(KEY_TRIP_HEADSIGN, trips.getTrip_headsign());
		values.put(KEY_DIRECTION_ID, trips.getDirection_id());
		db.insert(TABLE_TRIPS, null, values);

	}

	public void addAllTrips(ArrayList<Trips> tripsList) {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "INSERT INTO " + TABLE_TRIPS + "(" + KEY_ROUTE_ID + " , "
				+ KEY_SERVICE_ID + " , " + KEY_TRIP_ID + " , "
				+ KEY_TRIP_HEADSIGN + " , " + KEY_DIRECTION_ID
				+ " ) VALUES (?, ?, ?, ?, ?)";
		db.beginTransaction();
		SQLiteStatement stmt = db.compileStatement(sql);
		for (int i = 0; i < tripsList.size(); i++) {
			stmt.bindString(1, tripsList.get(i).getRoute_id());
			stmt.bindString(2, tripsList.get(i).getService_id());
			stmt.bindString(3, tripsList.get(i).getTrip_id());
			stmt.bindString(4, tripsList.get(i).getTrip_headsign());
			stmt.bindString(5, tripsList.get(i).getDirection_id());
			stmt.execute();
			stmt.clearBindings();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		db.close();
		db.close();
	}

	public ArrayList<Trips> getAllTrips() {
		/* get all trips */
		ArrayList<Trips> dataList = new ArrayList<Trips>();
		String selectQuery = "SELECT  * FROM " + TABLE_TRIPS;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Trips trips = new Trips();
				trips.setRoute_id(cursor.getString(0));
				trips.setService_id(cursor.getString(1));
				trips.setTrip_id(cursor.getString(2));
				trips.setTrip_headsign(cursor.getString(3));
				trips.setDirection_id(cursor.getString(4));
				dataList.add(trips);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		return dataList;
	}

	public ArrayList<Trips> getAllTripsToDisplay() {
		/* get all valid trips from entries(trips) */
		ArrayList<Trips> dataList = new ArrayList<Trips>();
		String selectQuery = "SELECT  * FROM " + TABLE_TRIPS + " WHERE "
				+ KEY_TRIP_ID + " IN (SELECT " + KEY_TRIP_ID + " FROM "
				+ TABLE_STOP_TIMES + ") AND " + KEY_SERVICE_ID + " IN (SELECT "
				+ KEY_SERVICE_ID + " FROM " + TABLE_CALENDER + ") AND "
				+ KEY_ROUTE_ID + " IN (SELECT " + KEY_ROUTE_ID + " FROM "
				+ TABLE_ROUTES + ")";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Trips trips = new Trips();
				trips.setRoute_id(cursor.getString(0));
				trips.setService_id(cursor.getString(1));
				trips.setTrip_id(cursor.getString(2));
				trips.setTrip_headsign(cursor.getString(3));
				trips.setDirection_id(cursor.getString(4));
				dataList.add(trips);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		return dataList;
	}

	public ArrayList<Stops> getAllStopsFromStops() {
		/*
		 * get all stops with ordering, replacing first position with yangon and
		 * second position with mandalay
		 */
		ArrayList<Stops> dataList = new ArrayList<Stops>();
		String selectQuery = "SELECT * FROM " + TABLE_STOPS + " ORDER BY "
				+ KEY_STOP_DESC + "";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Stops stops = new Stops();
				stops.setStop_id(cursor.getString(0));
				stops.setStop_code(cursor.getString(1));
				stops.setStop_name(cursor.getString(2));
				stops.setStop_desc(cursor.getString(3));
				stops.setStop_lat(cursor.getFloat(4));
				stops.setStop_lon(cursor.getFloat(5));
				stops.setZone_id(cursor.getString(6));
				stops.setStop_url(cursor.getString(7));
				stops.setLocation_type(cursor.getInt(8));
				stops.setParent_station(cursor.getString(9));
				stops.setStop_timezone(cursor.getString(10));
				stops.setWheelchair_boarding(cursor.getString(11));
				if (cursor.getString(3).toString().trim()
						.equalsIgnoreCase("Yangon")) {
					if (dataList.contains(stops)) {
					} else {
						dataList.add(0, stops);
					}
				} else if (cursor.getString(3).toString().trim()
						.equalsIgnoreCase("Mandalay")) {
					if (dataList.contains(stops)) {
					} else {
						dataList.add(0, stops);
					}
				} else {
					if (dataList.contains(stops)) {
					} else {
						dataList.add(stops);
					}
				}
			} while (cursor.moveToNext());
			Stops stops = new Stops();
			stops.setStop_id("");
			stops.setStop_code("");
			stops.setStop_name("");
			stops.setStop_lat((float) 0);
			stops.setStop_lon((float) 0);
			stops.setStop_url("");
			stops.setLocation_type(0);
			stops.setStop_desc("Leaving from...");
			dataList.add(0, stops);
		}

		cursor.close();
		db.close();
		return dataList;
	}

	public ArrayList<Stops> getBusStops() {
		/*
		 * get all bus stops with ordering, replacing first position with yangon
		 * and second position with mandalay
		 */
		ArrayList<Stops> dataList = new ArrayList<Stops>();
		String selectQuery = "SELECT * FROM " + TABLE_STOPS + " WHERE "
				+ KEY_STOP_ID + " LIKE 'STB%' ORDER BY " + KEY_STOP_DESC + "";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Stops stops = new Stops();
				stops.setStop_id(cursor.getString(0));
				stops.setStop_code(cursor.getString(1));
				stops.setStop_name(cursor.getString(2));
				stops.setStop_desc(cursor.getString(3));
				stops.setStop_lat(cursor.getFloat(4));
				stops.setStop_lon(cursor.getFloat(5));
				stops.setZone_id(cursor.getString(6));
				stops.setStop_url(cursor.getString(7));
				stops.setLocation_type(cursor.getInt(8));
				stops.setParent_station(cursor.getString(9));
				stops.setStop_timezone(cursor.getString(10));
				stops.setWheelchair_boarding(cursor.getString(11));
				if (cursor.getString(3).toString().trim()
						.equalsIgnoreCase("Yangon")) {
					dataList.add(0, stops);
				} else if (cursor.getString(3).toString().trim()
						.equalsIgnoreCase("Mandalay")) {
					dataList.add(0, stops);
				} else {
					dataList.add(stops);
				}
			} while (cursor.moveToNext());
			Stops stops = new Stops();
			stops.setStop_id("");
			stops.setStop_code("");
			stops.setStop_name("");
			stops.setStop_lat((float) 0);
			stops.setStop_lon((float) 0);
			stops.setStop_url("");
			stops.setLocation_type(0);
			stops.setStop_desc("Leaving from...");
			dataList.add(0, stops);
		}

		cursor.close();
		db.close();
		return dataList;
	}

	public ArrayList<Stops> getAirStops() {
		/*
		 * get all air stops with ordering, replacing first position with yangon
		 * and second position with mandalay
		 */
		ArrayList<Stops> dataList = new ArrayList<Stops>();
		String selectQuery = "SELECT * FROM " + TABLE_STOPS + " WHERE "
				+ KEY_STOP_ID + " LIKE 'STA%' ORDER BY " + KEY_STOP_DESC + "";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Stops stops = new Stops();
				stops.setStop_id(cursor.getString(0));
				stops.setStop_code(cursor.getString(1));
				stops.setStop_name(cursor.getString(2));
				stops.setStop_desc(cursor.getString(3));
				stops.setStop_lat(cursor.getFloat(4));
				stops.setStop_lon(cursor.getFloat(5));
				stops.setZone_id(cursor.getString(6));
				stops.setStop_url(cursor.getString(7));
				stops.setLocation_type(cursor.getInt(8));
				stops.setParent_station(cursor.getString(9));
				stops.setStop_timezone(cursor.getString(10));
				stops.setWheelchair_boarding(cursor.getString(11));

				if (cursor.getString(3).toString().trim()
						.equalsIgnoreCase("Yangon")) {
					dataList.add(0, stops);
				} else if (cursor.getString(3).toString().trim()
						.equalsIgnoreCase("Mandalay")) {
					dataList.add(0, stops);
				} else {
					dataList.add(stops);
				}
			} while (cursor.moveToNext());
			Stops stops = new Stops();
			stops.setStop_id("");
			stops.setStop_code("");
			stops.setStop_name("");
			stops.setStop_lat((float) 0);
			stops.setStop_lon((float) 0);
			stops.setStop_url("");
			stops.setLocation_type(0);
			stops.setStop_desc("Leaving from...");
			dataList.add(0, stops);
		}

		cursor.close();
		db.close();
		return dataList;
	}

	public ArrayList<Stops> getRailStops() {
		/*
		 * get all rail stops with ordering, replacing first position with
		 * yangon and second position with mandalay
		 */
		ArrayList<Stops> dataList = new ArrayList<Stops>();
		String selectQuery = "SELECT * FROM " + TABLE_STOPS + " WHERE "
				+ KEY_STOP_ID + " LIKE 'STR%' ORDER BY " + KEY_STOP_DESC + "";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Stops stops = new Stops();
				stops.setStop_id(cursor.getString(0));
				stops.setStop_code(cursor.getString(1));
				stops.setStop_name(cursor.getString(2));
				stops.setStop_desc(cursor.getString(3));
				stops.setStop_lat(cursor.getFloat(4));
				stops.setStop_lon(cursor.getFloat(5));
				stops.setZone_id(cursor.getString(6));
				stops.setStop_url(cursor.getString(7));
				stops.setLocation_type(cursor.getInt(8));
				stops.setParent_station(cursor.getString(9));
				stops.setStop_timezone(cursor.getString(10));
				stops.setWheelchair_boarding(cursor.getString(11));

				if (cursor.getString(3).toString().trim()
						.equalsIgnoreCase("Yangon")) {
					dataList.add(0, stops);
				} else if (cursor.getString(3).toString().trim()
						.equalsIgnoreCase("Mandalay")) {
					dataList.add(0, stops);
				} else {
					dataList.add(stops);
				}
			} while (cursor.moveToNext());
			Stops stops = new Stops();
			stops.setStop_id("");
			stops.setStop_code("");
			stops.setStop_name("");
			stops.setStop_lat((float) 0);
			stops.setStop_lon((float) 0);
			stops.setStop_url("");
			stops.setLocation_type(0);
			stops.setStop_desc("Leaving from...");
			dataList.add(0, stops);
		}

		cursor.close();
		db.close();
		return dataList;
	}

	public ArrayList<StopTimes> getTripSOfRoute(String from, String Day,
			String str, String date, String years, int i) {
		/*
		 * get all stoptimess with stopname,day,stoptype, date, year,
		 * listtype(from/to) (specified)
		 */

		dataList = new ArrayList<StopTimes>();
		dataListCopy = new ArrayList<StopTimes>();
		String selectQuery = "SELECT * FROM " + TABLE_STOP_TIMES + ","
				+ TABLE_TRIPS + "," + TABLE_CALENDER + " WHERE " + KEY_STOP_ID
				+ " IN ( SELECT " + KEY_STOP_ID + " FROM " + TABLE_STOPS
				+ " WHERE " + KEY_STOP_DESC + "=\"" + from + "\" AND "
				+ KEY_STOP_ID + " LIKE '" + str + "%') AND " + TABLE_TRIPS
				+ "." + KEY_TRIP_ID + "=" + TABLE_STOP_TIMES + "."
				+ KEY_TRIP_ID + " AND " + TABLE_CALENDER + "." + KEY_SERVICE_ID
				+ "=" + TABLE_TRIPS + "." + KEY_SERVICE_ID + " AND " + Day
				+ "=1 ORDER BY " + KEY_DEPARTURE_TIME;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				StopTimes stopTime = new StopTimes();
				stopTime.setTrip_id(cursor.getString(0));
				stopTime.setArrival_time(cursor.getString(1));
				stopTime.setDeparture_time(cursor.getString(2));
				stopTime.setStop_id(cursor.getString(3));
				stopTime.setStop_sequence(cursor.getInt(4));
				dataList.add(stopTime);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		return dataList;
	}

	String getEndDateOf(String serviceIDFromTripID) {
		/*
		 * check the enddate of a service
		 */
		String dataList = null;
		String selectQuery = "SELECT " + KEY_END_DATE + " FROM "
				+ TABLE_CALENDER + " WHERE " + KEY_SERVICE_ID + "=\""
				+ serviceIDFromTripID + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				dataList = cursor.getString(0);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		return dataList;
	}

	public String getStartDateOf(String serviceIDFromTripID) {
		/*
		 * check the startdate of a service
		 */
		String dataList = null;
		String selectQuery = "SELECT " + KEY_START_DATE + " FROM "
				+ TABLE_CALENDER + " WHERE " + KEY_SERVICE_ID + "=\""
				+ serviceIDFromTripID + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					dataList = cursor.getString(0);
				} while (cursor.moveToNext());
			}
		}
		cursor.close();
		db.close();
		return dataList;
	}

	public String getParentStation(String parentstationstopid) {
		/*
		 * return the parent station of the stop with stop_id(specified)
		 */
		String dataList = null;
		String selectQuery = "SELECT " + KEY_STOP_NAME + " FROM " + TABLE_STOPS
				+ " WHERE " + KEY_STOP_ID + "=\"" + parentstationstopid + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				dataList = cursor.getString(0);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		return dataList;
	}

	public ArrayList<ListClass> getFilteredDateList(
			ArrayList<ListClass> listClassArray, String date, String years) {
		/*
		 * filter the list of Listclass array with date and year(specified)
		 */

		String dtaenew = getDateFormatNew(context, date, years);

		final ArrayList<Integer> dayList = new ArrayList<Integer>();
		final ArrayList<String> tripIdList = new ArrayList<String>();
		ArrayList<ListClass> dataList = new ArrayList<ListClass>();
		String segments[] = dtaenew.split("/");

		int day = Integer.parseInt(segments[segments.length - 3]);
		int month = Integer.parseInt(segments[segments.length - 2]);
		int year = Integer.parseInt(segments[segments.length - 1]);

		dayList.clear();

		Calendar calendar1 = Calendar.getInstance();
		calendar1.set(Calendar.YEAR, year);
		calendar1.set(Calendar.MONTH, month - 1);
		calendar1.set(Calendar.DATE, day);
		int weekday = calendar1.get(Calendar.DAY_OF_WEEK);
		dayList.add(weekday);

		/*
		 * check the trips and service with the listclass array
		 */
		for (int tripidcount = 0; tripidcount < listClassArray.size(); tripidcount++) {
			String selectQueryForServideId = "SELECT *" + " FROM "
					+ TABLE_TRIPS + " WHERE " + KEY_TRIP_ID + "=\""
					+ listClassArray.get(tripidcount).getTrip_id() + "\""
					+ " AND " + KEY_SERVICE_ID + " IN ( SELECT "
					+ KEY_SERVICE_ID + " FROM " + TABLE_CALENDER + ")";

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQueryForServideId, null);
			if (cursor.moveToFirst()) {
				do {
					tripIdList.add(cursor.getString(0));
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}
		return dataList;
	}

	public String getServiceIDFromTripID(String trip_id) {
		/*
		 * return service id from a trip_id (specified)
		 */
		String dataList = null;
		String selectQuery = "SELECT " + KEY_SERVICE_ID + " FROM "
				+ TABLE_TRIPS + " WHERE " + KEY_TRIP_ID + "=\"" + trip_id
				+ "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					dataList = cursor.getString(0);
				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		db.close();
		return dataList;
	}

	public int isNextDayAvailable(String serviceIDFromTripID,
			String day_selected) {
		/*
		 * check for the service is available on next day with service_id
		 * (specified)
		 */
		int dataList = 0;
		String selectQuery = "SELECT " + getNextDay(day_selected) + " FROM "
				+ TABLE_CALENDER + " WHERE " + KEY_SERVICE_ID + "=\""
				+ serviceIDFromTripID + "\"";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					dataList = cursor.getInt(0);
				} while (cursor.moveToNext());
			}

			cursor.close();
		}
		db.close();
		return dataList;
	}

	@SuppressLint("DefaultLocale")
	private String getNextDay(String day_selected) {
		/*
		 * return the next day as string
		 */
		String day = null;

		if (day_selected.equals("SUNDAY")) {
			day = "monday";
		}
		if (day_selected.equals("MONDAY")) {
			day = "tuesday";
		}
		if (day_selected.equals("TUESDAY")) {
			day = "wednesday";
		}
		if (day_selected.equals("WEDNESDAY")) {
			day = "thursday";
		}
		if (day_selected.equals("THURSDAY")) {
			day = "friday";
		}
		if (day_selected.equals("FRIDAY")) {
			day = "saturday";
		}
		if (day_selected.equals("SATURDAY")) {
			day = "sunday";
		}
		return day;
	}

	public String getRouteId(String service_Id, String trip_Id) {
		/*
		 * return route_id from service_id, trip_id (specified)
		 */
		String route_id = "";
		String selectQuery = "SELECT " + KEY_ROUTE_ID + " FROM " + TABLE_TRIPS
				+ " WHERE " + KEY_SERVICE_ID + "=\"" + service_Id + "\" AND "
				+ KEY_TRIP_ID + "=\"" + trip_Id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					route_id += cursor.getString(0);
				} while (cursor.moveToNext());
			}

			cursor.close();
		}
		db.close();
		return route_id;
	}

	public String getAgencyName(String agency_id) {
		/*
		 * return agency_name from agency_id (specified)
		 */
		String agency_name = "";
		String selectQuery = "SELECT " + KEY_AGENCY_NAME + " FROM "
				+ TABLE_AGENCY + " WHERE " + KEY_AGENCY_ID + "=\"" + agency_id
				+ "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					agency_name += cursor.getString(0);
				} while (cursor.moveToNext());
			}

			cursor.close();
			db.close();
		}
		return agency_name;
	}

	public String getAgencyId(String route_id) {
		/*
		 * return agency_Id from route_id (specified)
		 */
		String agency_id = "";
		String selectQuery = "SELECT " + KEY_AGENCY_ID + " FROM "
				+ TABLE_ROUTES + " WHERE " + KEY_ROUTE_ID + "=\"" + route_id
				+ "\"";

		// Log.e("selectQuery", "" + selectQuery);
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					agency_id += cursor.getString(0);
				} while (cursor.moveToNext());
			}
			// Log.e("Agency id:", ">> " + agency_id);

			cursor.close();
		}
		db.close();
		return agency_id;
	}

	public ArrayList<StopTimes> getRoute(String str) {
		/*
		 * return StopTimes array with matching from trip_id (specified)
		 */
		ArrayList<StopTimes> dataList = new ArrayList<StopTimes>();
		String selectQuery = "SELECT * FROM " + TABLE_STOP_TIMES + " WHERE "
				+ KEY_TRIP_ID + "=\"" + str + "\"";

		// Log.e("selectQuery:", selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					StopTimes stopTime = new StopTimes();
					stopTime.setTrip_id(cursor.getString(0));
					stopTime.setArrival_time(cursor.getString(1));
					stopTime.setDeparture_time(cursor.getString(2));
					stopTime.setStop_id(cursor.getString(3));
					stopTime.setStop_sequence(cursor.getInt(4));

					dataList.add(stopTime);
				} while (cursor.moveToNext());
			}

			cursor.close();
		}
		db.close();
		return dataList;
	}

	public RouteList getCordinates(String stop_id) {
		// Log.e(stop_id, stop_id);
		/*
		 * return co-ordinates with city from stop_id (specified)
		 */
		RouteList parent_station = new RouteList();
		String selectQuery = "SELECT " + KEY_STOP_DESC + ", " + KEY_STOP_LAT
				+ ", " + KEY_STOP_LON + " FROM " + TABLE_STOPS + " WHERE "
				+ KEY_STOP_ID + "=\"" + stop_id + "\"";

		// Log.e("selectQuery", "" + selectQuery);
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				parent_station.setCity(cursor.getString(0));
				parent_station.setLat(cursor.getFloat(1));
				parent_station.setLon(cursor.getFloat(2));

			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		return parent_station;
	}

	public String getLat(String source_stop_id) {
		/*
		 * return latitude from stop_id (specified)
		 */
		String latitude = null;
		String selectQuery = "SELECT " + KEY_STOP_LAT + " FROM " + TABLE_STOPS
				+ " WHERE " + KEY_STOP_ID + "=\"" + source_stop_id + "\"";

		// Log.e("selectQuery:", selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				// Log.d("latitude", cursor.getString(0) + "");
				latitude = cursor.getString(0);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return latitude;
	}

	public String getLong(String source_stop_id) {
		/*
		 * return longitude from stop_id (specified)
		 */
		String longitude = null;
		String selectQuery = "SELECT " + KEY_STOP_LON + " FROM " + TABLE_STOPS
				+ " WHERE " + KEY_STOP_ID + "=\"" + source_stop_id + "\"";

		// Log.e("selectQuery:", selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				// Log.d("longitude", cursor.getString(0) + "");
				longitude = cursor.getString(0);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return longitude;
	}

	public String getCity(String source_stop_id) {
		/*
		 * return city from stop_id (specified)
		 */
		String city = null;
		String selectQuery = "SELECT " + KEY_STOP_NAME + " FROM " + TABLE_STOPS
				+ " WHERE " + KEY_STOP_ID + "=\"" + source_stop_id + "\"";

		// Log.e("selectQuery:", selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				// Log.d("latitude", cursor.getString(0) + "");
				city = cursor.getString(0);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return city;
	}

	public int getSequenceNumb(String trip_Id, String source_Id) {
		/*
		 * return sequence number from trip_Id, stop_Id (specified)
		 */
		int seq = 0;
		String selectQuery = "SELECT " + KEY_STOP_SEQUENCE + " FROM "
				+ TABLE_STOP_TIMES + " WHERE " + KEY_STOP_ID + "=\""
				+ source_Id + "\" AND " + KEY_TRIP_ID + "=\"" + trip_Id + "\"";

		// Log.e("selectQuery:", selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				// Log.d("latitude", cursor.getInt(0) + "");
				seq = cursor.getInt(0);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return seq;
	}

	public void addFavourite(ListClass listClass, String currentDate) {
		/*
		 * add favourite
		 */
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_SOURCE_STOP_ID, listClass.getSource_stop_id());
		values.put(KEY_DESTINATION_STOP_ID, listClass.getDest_stop_id());
		values.put(KEY_SOURCE_STOP_NAME, listClass.getSource_stop_name());
		values.put(KEY_DESTINATION_STOP_NAME, listClass.getDest_stop_name());
		values.put(KEY_SOURCE_DEPARTURE_TIME, listClass.getSource_dept_time());
		values.put(KEY_DESTINATION_ARRIVAL_TIME,
				listClass.getDest_arrival_time());
		values.put(KEY_SERVICE_ID, listClass.getService_id());
		values.put(KEY_TRIP_ID, listClass.getTrip_id());
		values.put(KEY_SOURCE_STOP_SEQ, listClass.getSource_sequence());
		values.put(KEY_DESTINATION_STOP_SEQ, listClass.getDest_sequence());
		values.put(KEY_IS_AFTER_DATE, listClass.isAfterDate());

		String date = currentDate;
		values.put(KEY_DATE, date);
		db.insert(TABLE_FAV_STOPS, null, values);

		db.close();
	}

	public void deleteFavourite(ListClass listClass) {
		/*
		 * delete favourite
		 */
		SQLiteDatabase db = this.getWritableDatabase();
		String selectQuery = "DELETE FROM " + TABLE_FAV_STOPS + " WHERE "
				+ KEY_SOURCE_STOP_ID + "=\"" + listClass.getSource_stop_id()
				+ "\" AND " + KEY_DESTINATION_STOP_ID + "=\""
				+ listClass.getDest_stop_id() + "\" AND " + KEY_TRIP_ID + "=\""
				+ listClass.getTrip_id() + "\" AND " + KEY_SERVICE_ID + "=\""
				+ listClass.getService_id() + "\"";
		db.execSQL(selectQuery);
		db.close();
	}

	public ArrayList<ListClass> getAllFavs() {
		/*
		 * return all favourite list
		 */
		ArrayList<ListClass> dataList = new ArrayList<ListClass>();
		String selectQuery = "SELECT * FROM " + TABLE_FAV_STOPS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				ListClass favListClass = new ListClass();
				favListClass.setSource_stop_id(cursor.getString(1));
				favListClass.setDest_stop_id(cursor.getString(2).toString());
				favListClass.setSource_stop_name(cursor.getString(3));
				favListClass.setDest_stop_name(cursor.getString(4).toString());
				favListClass.setSource_dept_time(cursor.getString(5));
				favListClass.setDest_arrival_time(cursor.getString(6));
				favListClass.setService_id(cursor.getString(7).toString());
				favListClass.setTrip_id(cursor.getString(8));
				favListClass.setSource_sequence(cursor.getInt(9));
				favListClass.setDest_sequence(cursor.getInt(10));
				favListClass.setDate(cursor.getString(12));
				int ad = cursor.getInt(11);
				if (ad == 0) {
					favListClass.setAfterDate(true);
				} else {
					favListClass.setAfterDate(false);
				}
				dataList.add(favListClass);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return dataList;
	}

	public ArrayList<ListClass> getAllFavsWithDates() {
		/*
		 * return all favourite list
		 */
		ArrayList<ListClass> dataList = new ArrayList<ListClass>();
		String selectQuery = "SELECT * FROM " + TABLE_FAV_STOPS + " ORDER BY "
				+ KEY_DATE + " ASC , " + KEY_SOURCE_DEPARTURE_TIME;
		// String selectQuery =
		// "SELECT * FROM fav ORDER BY fav_date,fav_source_departure_time DESC";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				ListClass favListClass = new ListClass();
				favListClass.setSource_stop_id(cursor.getString(1));
				favListClass.setDest_stop_id(cursor.getString(2).toString());
				favListClass.setSource_stop_name(cursor.getString(3));
				favListClass.setDest_stop_name(cursor.getString(4).toString());
				favListClass.setSource_dept_time(cursor.getString(5));
				favListClass.setDest_arrival_time(cursor.getString(6));
				favListClass.setService_id(cursor.getString(7).toString());
				favListClass.setTrip_id(cursor.getString(8));
				favListClass.setSource_sequence(cursor.getInt(9));
				favListClass.setDest_sequence(cursor.getInt(10));
				String dateSaved = null;
				String segments[] = cursor.getString(12).split("/");
				String day = segments[segments.length - 3];
				if (day.length() == 1) {
					dateSaved = "0" + day;
				} else {
					dateSaved = day;
				}
				String month = segments[segments.length - 2];
				if (month.length() == 1) {
					dateSaved = dateSaved + "/0" + month;
				} else {
					dateSaved = dateSaved + "/" + month;
				}
				String year = segments[segments.length - 1];
				dateSaved = dateSaved + "/" + year;
				// Log.e("dateSaved", cursor.getString(12));
				if (cursor.getString(12).length() < 10) {
					favListClass.setDate("0" + cursor.getString(12));
				} else {
					favListClass.setDate(cursor.getString(12));
				}
				int ad = cursor.getInt(11);
				if (ad == 0) {
					favListClass.setAfterDate(false);
				} else {
					favListClass.setAfterDate(true);
				}
				dataList.add(favListClass);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return dataList;
	}

	public boolean favExists(ListClass listClass, String currentDate,
			String currentMonth, String currentYear) {
		/*
		 * check if favourite exists
		 */
		int yes = 0;
		int day = Integer.parseInt(currentDate.trim());
		int month = Integer.parseInt(currentMonth.trim());
		int year = Integer.parseInt(currentYear.trim());
		String date = day + "/" + month + "/" + year;
		String selectQuery = "SELECT * FROM " + TABLE_FAV_STOPS + " WHERE "
				+ KEY_SOURCE_STOP_ID + "=\"" + listClass.getSource_stop_id()
				+ "\" AND " + KEY_DESTINATION_STOP_ID + "=\""
				+ listClass.getDest_stop_id() + "\" AND " + KEY_TRIP_ID + "=\""
				+ listClass.getTrip_id() + "\" AND " + KEY_SERVICE_ID + "=\""
				+ listClass.getService_id() + "\" AND " + KEY_DATE + "=\""
				+ date + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				yes = 1;
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		if (yes == 1) {
			return true;
		} else {
			return false;
		}
	}

	public void deleteAllTables() {
		/*
		 * delete all tables
		 */
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_AGENCY, null, null);
		db.delete(TABLE_CALENDER, null, null);
		db.delete(TABLE_ROUTES, null, null);
		db.delete(TABLE_STOP_TIMES, null, null);
		db.delete(TABLE_STOPS, null, null);
		db.delete(TABLE_TRIPS, null, null);
		db.close();
	}

	public String getRouteColor(String service_id, String trip_id) {
		/*
		 * return route color
		 */
		// Log.e(service_id, trip_id);
		String routeColor = null;
		String routeId = getRouteId(service_id, trip_id);
		String selectQuery = "SELECT " + KEY_ROUTE_COLOR + " FROM "
				+ TABLE_ROUTES + " WHERE " + KEY_ROUTE_ID + "=\"" + routeId
				+ "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				routeColor = cursor.getString(0);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return routeColor;
	}

	public ArrayList<String> getStopIdFromName(String stringStopNameSrc,
			String stopType) {
		/*
		 * return stop_id from stop_desc (stop name) with stopname, stoptype
		 * (spcified)
		 */
		// Log.e("stringStopNameSrc", stringStopNameSrc + "");
		// Log.e("stopType", stopType + "");

		ArrayList<String> stopListings = new ArrayList<String>();
		String stopid = null;
		String selectQuery = "SELECT " + KEY_STOP_ID + " FROM " + TABLE_STOPS
				+ " WHERE " + KEY_STOP_ID + " LIKE '" + stopType + "%' AND "
				+ KEY_STOP_DESC + "=\"" + stringStopNameSrc + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		// Log.e(selectQuery, selectQuery);
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				stopid = cursor.getString(0);
				stopListings.add(stopid);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return stopListings;
	}

	public void deleteFavourite(ListClass listClass, String date) {

		String segments[] = date.split("/");
		int day = Integer.parseInt(segments[segments.length - 3]);
		int month = Integer.parseInt(segments[segments.length - 2]);
		int year = Integer.parseInt(segments[segments.length - 1]);
		date = day + "/" + month + "/" + year;
		/*
		 * delete favourite
		 */
		// Log.e("listrclass date", date + "is giving");
		SQLiteDatabase db = this.getWritableDatabase();
		String selectQuery = "DELETE FROM " + TABLE_FAV_STOPS + " WHERE "
				+ KEY_SOURCE_STOP_ID + "=\"" + listClass.getSource_stop_id()
				+ "\" AND " + KEY_DESTINATION_STOP_ID + "=\""
				+ listClass.getDest_stop_id() + "\" AND " + KEY_TRIP_ID + "=\""
				+ listClass.getTrip_id() + "\" AND " + KEY_SERVICE_ID + "=\""
				+ listClass.getService_id() + "\" AND " + KEY_DATE + "='"
				+ date + "'";
		db.execSQL(selectQuery);
		db.close();
		// Log.e("deleted fac", selectQuery);
	}

	public int getDataCount() {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("SELECT COUNT (*) FROM " + TABLE_AGENCY,
				null);
		cursor.moveToFirst();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		return count;
	}

	public Agency getAgencyDetailsWithId(String agency_id) {
		Agency agency = new Agency();
		String selectQuery = "SELECT * FROM " + TABLE_AGENCY + " WHERE "
				+ KEY_AGENCY_ID + "=\"" + agency_id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				agency.setAgency_id(cursor.getString(0));
				agency.setAgency_name(cursor.getString(1));
				agency.setAgency_url(cursor.getString(2));
				agency.setAgency_timezone(cursor.getString(3));
				agency.setAgency_lang(cursor.getString(4));
				agency.setAgency_phone(cursor.getString(5));
				agency.setAgency_fare_url(cursor.getString(6));
				agency.setAgency_email(cursor.getString(7));
				agency.setAgency_address(cursor.getString(8));
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return agency;
	}

	public ArrayList<String> findtopsStartPoint(String spinnerOneStation,
			String like) {

		ArrayList<String> nextStopsString = new ArrayList<String>();
		String selectQuery = "SELECT * FROM " + TABLE_STOPS + " WHERE "
				+ KEY_STOP_DESC + "=\"" + spinnerOneStation + "\" AND "
				+ KEY_STOP_ID + " LIKE '" + like + "%' ";

		// Log.e("selectQuery:", selectQuery);
		Stops stops = null;

		ArrayList<Stops> stopListings = new ArrayList<Stops>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				stops = new Stops();
				stops.setStop_id(cursor.getString(0));
				// Log.e("stop name", cursor.getString(3));
				stops.setStop_code(cursor.getString(1));
				stops.setStop_name(cursor.getString(2));
				stops.setStop_desc(cursor.getString(3));
				stops.setStop_lat(cursor.getFloat(4));
				stops.setStop_lon(cursor.getFloat(5));
				stops.setZone_id(cursor.getString(6));
				stops.setStop_url(cursor.getString(7));
				stops.setLocation_type(cursor.getInt(8));
				stops.setParent_station(cursor.getString(9));
				stops.setStop_timezone(cursor.getString(10));
				stops.setWheelchair_boarding(cursor.getString(11));
				stopListings.add(stops);
			} while (cursor.moveToNext());
		}

		/* stopTimes with stop id */
		if (stops != null) {
			ArrayList<StopTimes> dataList = new ArrayList<StopTimes>();
			for (int stopListingCount = 0; stopListingCount < stopListings
					.size(); stopListingCount++) {
				String selectQueryStopTimes = "SELECT * FROM "
						+ TABLE_STOP_TIMES + "  WHERE " + KEY_STOP_ID + "=\""
						+ stopListings.get(stopListingCount).getStop_id()
						+ "\" ";
				// Log.e("selectQueryStopTimes:", selectQueryStopTimes);
				db = this.getWritableDatabase();
				cursor = db.rawQuery(selectQueryStopTimes, null);
				if (cursor.moveToFirst()) {
					do {
						StopTimes stopTimes = new StopTimes();
						stopTimes.setTrip_id(cursor.getString(0));
						stopTimes.setArrival_time(cursor.getString(1));
						stopTimes.setDeparture_time(cursor.getString(2));
						stopTimes.setStop_id(cursor.getString(3));
						stopTimes.setStop_sequence(cursor.getInt(4));
						stopTimes.setStop_headsign(cursor.getString(5));
						stopTimes.setPickup_time(cursor.getString(6));
						stopTimes.setDrop_off_type(cursor.getString(7));
						stopTimes.setShape_dist_traveled(cursor.getString(8));
						dataList.add(stopTimes);
					} while (cursor.moveToNext());
				}
			}

			/* next stops */
			ArrayList<String> nextStops = new ArrayList<String>();
			for (int i = 0; i < dataList.size(); i++) {
				StopTimes stopTimesOb = dataList.get(i);

				String trip_id = stopTimesOb.getTrip_id();
				int stop_seq = stopTimesOb.getStop_sequence();

				String selectQuerynextStops = "SELECT * FROM "
						+ TABLE_STOP_TIMES + "  WHERE " + KEY_TRIP_ID + "=\""
						+ trip_id + "\" ";

				// Log.e("selectQuerynextStops:", selectQuerynextStops);
				db = this.getWritableDatabase();
				cursor = db.rawQuery(selectQuerynextStops, null);
				if (cursor != null) {
					if (cursor.moveToFirst()) {
						do {
							StopTimes stopTimes = new StopTimes();
							stopTimes.setTrip_id(cursor.getString(0));
							stopTimes.setArrival_time(cursor.getString(1));
							stopTimes.setDeparture_time(cursor.getString(2));
							stopTimes.setStop_id(cursor.getString(3));
							stopTimes.setStop_sequence(cursor.getInt(4));
							stopTimes.setStop_headsign(cursor.getString(5));
							stopTimes.setPickup_time(cursor.getString(6));
							stopTimes.setDrop_off_type(cursor.getString(7));
							stopTimes.setShape_dist_traveled(cursor
									.getString(8));
							if (cursor.getInt(4) > stop_seq) {
								nextStops.add(cursor.getString(3));
							}
						} while (cursor.moveToNext());
					}
				}
			}

			/* get stop name from stop id */
			for (int i = 0; i < nextStops.size(); i++) {
				String selectQuerynextStops = "SELECT " + KEY_STOP_DESC
						+ " FROM " + TABLE_STOPS + "  WHERE " + KEY_STOP_ID
						+ " =\"" + nextStops.get(i) + "\" AND " + KEY_STOP_ID
						+ " LIKE '" + like + "%' ";
				db = this.getWritableDatabase();
				cursor = db.rawQuery(selectQuerynextStops, null);
				if (cursor.moveToFirst()) {
					do {
						String nxtStop = cursor.getString(0);
						if (!nextStopsString.contains(nxtStop)) {
							// Log.e(nxtStop, nxtStop);
							nextStopsString.add(nxtStop);
						}
					} while (cursor.moveToNext());
				}
			}
			Collections.sort(nextStopsString, String.CASE_INSENSITIVE_ORDER);
			if (nextStopsString.contains("Mandalay")) {
				nextStopsString.remove("Mandalay");
				nextStopsString.add(0, "Mandalay");
			}
			if (nextStopsString.contains("Yangon")) {
				nextStopsString.remove("Yangon");
				nextStopsString.add(0, "Yangon");
			}
			nextStopsString.add(0, "Going to...");
			if (nextStopsString.contains(spinnerOneStation)) {
				nextStopsString.remove(spinnerOneStation);
			}
		} else {
			nextStopsString.add(0, "Going to...");
		}
		return nextStopsString;
	}

	public static String getDateFormat(Context context, String string) {
		// 25/12/2013

		String segments[] = string.split("/");
		int day = Integer.parseInt(segments[segments.length - 3]);
		int month = Integer.parseInt(segments[segments.length - 2]);
		int year = Integer.parseInt(segments[segments.length - 1].trim());
		Calendar testDate = Calendar.getInstance();
		testDate.set(Calendar.YEAR, year);
		testDate.set(Calendar.MONTH, month - 1);
		testDate.set(Calendar.DAY_OF_MONTH, day);

		Format format = android.text.format.DateFormat.getDateFormat(context);
		String testDateFormat = format.format(testDate.getTime());
		String[] parts = testDateFormat.split("/");
		StringBuilder sb = new StringBuilder();
		for (String s : parts) {
			if (s.equals(day + "")) {
				sb.append("dd/");
			}
			if (s.equals(month + "")) {
				sb.append("MM/");
			}
			if (s.equals(year + "")) {
				sb.append("yyyy/");
			}
		}

		String df = "";
		try {
			df = sb.toString().substring(0, sb.toString().length() - 1);
			// Log.e("test", df);
		} catch (Exception e) {
			format = android.text.format.DateFormat.getDateFormat(context);
			sb.append("MM/");
			sb.append("dd/");
			sb.append("yyyy/");

			df = sb.toString().substring(0, sb.toString().length() - 1);
			// Log.e("test", df);
		}

		// Log.e("test", df);

		String[] partsd = df.split("/");
		StringBuilder sbd = new StringBuilder();
		for (String s : partsd) {
			if (s.equals("dd")) {
				sbd.append(day + "/");
			}
			if (s.equals("MM")) {
				sbd.append(month + "/");
			}
			if (s.equals("yyyy")) {
				sbd.append(year + "/");
			}
		}
		return sbd.toString().substring(0, sbd.toString().length() - 1);
	}

	public static String getDateFormatNew(Context context, String date,
			String years) {
		// 25/12/2013
		String dateToBeSent = "";
		Format dateFormat = android.text.format.DateFormat
				.getDateFormat(context);
		String pattern = ((SimpleDateFormat) dateFormat).toLocalizedPattern();
		// Log.e("datepattern", pattern);
		String dateArray[] = date.split("/");
		int day = 0, month = 0, year = 0;
		if (pattern.equals("dd/MM/yyyy")) {
			day = Integer.parseInt(dateArray[dateArray.length - 2]);
			month = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("d/MM/yyyy")) {
			day = Integer.parseInt(dateArray[dateArray.length - 2]);
			month = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("dd/M/yyyy")) {
			day = Integer.parseInt(dateArray[dateArray.length - 2]);
			month = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("d/M/yyyy")) {
			day = Integer.parseInt(dateArray[dateArray.length - 2]);
			month = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("MM/dd/yyyy")) {
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("M/dd/yyyy")) {
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("MM/d/yyyy")) {
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("M/d/yyyy")) {
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("M/d/y")) {
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("yyyy/MM/dd")) {
			year = Integer.parseInt(years.trim());
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("E, MMM d, yyyy")) {
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("E, d MMM yyyy")) {
			day = Integer.parseInt(dateArray[dateArray.length - 2]);
			month = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("EE,dd MMM yyyy")) {
			day = Integer.parseInt(dateArray[dateArray.length - 2]);
			month = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("yyyy MMM d, E")) {
			year = Integer.parseInt(years.trim());
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		}
		return dateToBeSent;
	}

	public ArrayList<Trips> getAllTrips(String string) {
		/* get all trips */
		ArrayList<Trips> dataList = new ArrayList<Trips>();
		String selectQuery = "SELECT  * FROM " + TABLE_TRIPS + "  WHERE "
				+ KEY_ROUTE_ID + " LIKE '" + string + "%' ";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Trips trips = new Trips();
				trips.setRoute_id(cursor.getString(0));
				trips.setService_id(cursor.getString(1));
				trips.setTrip_id(cursor.getString(2));
				trips.setTrip_headsign(cursor.getString(3));
				trips.setDirection_id(cursor.getString(4));
				dataList.add(trips);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		return dataList;
	}
}
