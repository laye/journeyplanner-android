/* 
 #Class name: Favourites
 #Class description: class to hold Favourites_item
 */
package com.myanmar.travel.planner;

public class Favourites {
	private String fav_value;
	private String fav_id;
	private String stop_id;

	public Favourites() {
	}

	public Favourites(String fav_id, String stop_id, String fav_value) {
		this.fav_id = fav_id;
		this.stop_id = stop_id;
		this.fav_value = fav_value;
	}

	public String getFav_Value() {
		return fav_value;
	}

	public void setFav_Value(String fav_value) {
		this.fav_value = fav_value;
	}

	public String getStop_id() {
		return this.stop_id;
	}

	public void setStop_id(String stop_id) {
		this.stop_id = stop_id;
	}

	public String getFav_Id() {
		return fav_id;
	}

	public void setFav_Id(String fav_id) {
		this.fav_id = fav_id;
	}
}
