package com.myanmar.travel.planner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FavouritesActivity extends Activity {
	static DatabaseMain databaseMain;
	static ArrayList<ListClass> listOfFav = new ArrayList<ListClass>();
	static ArrayList<ListClass> listOfFavSortByTime = new ArrayList<ListClass>();
	static ArrayList<String> listOfFavDates = new ArrayList<String>();
	static ListView lv;
	static ActionBar mActionBar;
	static String routeColor;
	static ListViewAdapterNewFavourites lvaAdapterNew;

	private static ArrayList<String> agency_names;
	private static ArrayList<String> city;
	private static ArrayList<String> time;
	static ArrayList<String> service_id;
	static ArrayList<String> trip_ids;
	static int count = 0;
	static List<Integer> Src_Sequence = null;
	static List<Integer> Dest_Sequence = null;
	private static List<String> Trip_id;
	private static List<String> Source_Id;
	private static List<String> Dest_Id;
	private static ArrayList<String> Service_Id;
	static String timeadd;

	static TextView tv1;
	static TextView tv2;
	static ImageView tv3;
	static ImageView iv1;
	static ImageView iv2;
	static ArrayList<RouteList> maproute = new ArrayList<RouteList>();
	static ListViewAdapterForMapRoute lva;
	static ListView listViewRoute;
	static Context mContext;
	static RelativeLayout helloLayout;
	static TextView textViewIsEmpty;
	static Resources resourceString;
	static ConnectionDetector connectionDetector;
	protected static int positionOfListview;
	boolean twentyFourHour;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favourites);
		mContext = this;
		connectionDetector = new ConnectionDetector(getApplicationContext());
		resourceString = getResources();
		mActionBar = getActionBar();
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setDisplayHomeAsUpEnabled(true);
		helloLayout = (RelativeLayout) findViewById(R.id.favouriteViewHelloLayout);
		mActionBar.setTitle(getResources().getString(R.string.favourites));
		lv = (ListView) findViewById(R.id.listViewFavourite);
		textViewIsEmpty = (TextView) findViewById(R.id.textViewIsEmpty);
		databaseMain = new DatabaseMain(getApplicationContext());
		listOfFav = databaseMain.getAllFavsWithDates();

		Collections.sort(listOfFav, new Comparator<ListClass>() {
			@SuppressLint("SimpleDateFormat")
			@Override
			public int compare(ListClass arg0, ListClass arg1) {
				SimpleDateFormat format = new SimpleDateFormat("d/MM/yyyy");

				Date date1;
				Date date2;
				try {
					date1 = format.parse(arg0.getDate());
					date2 = format.parse(arg1.getDate());
				} catch (ParseException e) {
					throw new IllegalArgumentException("Could not parse date!",
							e);
				}
				return date1.compareTo(date2);
			}
		});
		lvaAdapterNew = new ListViewAdapterNewFavourites(
				FavouritesActivity.this, listOfFav);
		lv.setAdapter(lvaAdapterNew);
		if (listOfFav.size() == 0) {
			textViewIsEmpty.setVisibility(View.VISIBLE);
		} else {
			textViewIsEmpty.setVisibility(View.GONE);
		}
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		int itemId = item.getItemId();
		switch (itemId) {
		case android.R.id.home:
			onBackPressed();
			break;
		}
		return true;
	}

	public static void replaceActionBar(final ArrayList<ListClass> list,
			int position) {
		/*
		 * replace the action bar title and the layout to display the details if
		 * the trip
		 */
		count = position;
		Trip_id = new ArrayList<String>();
		Source_Id = new ArrayList<String>();
		Dest_Id = new ArrayList<String>();
		Service_Id = new ArrayList<String>();
		agency_names = new ArrayList<String>();
		Src_Sequence = new ArrayList<Integer>();
		Dest_Sequence = new ArrayList<Integer>();

		for (int listCount = 0; listCount < list.size(); listCount++) {
			Trip_id.add(list.get(listCount).getTrip_id());
			Source_Id.add(list.get(listCount).getSource_stop_id());
			Dest_Id.add(list.get(listCount).getDest_stop_id());
			Service_Id.add(list.get(listCount).getService_id());
			String route_id = databaseMain.getRouteId(list.get(listCount)
					.getService_id(), list.get(listCount).getTrip_id());
			String agency_id = databaseMain.getAgencyId(route_id);
			agency_names.add(databaseMain.getAgencyName(agency_id));
			Src_Sequence.add(list.get(listCount).getSource_sequence());
			Dest_Sequence.add(list.get(listCount).getDest_sequence());
		}

		mActionBar.setTitle(resourceString.getString(R.string.journey_details));
		helloLayout.removeAllViews();
		helloLayout.removeAllViewsInLayout();
		lv.setVisibility(View.GONE);
		helloLayout.setVisibility(View.VISIBLE);
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.route, null, false);

		listViewRoute = (ListView) rowView.findViewById(R.id.listView1);
		tv1 = (TextView) rowView.findViewById(R.id.textView1);
		tv2 = (TextView) rowView.findViewById(R.id.textView2);
		tv3 = (ImageView) rowView.findViewById(R.id.textView3);
		iv1 = (ImageView) rowView.findViewById(R.id.imageView1);
		iv2 = (ImageView) rowView.findViewById(R.id.imageView2);

		tv1.setTextColor(Color.BLACK);
		tv2.setTextColor(Color.BLACK);
		tv1.setTextColor(Color.BLACK);
		routeColor = databaseMain.getRouteColor(
				list.get(count).getService_id(), list.get(count).getTrip_id());
		tv3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (connectionDetector.isConnectingToInternet()) {
					Toast.makeText(mContext,
							resourceString.getString(R.string.loading_map),
							Toast.LENGTH_LONG).show();
					Intent mapIntent = new Intent(mContext, MapActivity.class);
					mapIntent.putExtra("mapdata", new DataWrapper(maproute));
					mapIntent.putExtra("routeColor", routeColor);
					mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					view.getContext().startActivity(mapIntent);
				} else {
					Toast.makeText(mContext,
							resourceString.getString(R.string.no_internet),
							Toast.LENGTH_LONG).show();
				}
			}
		});

		tv1.setText(agency_names.get(count));
		Log.d("agency_names.get(count)", "agency_names.get(count)"
				+ agency_names.get(count));
		tv2.setText(Service_Id.get(count));
		loadList(count);

		if (position == list.size() - 1) {
			iv2.setImageResource(R.drawable.next_disabled);
			iv2.setClickable(false);
		}
		if (position == 0) {
			iv1.setImageResource(R.drawable.previous_disabled);
			iv1.setClickable(false);
		}
		iv1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (count - 1 >= 0) {
					iv2.setImageResource(R.drawable.next);
					iv2.setClickable(true);
					tv1.setText(agency_names.get(--count));
					tv2.setText(Service_Id.get(count));
					routeColor = databaseMain.getRouteColor(list.get(count)
							.getService_id(), list.get(count).getTrip_id());
					loadList(count);
					if (count == 0) {
						iv1.setImageResource(R.drawable.previous_disabled);
						iv1.setClickable(false);
					} else {
						iv1.setImageResource(R.drawable.previous);
						iv1.setClickable(true);
					}
				}
			}
		});

		iv2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (count + 1 < agency_names.size()) {
					iv1.setImageResource(R.drawable.previous);
					iv1.setClickable(true);
					tv1.setText(agency_names.get(++count));
					tv2.setText(Service_Id.get(count));
					routeColor = databaseMain.getRouteColor(list.get(count)
							.getService_id(), list.get(count).getTrip_id());
					loadList(count);

					if (count == agency_names.size() - 1) {
						iv2.setImageResource(R.drawable.next_disabled);
						iv2.setClickable(false);
					} else {
						iv2.setImageResource(R.drawable.next);
						iv2.setClickable(true);
					}
				}
			}
		});
		try {
			helloLayout.removeAllViews();
		} catch (Exception e) {
		}
		helloLayout.addView(rowView);
	}

	public static void loadAdapter() {
		listOfFav = databaseMain.getAllFavsWithDates();
		Collections.sort(listOfFav, new Comparator<ListClass>() {
			@SuppressLint("SimpleDateFormat")
			@Override
			public int compare(ListClass arg0, ListClass arg1) {
				SimpleDateFormat format = new SimpleDateFormat("d/MM/yyyy");

				Date date1;
				Date date2;
				try {
					date1 = format.parse(arg0.getDate());
					date2 = format.parse(arg1.getDate());
				} catch (ParseException e) {
					throw new IllegalArgumentException("Could not parse date!",
							e);
				}
				return date1.compareTo(date2);
			}
		});
		lvaAdapterNew = new ListViewAdapterNewFavourites(mContext, listOfFav);
		lv.setAdapter(lvaAdapterNew);
		if (listOfFav.size() == 0) {
			textViewIsEmpty.setVisibility(View.VISIBLE);
		} else {
			textViewIsEmpty.setVisibility(View.GONE);
		}
	}

	@SuppressLint("SimpleDateFormat")
	public static void loadList(int count) {
		Integer src_seq = Src_Sequence.get(count);
		Integer dest_seq = Dest_Sequence.get(count);


		ArrayList<StopTimes> st = new ArrayList<StopTimes>();
		st = databaseMain.getRoute(Trip_id.get(count));
		ArrayList<StopTimes> ft = new ArrayList<StopTimes>();

		for (int i = 0; i < st.size(); i++) {

			if (st.get(i).getStop_sequence() >= src_seq
					&& st.get(i).getStop_sequence() <= dest_seq) {
				StopTimes finalStops = new StopTimes();
				finalStops.setTrip_id(st.get(i).getTrip_id());
				finalStops.setArrival_time(st.get(i).getArrival_time());
				finalStops.setDeparture_time(st.get(i).getDeparture_time());
				finalStops.setStop_id(st.get(i).getStop_id());
				finalStops.setStop_sequence(st.get(i).getStop_sequence());
				ft.add(finalStops);
			}
		}

		city = new ArrayList<String>();
		time = new ArrayList<String>();
		maproute = new ArrayList<RouteList>();
		maproute.clear();
		boolean twentyFourHour = DateFormat.is24HourFormat(mContext);
		if (twentyFourHour) {
			for (int i = 0; i < ft.size(); i++) {
				RouteList routcord = databaseMain.getCordinates(ft.get(i)
						.getStop_id());
				if (i == 0) {
					city.add(routcord.getCity());
					time.add(ft.get(i).getDeparture_time());
				} else if (i == ft.size() - 1) {
					city.add(routcord.getCity());
					StringTokenizer tk = null;
					Log.e("ft.get(i).getArrival_time()", ft.get(i)
							.getArrival_time());
					if (ft.get(i).getArrival_time().length() == 5) {
						tk = new StringTokenizer(ft.get(i).getArrival_time()
								+ ":00");
					} else {
						tk = new StringTokenizer(ft.get(i).getArrival_time());
					}
					String times = tk.nextToken();
					int hourTimes = Integer.parseInt(times.substring(0, 2));
					hourTimes = hourTimes % 24;
					String minuteTimes = times.substring(3, 5);
					String arrivalString = hourTimes + ":" + minuteTimes;
					String hourTimesString = hourTimes + "";
					if (hourTimesString.length() == 1) {
						arrivalString = "0" + hourTimes + ":" + minuteTimes;
					}
					time.add(arrivalString);
					Log.d("hourTimes : minuteTimes times", hourTimes + ":"
							+ minuteTimes);
				} else {
					city.add(routcord.getCity());
					StringTokenizer tk = null;
					Log.e("ft.get(i).getArrival_time()", ft.get(i)
							.getArrival_time());
					if (ft.get(i).getArrival_time().length() == 5) {
						tk = new StringTokenizer(ft.get(i).getArrival_time()
								+ ":00");
					} else {
						tk = new StringTokenizer(ft.get(i).getArrival_time());
					}
					String times = tk.nextToken();
					int hourTimes = Integer.parseInt(times.substring(0, 2));
					hourTimes = hourTimes % 24;
					String minuteTimes = times.substring(3, 5);
					String hourTimesString = hourTimes + "";
					String arrivalString = hourTimes + ":" + minuteTimes;
					if (hourTimesString.length() == 1) {
						arrivalString = "0" + hourTimes + ":" + minuteTimes;
					}

					tk = null;
					Log.e("ft.get(i).getDeparture_time()", ft.get(i)
							.getDeparture_time());
					if (ft.get(i).getDeparture_time().length() == 5) {
						tk = new StringTokenizer(ft.get(i).getDeparture_time()
								+ ":00");
					} else {
						tk = new StringTokenizer(ft.get(i).getDeparture_time());
					}
					times = tk.nextToken();
					hourTimes = Integer.parseInt(times.substring(0, 2));
					hourTimes = hourTimes % 24;
					minuteTimes = times.substring(3, 5);
					hourTimesString = hourTimes + "";
					String departureString = hourTimes + ":" + minuteTimes;
					if (hourTimesString.length() == 1) {
						departureString = "0" + hourTimes + ":" + minuteTimes;
					}
					time.add(arrivalString + "-" + departureString);
				}
				maproute.add(routcord);
			}
		} else {
			for (int i = 0; i < ft.size(); i++) {
				RouteList routcord = databaseMain.getCordinates(ft.get(i)
						.getStop_id());
				timeadd = "--";
				if (i == 0) {
					city.add(routcord.getCity());
					StringTokenizer tk = new StringTokenizer(ft.get(i)
							.getDeparture_time() + ":00");
					String times = tk.nextToken();
					SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
					SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
					SimpleDateFormat sdfss = new SimpleDateFormat("hh:mm");
					Date dt;
					try {
						dt = sdf.parse(times);
						timeadd = sdfs.format(dt);
						if (times.length() == 8) {
							if (times.substring(0, 1).equals("12")) {
								timeadd = sdfss.format(dt) + " PM";
							}
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
					time.add(timeadd);
				} else if (i == ft.size() - 1) {
					city.add(routcord.getCity());
					StringTokenizer tk = new StringTokenizer(ft.get(i)
							.getArrival_time() + ":00");
					String times = tk.nextToken();
					SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
					SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
					SimpleDateFormat sdfss = new SimpleDateFormat("hh:mm");
					Date dt;
					try {
						dt = sdf.parse(times);
						timeadd = sdfs.format(dt);
						if (times.length() == 8) {
							// Log.e("ckeck", times.substring(0, 2));
							if (times.substring(0, 2).equals("12")) {
								timeadd = sdfss.format(dt) + " PM";
							}
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
					time.add(timeadd);
				} else {
					city.add(routcord.getCity());

					StringTokenizer tk = null;
					if (ft.get(i).getArrival_time().length() == 8
							&& ft.get(i).getDeparture_time().length() == 5) {
						tk = new StringTokenizer(ft.get(i).getArrival_time()
								+ " " + ft.get(i).getDeparture_time() + ":00");
					} else if (ft.get(i).getArrival_time().length() == 5
							&& ft.get(i).getDeparture_time().length() == 8) {
						tk = new StringTokenizer(ft.get(i).getArrival_time()
								+ ":00" + " " + ft.get(i).getDeparture_time());
					} else if (ft.get(i).getArrival_time().length() == 8
							&& ft.get(i).getDeparture_time().length() == 8) {
						tk = new StringTokenizer(ft.get(i).getArrival_time()
								+ " " + ft.get(i).getDeparture_time());
					} else if (ft.get(i).getArrival_time().length() == 5
							&& ft.get(i).getDeparture_time().length() == 5) {
						tk = new StringTokenizer(ft.get(i).getArrival_time()
								+ ":00" + " " + ft.get(i).getDeparture_time()
								+ ":00");
					}
					if (tk != null) {
						if (tk.hasMoreTokens()) {
							String times = tk.nextToken();
							SimpleDateFormat sdf = new SimpleDateFormat(
									"hh:mm:ss");
							SimpleDateFormat sdfs = new SimpleDateFormat(
									"hh:mm a");
							SimpleDateFormat sdfss = new SimpleDateFormat(
									"hh:mm");
							Date dt;
							try {
								dt = sdf.parse(times);
								timeadd = sdfs.format(dt);
								// Log.d("ckeck", times);
								if (times.length() == 8) {
									// Log.e("ckeck", times.substring(0, 2));
									if (times.substring(0, 2).equals("12")) {
										timeadd = sdfss.format(dt) + " PM";
									}
								}
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
						if (tk.hasMoreTokens()) {
							String times = tk.nextToken();
							SimpleDateFormat sdf = new SimpleDateFormat(
									"hh:mm:ss");
							SimpleDateFormat sdfs = new SimpleDateFormat(
									"hh:mm a");
							SimpleDateFormat sdfss = new SimpleDateFormat(
									"hh:mm");
							Date dt;
							try {
								dt = sdf.parse(times);
								timeadd = timeadd + " - " + sdfs.format(dt);
								// Log.d("ckeck", times);
								if (times.length() == 8) {
									// Log.e("ckeck", times.substring(0, 1));
									if (times.substring(0, 2).equals("12")) {
										timeadd = sdfss.format(dt) + " PM";
									}
								}
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
					}
					time.add(timeadd);
				}
				maproute.add(routcord);
			}
		}
		loadAdapterRoute();
	}

	public static void loadAdapterRoute() {
		lva = new ListViewAdapterForMapRoute(mContext, city, time);
		listViewRoute.setAdapter(lva);
	}

	@Override
	public void onBackPressed() {
		if (mActionBar.getTitle().equals(
				resourceString.getString(R.string.journey_details))) {
			mActionBar.setTitle(resourceString.getString(R.string.favourites));
			helloLayout.removeAllViews();
			helloLayout.removeAllViewsInLayout();
			lv.setVisibility(View.VISIBLE);
			helloLayout.setVisibility(View.GONE);
		} else {
			super.onBackPressed();
		}
	}

	public static void favClickWhenLayoutOpen() {
		if (mActionBar.getTitle().equals(
				resourceString.getString(R.string.journey_details))) {
			mActionBar.setTitle(resourceString.getString(R.string.favourites));
			helloLayout.removeAllViews();
			helloLayout.removeAllViewsInLayout();
			lv.setVisibility(View.VISIBLE);
			helloLayout.setVisibility(View.GONE);
		}
	}
}
