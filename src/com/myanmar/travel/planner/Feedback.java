/* 
 #Class name: Feedback
 #Class description: class to send feedback of the app
 */

package com.myanmar.travel.planner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Feedback extends Activity {

	static ActionBar mActionBar;
	Button sendEmail;
	EditText editTextEmail;
	EditText editTextFeedback;
	String emailPattern;
	boolean email;
	boolean feedback;

	ConnectionDetector detector;
	Toast toast;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback);

		mActionBar = getActionBar();
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle(getResources().getString(R.string.feedback));
		sendEmail = (Button) findViewById(R.id.sendEmail);
		editTextFeedback = (EditText) findViewById(R.id.editTextFeedback);
		editTextEmail = (EditText) findViewById(R.id.editTextEmail);
		emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
		detector = new ConnectionDetector(getApplicationContext());
		sendEmail.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				/*
				 * check if email is blank
				 */
				if (editTextEmail.getText().toString().equals("")) {
					email = true;
				} else { /*
						 * email validation
						 */
					if (editTextEmail.getText().toString()
							.matches(emailPattern)) {
						editTextEmail.setError(null);
						email = true;
					} else { /*
							 * set error
							 */
						email = false;
						editTextEmail.requestFocus();
						editTextEmail.setError(getResources().getString(
								R.string.invalid_email_address));
					}
				}
				if (editTextFeedback.getText().toString().trim().equals("")) { /*
																				 * check
																				 * feedback
																				 * is
																				 * empty
																				 */

					editTextFeedback.requestFocus();
					editTextFeedback.setError(getResources().getString(
							R.string.enter_feedback));
					feedback = false;
				} else {
					editTextFeedback.setError(null);
					feedback = true;
				}
				if (email == true && feedback == true) {
					// if (feedback == true) {
					if (detector.isConnectingToInternet()) {
						sendEmail();
					} else {
						if (toast == null
								|| toast.getView().getWindowVisibility() != View.VISIBLE) {
							toast = Toast.makeText(
									getApplicationContext(),
									getResources().getString(
											R.string.no_internet),
									Toast.LENGTH_SHORT);
							toast.show();
						}
					}
				}
			}
		});
		editTextFeedback.addTextChangedListener(textWatcher);
		editTextEmail.addTextChangedListener(textWatcher);
	}

	TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			editTextEmail.setError(null);
			editTextFeedback.setError(null);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	protected void sendEmail() {
		// Session session = createSessionObject();
		String msgText = "Feedback: " + editTextFeedback.getText().toString();
		if (!editTextEmail.getText().toString().equals("")) {
			msgText = msgText + "\n";
			msgText = msgText + "\n att \n" + "Email: "
					+ editTextEmail.getText().toString();
		}

		new SendPHPMail().execute("Feedback from Myanmar Travel Planner user",
				msgText,
				getResources().getString(R.string.email_address_FeedBack));
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		/*
		 * on selected menu item choose what to do
		 */
		int itemId = item.getItemId();
		switch (itemId) {
		case android.R.id.home:
			onBackPressed();
			break;
		}

		return true;
	}

	private class SendPHPMail extends AsyncTask<String, Integer, Double> {
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Feedback.this, getResources()
					.getString(R.string.please_wait),
					getResources().getString(R.string.feedback_sending), true,
					false);
		}

		@Override
		protected Double doInBackground(String... params) {

			postData(params[0], params[1], params[2]);
			return null;
		}

		protected void onPostExecute(Double result) {
			progressDialog.dismiss();
		}

		protected void onProgressUpdate(Integer... progress) {
			progressDialog.setProgress(progress[0]);
		}

		public void postData(String subject, String body, String to) {
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://targetprogress.in/Horticulture/send_email/send_email.php");

			try {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("subject", subject));
				nameValuePairs.add(new BasicNameValuePair("body", body));
				nameValuePairs.add(new BasicNameValuePair("to", to));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				String result = EntityUtils.toString(entity);
				Log.e("response", result);
				if (!result.equals("success")) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(
									Feedback.this,
									getResources().getString(
											R.string.feedback_failed),
									// e.toString(),
									Toast.LENGTH_LONG + Toast.LENGTH_LONG)
									.show();
						}
					});
				} else if (result.equals("success")) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							Toast.makeText(
									Feedback.this,
									getResources().getString(
											R.string.feedback_sent),
									Toast.LENGTH_LONG).show();
							editTextFeedback.setText("");
							editTextEmail.setText("");
						}
					});
				}

			} catch (ClientProtocolException e) {
			} catch (IOException e) {
			}
		}

	}
}
