/* 
 #Class name: FixedDatePickerDialog
 #Class description: class to make custom dialogue for alert
 */

package com.myanmar.travel.planner;

import java.lang.reflect.Field;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.widget.DatePicker;

public class FixedDatePickerDialog extends DatePickerDialog implements
		OnCancelListener {

	public FixedDatePickerDialog(Context context, OnDateSetListener callBack,
			int year, int monthOfYear, int dayOfMonth) {
		super(context, null, year, monthOfYear, dayOfMonth);
		initializePicker(callBack);
	}

	public FixedDatePickerDialog(Context context, int theme,
			OnDateSetListener callBack, int year, int monthOfYear,
			int dayOfMonth) {
		super(context, theme, null, year, monthOfYear, dayOfMonth);
		initializePicker(callBack);
	}

	private void initializePicker(final OnDateSetListener callback) {
		try {

			// If you're only using Honeycomb+ then you can just call
			// getDatePicker() instead of using reflection
			Field pickerField = DatePickerDialog.class
					.getDeclaredField("mDatePicker");
			pickerField.setAccessible(true);
			final DatePicker picker = (DatePicker) pickerField.get(this);
			this.setCancelable(true);
			this.setButton(DialogInterface.BUTTON_NEGATIVE, getContext()
					.getText(android.R.string.cancel), new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

					onCancel(dialog);

				}
			});
			this.setButton(DialogInterface.BUTTON_POSITIVE, getContext()
					.getText(android.R.string.ok),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							picker.clearFocus(); // Focus must be cleared so the
													// value change listener is
													// called
							callback.onDateSet(picker, picker.getYear(),
									picker.getMonth(), picker.getDayOfMonth());
						}
					});
		} catch (Exception e) { /* Reflection probably failed */
		}
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		dialog.cancel();
	}
}
