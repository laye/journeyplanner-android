/* 
 #Class name: ListClass
 #Class description: class to hold ListClassObject
 */

package com.myanmar.travel.planner;

public class ListClass {
	String source_stop_id, dest_stop_id, source_stop_name, dest_stop_name,
			source_dept_time, dest_arrival_time, trip_id, service_id;
	int source_sequence, dest_sequence;
	String date;
	boolean afterDate;

	public boolean isAfterDate() {
		return afterDate;
	}

	public void setAfterDate(boolean afterDate) {
		this.afterDate = afterDate;
	}

	public ListClass() {
	}

	public ListClass(String source_stop_id, String dest_stop_id,
			String source_stop_name, String dest_stop_name,
			String source_dept_time, String dest_arrival_time, String trip_id,
			String service_id, int source_sequence, int dest_sequence,
			String date) {
		this.source_stop_id = source_stop_id;
		this.dest_stop_id = dest_stop_id;
		this.source_stop_name = source_stop_name;
		this.dest_stop_name = dest_stop_name;
		this.source_dept_time = source_dept_time;
		this.dest_arrival_time = dest_arrival_time;
		this.trip_id = trip_id;
		this.service_id = service_id;
		this.source_sequence = source_sequence;
		this.dest_sequence = dest_sequence;
		this.date = date;
	}

	public String getService_id() {
		return service_id;
	}

	public void setService_id(String service_id) {
		this.service_id = service_id;
	}

	public String getTrip_id() {
		return trip_id;
	}

	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}

	public String getSource_stop_id() {
		return source_stop_id;
	}

	public void setSource_stop_id(String source_stop_id) {
		this.source_stop_id = source_stop_id;
	}

	public String getDest_stop_id() {
		return dest_stop_id;
	}

	public void setDest_stop_id(String dest_stop_id) {
		this.dest_stop_id = dest_stop_id;
	}

	public String getSource_stop_name() {
		return source_stop_name;
	}

	public void setSource_stop_name(String source_stop_name) {
		this.source_stop_name = source_stop_name;
	}

	public String getDest_stop_name() {
		return dest_stop_name;
	}

	public void setDest_stop_name(String dest_stop_name) {
		this.dest_stop_name = dest_stop_name;
	}

	public String getSource_dept_time() {
		return source_dept_time;
	}

	public void setSource_dept_time(String source_dept_time) {
		this.source_dept_time = source_dept_time;
	}

	public String getDest_arrival_time() {
		return dest_arrival_time;
	}

	public void setDest_arrival_time(String dest_arrival_time) {
		this.dest_arrival_time = dest_arrival_time;
	}

	public int getSource_sequence() {
		return source_sequence;
	}

	public void setSource_sequence(int source_sequence) {
		this.source_sequence = source_sequence;
	}

	public int getDest_sequence() {
		return dest_sequence;
	}

	public void setDest_sequence(int dest_sequence) {
		this.dest_sequence = dest_sequence;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
