/* 
 #Class name: ListViewAdapterForMapRoute
 #Class description: custom adapter for maproutes
 */

package com.myanmar.travel.planner;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListViewAdapterForMapRoute extends ArrayAdapter<String> {

	private final Context context;
	private final ArrayList<String> values;
	private final ArrayList<String> values1;

	public ListViewAdapterForMapRoute(Context context,
			ArrayList<String> values, ArrayList<String> values1) {
		super(context, R.layout.newlistview, values);
		this.context = context;
		this.values = values;
		this.values1 = values1;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		/* inflate layout */
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.newlistview, parent, false);

		TextView textView1 = (TextView) rowView.findViewById(R.id.textView1);
		TextView textView2 = (TextView) rowView.findViewById(R.id.textView2);
		TextView textView3 = (TextView) rowView.findViewById(R.id.textView3);
		ImageView imageView2 = (ImageView) rowView
				.findViewById(R.id.imageView2);
		/*
		 * set images for the imageview depending on their positions
		 */
		if (values.size() % 2 == 0) {
			if (values.size() == 2) {
				if (position == 0) {
					textView1.setText(values.get(position).trim());
					textView2.setText(values1.get(position).trim());
					imageView2.setImageResource(R.drawable.newfirst);
				} else {
					textView1.setText(values.get(position).trim());
					textView2.setText(values1.get(position).trim());
					imageView2.setImageResource(R.drawable.newlast);
				}
			} else {
				if (position == 0) {
					textView1.setText(values.get(position).trim());
					textView2.setText(values1.get(position).trim());
					imageView2.setImageResource(R.drawable.newfirst);
				} else if (position == values.size() - 1) {
					textView1.setText(values.get(position).trim());
					textView2.setText(values1.get(position).trim());
					imageView2.setImageResource(R.drawable.newlast);
				}
				// else if (position == ((values.size() / 2) - 1)) {
				// textView1.setText(values.get(position));
				// textView2.setText(values1.get(position));
				// imageView2.setImageResource(R.drawable.newmiddle);
				// } else if (position == ((values.size() / 2))) {
				// textView1.setText(values.get(position));
				// textView2.setText(values1.get(position));
				// imageView2.setImageResource(R.drawable.newmiddle);
				// }
				else {
					String segments[] = values1.get(position).split("-");
					textView1.setText(values.get(position).trim());
					textView2.setText(segments[segments.length - 2].trim());
					textView3.setText(segments[segments.length - 1].trim());
					imageView2.setImageResource(R.drawable.newmiddle);
				}
			}
		} else if (position == 0) {
			textView1.setText(values.get(position).trim());
			textView2.setText(values1.get(position).trim());
			imageView2.setImageResource(R.drawable.newfirst);
		} else if (position == values.size() - 1) {
			textView1.setText(values.get(position).trim());
			textView2.setText(values1.get(position).trim());
			imageView2.setImageResource(R.drawable.newlast);
		} else if (position == (values.size() - 1) / 2) {
			String segments[] = values1.get(position).split("-");
			textView1.setText(values.get(position));
			textView2.setText(segments[segments.length - 2].trim());
			textView3.setText(segments[segments.length - 1].trim());
			imageView2.setImageResource(R.drawable.newmiddle);
		} else {
			String segments[] = values1.get(position).split("-");
			textView1.setText(values.get(position));
			textView2.setText(segments[segments.length - 2].trim());
			textView3.setText(segments[segments.length - 1].trim());
			imageView2.setImageResource(R.drawable.newmiddle);
		}
		return rowView;
	}

}
