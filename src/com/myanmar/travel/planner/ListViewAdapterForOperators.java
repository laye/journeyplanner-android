/* 
 #Class name: ListViewAdapterForOperators
 #Class description: custom adapter for Operators
 */

package com.myanmar.travel.planner;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListViewAdapterForOperators extends ArrayAdapter<Agency> {

	private Context context;
	TextView textViewOperatorName;
	ImageView imageViewOperatorImage;
	ArrayList<Agency> list;
	DatabaseMain databaseMain;
	int positionSelected;

	public ListViewAdapterForOperators(Context context, ArrayList<Agency> list) {
		super(context, R.layout.rowlayout, list);
		this.context = context;
		this.list = list;
	}

	@SuppressLint("DefaultLocale")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		/* inflate layout */
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.operator_list, parent, false);

		textViewOperatorName = (TextView) rowView
				.findViewById(R.id.textViewOperatorName);
		imageViewOperatorImage = (ImageView) rowView
				.findViewById(R.id.imageViewOperatorImage);

		textViewOperatorName.setText(list.get(position).getAgency_name());
		/*
		 * set image with agency id from drawable
		 */
		String agency_id = list.get(position).getAgency_id();
		final String agency_id_send = agency_id;
		agency_id = agency_id.toLowerCase();
		Context mcontext = imageViewOperatorImage.getContext();
		int id = mcontext.getResources().getIdentifier(agency_id, "drawable",
				mcontext.getPackageName());

		imageViewOperatorImage.setImageResource(id);

		rowView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent newiIntent = new Intent(context.getApplicationContext(),
						OperatorDetails.class);
				newiIntent.putExtra("agency_id", agency_id_send);/*
																 * open operator
																 * details
																 * layout
																 */
				context.startActivity(newiIntent);
			}
		});

		return rowView;
	}
}
