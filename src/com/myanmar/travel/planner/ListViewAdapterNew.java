/* 
 #Class name: ListViewAdapterNew
 #Class description: custom adapter for route list
 */

package com.myanmar.travel.planner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ListViewAdapterNew extends ArrayAdapter<ListClass> {

	private Context context;
	private ImageView imageView2;
	private ImageView textView3;
	ArrayList<ListClass> list;
	DatabaseMain databaseMain;
	ImageView imageView1;
	String timedestarrival, timesrcdept;
	boolean twentyFourHour;
	int positionSelected;
	private boolean nextDay;
	ConnectionDetector connectionDetector;

	enum Week {
		Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
	};

	public ListViewAdapterNew(Context context, ArrayList<ListClass> list) {
		super(context, R.layout.rowlayout, list);
		connectionDetector = new ConnectionDetector(context);
		this.context = context;
		this.list = list;
	}

	@SuppressLint({ "SimpleDateFormat", "DefaultLocale" })
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		/* store position inflate layout */
		positionSelected = position;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.best_newrowlayout, parent,
				false);

		// LinearLayout bgchange = (LinearLayout) rowView
		// .findViewById(R.id.bgchange);
		ImageView imageViewHiddenwarn = (ImageView) rowView
				.findViewById(R.id.imageViewHiddenwarn);
		if (list.get(position).isAfterDate()) {
			imageViewHiddenwarn.setVisibility(View.VISIBLE);
		}
		imageView1 = (ImageView) rowView.findViewById(R.id.imageView1);
		TextView textView1 = (TextView) rowView.findViewById(R.id.textView1);
		TextView textView5 = (TextView) rowView.findViewById(R.id.textView2);
		textView3 = (ImageView) rowView.findViewById(R.id.textView3);
		TextView textView4 = (TextView) rowView.findViewById(R.id.textView4);
		TextView textView2 = (TextView) rowView.findViewById(R.id.textView5);
		ImageView textView6 = (ImageView) rowView.findViewById(R.id.textView6);
		TextView textView8 = (TextView) rowView.findViewById(R.id.textView8);
		TextView textView9 = (TextView) rowView.findViewById(R.id.textView9);
		TextView textView10 = (TextView) rowView.findViewById(R.id.textView10);
		imageView2 = (ImageView) rowView.findViewById(R.id.imageView2);

		textView1.setTextColor(Color.BLACK);
		textView5.setTextColor(Color.BLACK);
		textView4.setTextColor(Color.BLACK);
		textView2.setTextColor(Color.BLACK);
		textView9.setTextColor(Color.BLACK);
		textView10.setTextColor(Color.BLACK);

		textView1.setText(list.get(position).getSource_stop_name());
		textView4.setText(list.get(position).getDest_stop_name());

		databaseMain = new DatabaseMain(context);
		String Dest_arrival_time = list.get(position).getDest_arrival_time();
		String segmentsDest_arrival_time[] = Dest_arrival_time.split(":");

		int day = Integer.parseInt(MainActivity.currentDay.trim());
		int month = Integer.parseInt(MainActivity.currentMonth.trim());
		int year = Integer.parseInt(MainActivity.currentYear.trim());
		Calendar calendar1 = Calendar.getInstance();
		try {
			int hh = 0;
			int mm = 0;
			try {
				hh = Integer
						.parseInt(segmentsDest_arrival_time[segmentsDest_arrival_time.length - 2]);
				mm = Integer
						.parseInt(segmentsDest_arrival_time[segmentsDest_arrival_time.length - 1]);
			} catch (Exception e) {
			}
			calendar1.set(year, month, day, hh, mm + 1);
		} catch (NumberFormatException e1) {
		}

		String Dest_stop_name = list.get(position).getSource_dept_time();
		String segmentsDest_stop_name[] = Dest_stop_name.split(":");

		Calendar calendar2 = Calendar.getInstance();
		try {
			calendar2
					.set(year,
							month,
							day,
							Integer.parseInt(segmentsDest_stop_name[segmentsDest_stop_name.length - 2]),
							Integer.parseInt(segmentsDest_stop_name[segmentsDest_stop_name.length - 1]));
		} catch (NumberFormatException e1) {
		}

		String startTime = list.get(position).getDest_arrival_time() + ":00";
		twentyFourHour = DateFormat.is24HourFormat(context); /*
															 * check if phone
															 * date is 24 format
															 */
		if (!twentyFourHour) {
			StringTokenizer tk = new StringTokenizer(startTime);
			String time = tk.nextToken();
			SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
			SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm aa");
			SimpleDateFormat sdfss = new SimpleDateFormat("hh:mm");

			Date dt;
			try {
				dt = sdf.parse(time);
				timesrcdept = sdfs.format(dt);
				if (time.length() == 8) {
					if (time.substring(0, 2).equals("12")) {
						timesrcdept = sdfss.format(dt) + " PM";
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			textView2.setText(timesrcdept);
		} else {
			StringTokenizer tk = null;
			// Log.e("list.get(i).getArrival_time()", list.get(position)
			// .getSource_dept_time());
			if (list.get(position).getSource_dept_time().length() == 5) {
				tk = new StringTokenizer(list.get(position)
						.getDest_arrival_time() + ":00");
			} else {
				tk = new StringTokenizer(list.get(position)
						.getDest_arrival_time());
			}
			if (tk.hasMoreTokens()) {
				String times = tk.nextToken();
				int hourTimes = Integer.parseInt(times.substring(0, 2));
				hourTimes = hourTimes % 24;
				String minuteTimes = times.substring(3, 5);
				String arrivalString = hourTimes + ":" + minuteTimes;
				String hourTimesString = hourTimes + "";
				if (hourTimesString.length() == 1) {
					arrivalString = "0" + hourTimes + ":" + minuteTimes;
				}
				textView2.setText(arrivalString);
			}
		}
		if (checkDate() == true) {
			textView8.setVisibility(View.VISIBLE);
		} else {
			textView8.setVisibility(View.INVISIBLE);
		}
		String route_id = databaseMain.getRouteId(list.get(positionSelected)
				.getService_id(), list.get(positionSelected).getTrip_id());
		String agency_id = databaseMain.getAgencyId(route_id);
		final String agency_id_send = agency_id;
		agency_id = agency_id.toLowerCase();
		Context mcontext = imageView1.getContext();
		int id = mcontext.getResources().getIdentifier(agency_id, "drawable",
				mcontext.getPackageName());
		imageView1.setImageResource(id);
		imageView1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent newiIntent = new Intent(context.getApplicationContext(),
						OperatorDetails.class);
				newiIntent.putExtra("agency_id", agency_id_send);/*
																 * open operator
																 * details
																 * layout
																 */
				context.startActivity(newiIntent);
			}
		});

		textView9.setText(list.get(position).getService_id());
		textView10.setText(getStops(list.get(position).getService_id()) + "");

		startTime = list.get(position).getSource_dept_time() + ":00";
		twentyFourHour = DateFormat.is24HourFormat(context); /*
															 * check if phone
															 * date is 24 format
															 */
		if (!twentyFourHour) {
			StringTokenizer tk = new StringTokenizer(startTime);
			String time = tk.nextToken();
			SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
			SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
			SimpleDateFormat sdfss = new SimpleDateFormat("hh:mm");
			Date dt;
			try {
				dt = sdf.parse(time);
				if (time.length() == 8) {
					if (time.substring(0, 2).equals("12")) {
						timesrcdept = sdfss.format(dt) + " PM";
					}
				}
				timedestarrival = sdfs.format(dt);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			textView5.setText(timedestarrival);
		} else {
			StringTokenizer tk = null;
			// Log.e("list.get(i).getArrival_time()", list.get(position)
			// .getSource_dept_time());
			if (list.get(position).getSource_dept_time().length() == 5) {
				tk = new StringTokenizer(list.get(position)
						.getSource_dept_time() + ":00");
			} else {
				tk = new StringTokenizer(list.get(position)
						.getSource_dept_time());
			}
			String times = tk.nextToken();
			int hourTimes = Integer.parseInt(times.substring(0, 2));
			hourTimes = hourTimes % 24;
			String minuteTimes = times.substring(3, 5);
			String arrivalString = hourTimes + ":" + minuteTimes;
			String hourTimesString = hourTimes + "";
			if (hourTimesString.length() == 1) {
				arrivalString = "0" + hourTimes + ":" + minuteTimes;
			}
			textView5.setText(arrivalString);
		}

		/*
		 * check favourite exists in database
		 */
		if (databaseMain.favExists(list.get(position), MainActivity.currentDay,
				MainActivity.currentMonth, MainActivity.currentYear) == true) {
			imageView2.setBackgroundResource(R.drawable.fav);
		} else {
			imageView2.setBackgroundResource(R.drawable.favourites);
		}

		imageView2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (databaseMain.favExists(list.get(position), /*
																 * delete
																 * favourite
																 */
						MainActivity.currentDay, MainActivity.currentMonth,
						MainActivity.currentYear) == true) {
					((ImageView) v)
							.setBackgroundResource(R.drawable.favourites);

					int day = Integer.parseInt(MainActivity.currentDay.trim());
					int month = Integer.parseInt(MainActivity.currentMonth
							.trim());
					int year = Integer.parseInt(MainActivity.currentYear.trim());
					String date = day + "/" + month + "/" + year;
					databaseMain.deleteFavourite(list.get(position), date);
				} else { /*
						 * add favourite
						 */
					int day = Integer.parseInt(MainActivity.currentDay.trim());
					int month = Integer.parseInt(MainActivity.currentMonth
							.trim());
					int year = Integer.parseInt(MainActivity.currentYear.trim());
					String date = day + "/" + month + "/" + year;
					((ImageView) v).setBackgroundResource(R.drawable.fav);
					Log.e("fav date " + date, date);
					databaseMain.addFavourite(list.get(position), date);
				}
			}
		});

		textView3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) { /*
											 * load map with city lat long
											 */
				if (MainActivity.mapOrListClicked == false) {
					MainActivity.mapOrListClicked = true;
					if (connectionDetector.isConnectingToInternet()) {
						Toast.makeText(
								context,
								context.getResources().getString(
										R.string.loading_map),
								Toast.LENGTH_LONG).show();
						Intent intent = new Intent(context,
								MapPointActivity.class);
						String routeColor = databaseMain.getRouteColor(list
								.get(position).getService_id(),
								list.get(position).getTrip_id());
						String lat = databaseMain.getLat(list.get(position)
								.getSource_stop_id());
						String longitu = databaseMain.getLong(list
								.get(position).getSource_stop_id());
						intent.putExtra("maplat", lat);
						intent.putExtra("maplongt", longitu);
						intent.putExtra("routeColor", routeColor);

						intent.putExtra("mapcity", databaseMain.getCity(list
								.get(position).getSource_stop_id()));
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						view.getContext().startActivity(intent);
					} else {
						Toast.makeText(
								context,
								context.getResources().getString(
										R.string.no_internet),
								Toast.LENGTH_LONG).show();
						MainActivity.mapOrListClicked = false;
					}
				}
			}
		});

		textView6.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {/*
											 * load map with city lat long
											 */
				if (MainActivity.mapOrListClicked == false) {
					MainActivity.mapOrListClicked = true;
					if (connectionDetector.isConnectingToInternet()) {
						Toast.makeText(
								context,
								context.getResources().getString(
										R.string.loading_map),
								Toast.LENGTH_LONG).show();
						Intent intent = new Intent(context,
								MapPointActivity.class);
						intent.putExtra("maplat", databaseMain.getLat(list.get(
								position).getDest_stop_id()));
						intent.putExtra("maplongt", databaseMain.getLong(list
								.get(position).getDest_stop_id()));
						intent.putExtra("mapcity", databaseMain.getCity(list
								.get(position).getDest_stop_id()));
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						view.getContext().startActivity(intent);
					} else {
						Toast.makeText(
								context,
								context.getResources().getString(
										R.string.no_internet),
								Toast.LENGTH_LONG).show();
						MainActivity.mapOrListClicked = false;
					}
				}
			}
		});

		imageViewHiddenwarn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String a = databaseMain.getEndDateOf(list.get(position)
						.getService_id());
				String b = databaseMain.getStartDateOf(list.get(position)
						.getService_id());
				int yra = Integer.parseInt(a.substring(0, 4));
				int montha = Integer.parseInt(a.substring(4, 6));
				int dtea = Integer.parseInt(a.substring(6, 8));
				int yrb = Integer.parseInt(b.substring(0, 4));
				int monthb = Integer.parseInt(b.substring(4, 6));
				int dteb = Integer.parseInt(b.substring(6, 8));
				java.text.DateFormat dateFormat = android.text.format.DateFormat
						.getDateFormat(context);
				Calendar calendara = Calendar.getInstance();
				Calendar calendarb = Calendar.getInstance();
				calendara.set(yra, montha - 1, dtea);
				calendarb.set(yrb, monthb - 1, dteb);
				String outputFormata = dateFormat.format(calendara.getTime())
						.toString();
				String outputFormatb = dateFormat.format(calendarb.getTime())
						.toString();
				Toast.makeText(
						context,
						"The routes shown are estimates only, as the schedules are from "
								+ outputFormatb.toString()
								+ " to "
								+ outputFormata.toString()
								+ ". Please contact the operator(s) directly to check the exact schedules.",
						Toast.LENGTH_LONG).show();
			}
		});

		rowView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Log.e(position + "", list.get(position).getTrip_id() + "");
				Log.e(position + "", list.get(position).getService_id() + "");
				// if (list.get(position).isAfterDate()) {
				// String a = databaseMain.getEndDateOf(list.get(position)
				// .getService_id());
				// int yr = Integer.parseInt(a.substring(0, 4));
				// int month = Integer.parseInt(a.substring(4, 6));
				// int dte = Integer.parseInt(a.substring(6, 8));
				// java.text.DateFormat dateFormat =
				// android.text.format.DateFormat
				// .getDateFormat(context);
				// Calendar calendar2 = Calendar.getInstance();
				// calendar2.set(yr, month - 1, dte);
				// String outputFormat = dateFormat
				// .format(calendar2.getTime()).toString();
				// Toast.makeText(
				// context,
				// "The routes shown are estimates only, as the schedules expires after "
				// + outputFormat.toString()
				// +
				// ". Please contact the operator(s) directly to check the exact schedules.",
				// Toast.LENGTH_LONG).show();
				// } else {
				if (MainActivity.mapOrListClicked == false) {
					MainActivity.mapOrListClicked = true;
					MainActivity.replaceActionBar(list, position);/*
																 * show journey
																 * details
																 */
					// Log.d("Trip_id:", "" +
					// list.get(position).getTrip_id());
					// Log.d("Dest_stop_id:", ""
					// + list.get(position).getDest_stop_id());
					// Log.d("Source_stop_id:", ""
					// + list.get(position).getSource_stop_id());
				}
			}
			// }
		});

		return rowView;
	}

	private CharSequence getStops(String service_id) {
		List<String> Trip_id = new ArrayList<String>();
		List<Integer> Src_Sequence = new ArrayList<Integer>();
		List<Integer> Dest_Sequence = new ArrayList<Integer>();
		Trip_id.add(list.get(positionSelected).getTrip_id());
		Src_Sequence.add(list.get(positionSelected).getSource_sequence());
		Dest_Sequence.add(list.get(positionSelected).getDest_sequence());
		ArrayList<StopTimes> st = new ArrayList<StopTimes>();
		st = databaseMain.getRoute(Trip_id.get(0));

		int src_seq = Src_Sequence.get(0);
		int dest_seq = Dest_Sequence.get(0);
		ArrayList<StopTimes> ft = new ArrayList<StopTimes>();

		for (int i = 0; i < st.size(); i++) {

			if ((Integer) st.get(i).getStop_sequence() >= src_seq
					&& (Integer) st.get(i).getStop_sequence() <= dest_seq) {
				StopTimes finalStops = new StopTimes();
				finalStops.setTrip_id(st.get(i).getTrip_id());
				finalStops.setArrival_time(st.get(i).getArrival_time());
				finalStops.setDeparture_time(st.get(i).getDeparture_time());
				finalStops.setStop_id(st.get(i).getStop_id());
				finalStops.setStop_sequence(st.get(i).getStop_sequence());
				ft.add(finalStops);
			}
		}
		// Log.d("finalStops size", ft.size() + "");
		String stopSize = null;
		if ((ft.size() - 2) > 0) {
			stopSize = (ft.size() - 2) + " Stops";
		}
		if ((ft.size() - 2) == 1) {
			stopSize = (ft.size() - 2) + " Stop";
		}
		if ((ft.size() - 2) == 0) {
			stopSize = "Direct";
		}
		return stopSize;
	}

	public boolean checkDate() {
		List<String> Trip_id = new ArrayList<String>();
		List<Integer> Src_Sequence = new ArrayList<Integer>();
		List<Integer> Dest_Sequence = new ArrayList<Integer>();
		ArrayList<String> ft = new ArrayList<String>();

		for (int listCount = 0; listCount < list.size(); listCount++) {
			Trip_id.add(list.get(listCount).getTrip_id());
			Src_Sequence.add(list.get(listCount).getSource_sequence());
			Dest_Sequence.add(list.get(listCount).getDest_sequence());
		}

		int src_seq = Src_Sequence.get(positionSelected);
		int dest_seq = Dest_Sequence.get(positionSelected);
		ArrayList<StopTimes> st = new ArrayList<StopTimes>();
		st = databaseMain.getRoute(Trip_id.get(positionSelected));
		int i = 0;
		for (i = 0; i < st.size(); i++) {
			if ((Integer) st.get(i).getStop_sequence() >= src_seq
					&& (Integer) st.get(i).getStop_sequence() <= dest_seq) {
				StringTokenizer tk = null;
				// Log.e("list.get(i).getDeparture_time()", st.get(i)
				// .getDeparture_time());
				if (st.get(i).getDeparture_time().length() == 5) {
					tk = new StringTokenizer(st.get(i).getDeparture_time()
							+ ":00");
				} else {
					tk = new StringTokenizer(st.get(i).getDeparture_time());
				}
				if (tk.hasMoreTokens()) {
					String times = tk.nextToken();
					int hourTimes = Integer.parseInt(times.substring(0, 2));
					hourTimes = hourTimes % 24;
					String minuteTimes = times.substring(3, 5);
					String arrivalString = hourTimes + ":" + minuteTimes;
					String hourTimesString = hourTimes + "";
					if (hourTimesString.length() == 1) {
						arrivalString = "0" + hourTimes + ":" + minuteTimes;
					}
					ft.add(arrivalString);
				}
			}
		}
		Boolean nxtDay = false;
		i = 0;

		while (!nxtDay && i < ft.size() - 1) {
			String[] timeCur = ft.get(i).split(":");
			String[] timeNxt = ft.get(i + 1).split(":");
			int minsCur = 0;
			try {
				minsCur = (Integer.parseInt(timeCur[0]) * 60)
						+ Integer.parseInt(timeCur[1]);
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			}
			int minsNxt = 0;
			try {
				minsNxt = (Integer.parseInt(timeNxt[0]) * 60)
						+ Integer.parseInt(timeNxt[1]);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			int diff = minsNxt - minsCur;
			if (diff < 0) {
				nxtDay = true;
			}
			i++;
		}
		if (nxtDay) {
			nextDay = true;
		} else {
			nextDay = false;
		}
		return nextDay;
	}
}
