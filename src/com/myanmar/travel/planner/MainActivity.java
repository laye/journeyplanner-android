/* 
 #Class name: MainActivity
 #Class description: class to start the main activity
 */

package com.myanmar.travel.planner;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.bugsense.trace.BugSenseHandler;

public class MainActivity extends FragmentActivity {
	static String tabSelectd;
	static RelativeLayout helloLayout;
	static FragmentTabHost mTabHost;
	static Context mContext;
	Resources res;
	static ActionBar mActionBar;
	Fragment mFragment1;
	Fragment mFragment2;
	Fragment mFragment3;
	static ImageButton calenderButton;
	static TextView textViewForDate;
	static TextView textViewForViewDate;
	static Spinner spinner1;
	static Spinner spinner2;
	static ImageButton button1Switch;
	static DatabaseMain databaseMain;
	TabWidget tabs;
	int width;
	static int height;
	LayoutParams params;
	public static String dtaenew;
	boolean calenderButtonClicked;
	static String day_selected;
	static boolean mapIsLoading;
	static Toast mapToast;
	static int firstPosition, index;

	static ArrayList<Stops> stopsList = new ArrayList<Stops>();
	static ArrayList<StopTimes> stopTimesList = new ArrayList<StopTimes>();
	ArrayList<Calender> calenderList = new ArrayList<Calender>();
	static List<String> list = new ArrayList<String>();
	static List<String> listsource = new ArrayList<String>();
	static List<String> listdest = new ArrayList<String>();
	static ArrayAdapter<String> dataAdapterlistsource;
	static ArrayAdapter<String> dataAdapterlistdest;

	String stringStopName;
	static FrameLayout frameLayout;
	static ImageView imageViewWarning;

	private static ArrayList<String> agency_names;
	private static ArrayList<String> city;
	private static ArrayList<String> time;
	static ArrayList<String> service_id;
	static ArrayList<String> trip_ids;
	static int count = 0;
	static String routeColor;
	static ListViewAdapterForMapRoute lva;
	static ListView listViewRoute;
	static TextView tv1;
	static TextView tv2;
	static ImageView iv1;
	static ImageView iv2;
	static int listPoition;
	static ArrayList<ListClass> listlist;
	private static ImageView tv3;
	static ArrayList<RouteList> maproute = new ArrayList<RouteList>();
	private static List<String> Trip_id;
	private static List<String> Source_Id;
	private static List<String> Dest_Id;
	private static ArrayList<String> Service_Id;
	static List<Integer> Src_Sequence = null;
	static List<Integer> Dest_Sequence = null;
	static String timeadd;
	static int spinnerOnePosition = 0;
	static int spinnerTwoPosition = 0;
	static boolean pressGoSelected;
	// static String currentDate;
	static String currentDay, currentMonth;
	static int currentButton;
	static String currentYear;
	public static ArrayList<String> destStopID;
	public static int journeyDetailsLayoutHeight;
	static int maxYear, maxMonth, maxDay;
	static int maxYearCal, maxMonthCal, maxDayCal;
	static Resources resourceString;
	static String spinnerTwoStation;
	static String spinnerOneStation;
	static ConnectionDetector connectionDetector;
	static boolean mapOrListClicked;
	static int previousPos;
	FixedDatePickerDialog datePickerDialog;

	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		BugSenseHandler.initAndStartSession(MainActivity.this, "5d76adb8");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newactivityscreen);

		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		width = dm.widthPixels;
		height = dm.heightPixels;
		params = new LayoutParams(width / 3, 60);
		mContext = this;
		connectionDetector = new ConnectionDetector(getApplicationContext());
		resourceString = getResources();
		mActionBar = getActionBar();
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setDisplayHomeAsUpEnabled(false);
		imageViewWarning = (ImageView) findViewById(R.id.imageViewWarning);
		textViewForDate = (TextView) findViewById(R.id.textViewForDate);
		textViewForViewDate = (TextView) findViewById(R.id.textViewForViewDate);

		spinner1 = (Spinner) findViewById(R.id.spinner1);
		spinner2 = (Spinner) findViewById(R.id.spinner2);

		Calendar calendarRange = Calendar.getInstance();
		maxYear = calendarRange.get(Calendar.YEAR);
		maxMonth = calendarRange.get(Calendar.MONTH);
		maxDay = calendarRange.get(Calendar.DAY_OF_MONTH);
		maxYearCal = calendarRange.get(Calendar.YEAR);
		maxMonthCal = calendarRange.get(Calendar.MONTH);
		maxDayCal = calendarRange.get(Calendar.DAY_OF_MONTH);

		textViewForDate.setText(new StringBuilder().append(maxDay).append("/")
				.append(maxMonth + 1).append("/").append(maxYear));/*
																	 * set date
																	 * to
																	 * textview
																	 */

		Log.e("maxDay", maxDay + "aa");
		Log.e("maxMonth", maxMonth + "aa");
		Log.e("maxYear", maxYear + "aa");
		String dateSetting = getDateFormat(mContext, maxDay + "/"
				+ (maxMonth + 1) + "/" + maxYear);
		textViewForViewDate.setText(dateSetting);
		textViewForDate.setText(dateSetting);
		dtaenew = textViewForViewDate.getText().toString();
		mActionBar.setTitle(getResources()
				.getString(R.string.plan_your_journey));
		databaseMain = new DatabaseMain(getApplicationContext());

		helloLayout = (RelativeLayout) findViewById(R.id.tabcontentssssssssssss);
		journeyDetailsLayoutHeight = helloLayout.getLayoutParams().height;
		frameLayout = (FrameLayout) findViewById(R.id.realtabcontent);

		stopsList = databaseMain.getAllStops();
		stopTimesList = databaseMain.getAllStopTime();
		calenderList = databaseMain.getAllCalender();
		/*
		 * send new list of selected stops, calender, and new selected date
		 */
		Tab1Edited.sendList(calenderList, dtaenew);
		Tab2Edited.sendList(calenderList, dtaenew);
		Tab3Edited.sendList(calenderList, dtaenew);

		android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		android.support.v4.app.Fragment frag = new FragmentTab();
		transaction.replace(android.R.id.tabcontent, frag);
		transaction.commit();

		button1Switch = (ImageButton) findViewById(R.id.button1Switch);

		list.clear();
		list.add("Select Destination");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_list_item_1);
		spinner1.setAdapter(dataAdapter);
		spinner2.setAdapter(dataAdapter);
		spinner2.setEnabled(false);

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);

		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

		/*
		 * set tabs to tabhost
		 */
		@SuppressWarnings("deprecation")
		BitmapDrawable bmd3 = new BitmapDrawable();
		bmd3 = (BitmapDrawable) getResources().getDrawable(R.drawable.plane);
		mTabHost.addTab(mTabHost.newTabSpec("Air").setIndicator("", bmd3),
				Tab1Edited.class, null);
		bmd3 = (BitmapDrawable) getResources().getDrawable(R.drawable.rail);
		mTabHost.addTab(mTabHost.newTabSpec("Rail").setIndicator("", bmd3),
				Tab2Edited.class, null);
		bmd3 = (BitmapDrawable) getResources().getDrawable(R.drawable.bus);
		mTabHost.addTab(mTabHost.newTabSpec("Bus").setIndicator("", bmd3),
				Tab3Edited.class, null);

		tabs = (TabWidget) findViewById(android.R.id.tabs);

		/*
		 * set tabindicator to tab
		 */
		mTabHost.getTabWidget().getChildAt(0)
				.setBackgroundResource(R.drawable.tab_indicator_ab_holo);
		mTabHost.getTabWidget().getChildAt(1)
				.setBackgroundResource(R.drawable.tab_indicator_ab_holo);
		mTabHost.getTabWidget().getChildAt(2)
				.setBackgroundResource(R.drawable.tab_indicator_ab_holo);

		mTabHost.setCurrentTab(0);
		tabSelectd = "Air";
		/*
		 * set listener to tab
		 */
		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabchanged) {
				mActionBar.setTitle(getResources().getString(
						R.string.plan_your_journey));
				helloLayout.removeAllViews();
				helloLayout.removeAllViewsInLayout();
				helloLayout.setVisibility(View.GONE);
				frameLayout.setVisibility(View.VISIBLE);

				// Log.e("tab", tabchanged);
				tabSelectd = tabchanged;
				// tryAndCatchAsyncTasks();
			}

		});

		textViewForViewDate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				calenderButton.performClick();
			}
		});
		calenderButton = (ImageButton) findViewById(R.id.calenderButton);
		calenderButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!currentDay.equals("")) {
					getDateFormat(mContext, maxDay + "/" + (maxMonth + 1) + "/"
							+ maxYear);
					currentDay = maxDay + "";
					currentMonth = (maxMonth + 1) + "";
				} else {
					currentDay = maxDay + "/" + (maxMonth + 1);
					currentDay = maxDay + "";
					currentMonth = (maxMonth + 1) + "";
				}
				if (datePickerDialog == null) {
					displayDatePickerDialogue();
					datePickerDialog.show();
				} else {
					if (!datePickerDialog.isShowing()) {
						displayDatePickerDialogue();
						datePickerDialog.show();
					}
				}
			}

			private void displayDatePickerDialogue() {
				datePickerDialog = new FixedDatePickerDialog(mContext,
						new OnDateSetListener() {

							@Override
							public void onDateSet(DatePicker view, int year,
									int monthOfYear, int dayOfMonth) {
								textViewForDate.setText(new StringBuilder()
										.append(dayOfMonth).append("/")
										.append(monthOfYear + 1).append("/")
										.append(year));/*
														 * set date to textview
														 */
								maxDay = dayOfMonth;
								maxMonth = monthOfYear;
								maxYear = year;
								String dateSetting = getDateFormat(mContext,
										maxDay + "/" + (maxMonth + 1) + "/"
												+ maxYear);
								textViewForViewDate.setText(dateSetting);
								textViewForDate.setText(dateSetting);
								// currentDate = new StringBuilder()
								// .append(dayOfMonth).append("/")
								// .append(monthOfYear + 1).toString();
								currentDay = dayOfMonth + "";
								currentMonth = (monthOfYear + 1) + "";
								currentYear = year + "";
								try {
									dtaenew = textViewForViewDate.getText()
											.toString();
									Tab1Edited.setdatenew(maxDay + "/"
											+ (maxMonth + 1) + "/" + maxYear);
								} catch (Exception e) {
								}
								try {
									dtaenew = textViewForViewDate.getText()
											.toString();
									Tab2Edited.setdatenew(maxDay + "/"
											+ (maxMonth + 1) + "/" + maxYear);
								} catch (Exception e) {
								}
								try {
									dtaenew = textViewForViewDate.getText()
											.toString();
									Tab3Edited.setdatenew(maxDay + "/"
											+ (maxMonth + 1) + "/" + maxYear);
								} catch (Exception e) {
								}
								/*
								 * set hide journey details
								 */
								mActionBar.setTitle(resourceString
										.getString(R.string.plan_your_journey));
								helloLayout.removeAllViews();
								helloLayout.removeAllViewsInLayout();
								helloLayout.setVisibility(View.GONE);
								frameLayout.setVisibility(View.VISIBLE);
							}
						}, maxYearCal, maxMonthCal, maxDayCal);
			}
		});
		try {
			Tab1Edited.loadAdapter(1);
		} catch (Exception e) {
		}
		try {
			Tab2Edited.loadAdapter(1);
		} catch (Exception e) {
		}
		try {
			Tab3Edited.loadAdapter(1);
		} catch (Exception e) {
		}

	}

	@Override
	public void onBackPressed() {
		/*
		 * check if journey details is open
		 */
		if (mActionBar.getTitle().equals(
				getResources().getString(R.string.journey_details))) {
			mActionBar.setTitle(getResources().getString(
					R.string.plan_your_journey));
			helloLayout.removeAllViews();
			helloLayout.removeAllViewsInLayout();
			helloLayout.setVisibility(View.GONE);
			frameLayout.setVisibility(View.VISIBLE);
			mActionBar.setDisplayHomeAsUpEnabled(false);
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setMessage("Do you wish to exit application?")
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									finish();
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
								}
							});
			builder.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		/*
		 * inflate menu.xml
		 */
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		/*
		 * on selected menu item choose what to do
		 */
		int itemId = item.getItemId();
		switch (itemId) {
		case android.R.id.home:
			/*
			 * check if journey details is open
			 */
			if (mActionBar.getTitle().equals(
					getResources().getString(R.string.journey_details))) {
				mActionBar.setTitle(getResources().getString(
						R.string.plan_your_journey));
				helloLayout.removeAllViews();
				helloLayout.removeAllViewsInLayout();
				helloLayout.setVisibility(View.GONE);
				frameLayout.setVisibility(View.VISIBLE);
				mActionBar.setDisplayHomeAsUpEnabled(false);
			}
			break;
		case R.id.action_more:
			break;
		case R.id.feedback:
			/*
			 * open feedback layout
			 */

			getListPosition();

			startActivity(new Intent(MainActivity.this, Feedback.class));
			break;
		case R.id.operators:
			/*
			 * open operators layout
			 */

			getListPosition();
			startActivity(new Intent(MainActivity.this, Operators.class));
			break;
		case R.id.favourites:
			/*
			 * open favourites layout
			 */

			getListPosition();
			startActivity(new Intent(MainActivity.this,
					FavouritesActivity.class));
			break;
		case R.id.about:
			/*
			 * open about layout
			 */

			getListPosition();
			startActivity(new Intent(MainActivity.this, About.class));
			break;
		}

		return true;
	}

	private void getListPosition() {
		if (tabSelectd.equals("Air")) {
			index = Tab1Edited.lv.getFirstVisiblePosition();
			View v = Tab1Edited.lv.getChildAt(0);
			firstPosition = (v == null) ? 0 : v.getTop();
		}
		if (tabSelectd.equals("Rail")) {
			index = Tab2Edited.lv.getFirstVisiblePosition();
			View v = Tab2Edited.lv.getChildAt(0);
			firstPosition = (v == null) ? 0 : v.getTop();
		}
		if (tabSelectd.equals("Bus")) {
			index = Tab3Edited.lv.getFirstVisiblePosition();
			View v = Tab3Edited.lv.getChildAt(0);
			firstPosition = (v == null) ? 0 : v.getTop();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (databaseMain != null) {
			databaseMain.close();
		}
	}

	public static void setSpinner1(ArrayList<String> getStops) {
		/*
		 * set spinners (to/from)
		 */
		listsource.clear();

		for (int stopCounts = 0; stopCounts < getStops.size(); stopCounts++) {
			if (!listsource.contains(getStops.get(stopCounts).toString())) {
				listsource.add(getStops.get(stopCounts).toString());
			}
		}
		dataAdapterlistsource = new ArrayAdapter<String>(mContext,
				android.R.layout.simple_spinner_item, listsource);
		dataAdapterlistsource
				.setDropDownViewResource(android.R.layout.simple_list_item_1);
		spinner1.setAdapter(dataAdapterlistsource);
	}

	public static void setSpinner2(ArrayList<String> getStops) {
		/*
		 * set spinners (to/from)
		 */
		listdest.clear();

		for (int stopCounts = 0; stopCounts < getStops.size(); stopCounts++) {
			if (!listdest.contains(getStops.get(stopCounts).toString())) {
				listdest.add(getStops.get(stopCounts).toString());
			}
		}
		dataAdapterlistdest = new ArrayAdapter<String>(mContext,
				android.R.layout.simple_spinner_item, listdest);
		dataAdapterlistdest
				.setDropDownViewResource(android.R.layout.simple_list_item_1);
		spinner2.setAdapter(dataAdapterlistdest);
	}

	public static void replaceActionBar(final ArrayList<ListClass> list,
			int position) {
		/*
		 * open journey details
		 */
		MainActivity.mapOrListClicked = false;
		count = position;
		Trip_id = new ArrayList<String>();
		Source_Id = new ArrayList<String>();
		Dest_Id = new ArrayList<String>();
		Service_Id = new ArrayList<String>();
		agency_names = new ArrayList<String>();
		Src_Sequence = new ArrayList<Integer>();
		Dest_Sequence = new ArrayList<Integer>();
		//
		// Log.d("Trip Id:", "> " + list.get(position).getTrip_id());
		// Log.d("Src Id:", "> " + list.get(position).getSource_stop_id());
		// Log.d("Dest Id:", "> " + list.get(position).getDest_stop_id());
		// Log.d("Dest Name:", "> " + list.get(position).getSource_stop_name());
		// Log.d("Src Name:", "> " + list.get(position).getDest_stop_name());

		for (int listCount = 0; listCount < list.size(); listCount++) {
			Trip_id.add(list.get(listCount).getTrip_id());
			Source_Id.add(list.get(listCount).getSource_stop_id());
			Dest_Id.add(list.get(listCount).getDest_stop_id());
			Service_Id.add(list.get(listCount).getService_id());
			String route_id = databaseMain.getRouteId(list.get(listCount)
					.getService_id(), list.get(listCount).getTrip_id());
			String agency_id = databaseMain.getAgencyId(route_id);
			agency_names.add(databaseMain.getAgencyName(agency_id));
			Src_Sequence.add(list.get(listCount).getSource_sequence());
			Dest_Sequence.add(list.get(listCount).getDest_sequence());
		}

		mActionBar.setTitle(resourceString.getString(R.string.journey_details));
		frameLayout.setVisibility(View.GONE);
		helloLayout.setVisibility(View.VISIBLE);
		mActionBar.setDisplayHomeAsUpEnabled(true);
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.route, null, false);

		listViewRoute = (ListView) rowView.findViewById(R.id.listView1);
		tv1 = (TextView) rowView.findViewById(R.id.textView1);
		tv2 = (TextView) rowView.findViewById(R.id.textView2);
		tv3 = (ImageView) rowView.findViewById(R.id.textView3);
		iv1 = (ImageView) rowView.findViewById(R.id.imageView1);
		iv2 = (ImageView) rowView.findViewById(R.id.imageView2);

		routeColor = databaseMain.getRouteColor(
				list.get(count).getService_id(), list.get(count).getTrip_id());
		tv3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				if (connectionDetector.isConnectingToInternet()) {
					if (mapIsLoading == false) {
						mapIsLoading = true;
						mapToast = Toast.makeText(mContext,
								resourceString.getString(R.string.loading_map),
								Constant.toastLength);
						mapToast.show();
						Intent mapIntent = new Intent(mContext,
								MapActivity.class);
						mapIntent
								.putExtra("mapdata", new DataWrapper(maproute));
						mapIntent.putExtra("routeColor", routeColor);
						mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						view.getContext().startActivity(mapIntent);
					}
				} else {
					Toast.makeText(mContext,
							resourceString.getString(R.string.no_internet),
							Toast.LENGTH_LONG).show();
				}
			}
		});

		tv1.setText(agency_names.get(count));
		tv2.setText(Service_Id.get(count));
		loadList(count);

		if (position == list.size() - 1) {
			iv2.setImageResource(R.drawable.next_disabled);
			iv2.setClickable(false);
		}
		if (position == 0) {
			iv1.setImageResource(R.drawable.previous_disabled);
			iv1.setClickable(false);
		}
		iv1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (count - 1 >= 0) {
					iv2.setImageResource(R.drawable.next);
					iv2.setClickable(true);
					tv1.setText(agency_names.get(--count));
					tv2.setText(Service_Id.get(count));
					routeColor = databaseMain.getRouteColor(list.get(count)
							.getService_id(), list.get(count).getTrip_id());
					loadList(count);
					if (count == 0) {
						iv1.setImageResource(R.drawable.previous_disabled);
						iv1.setClickable(false);
					} else {
						iv1.setImageResource(R.drawable.previous);
						iv1.setClickable(true);
					}
				}
			}
		});

		iv2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (count + 1 < agency_names.size()) {
					iv1.setImageResource(R.drawable.previous);
					iv1.setClickable(true);
					tv1.setText(agency_names.get(++count));
					tv2.setText(Service_Id.get(count));
					routeColor = databaseMain.getRouteColor(list.get(count)
							.getService_id(), list.get(count).getTrip_id());
					loadList(count);

					if (count == agency_names.size() - 1) {
						iv2.setImageResource(R.drawable.next_disabled);
						iv2.setClickable(false);
					} else {
						iv2.setImageResource(R.drawable.next);
						iv2.setClickable(true);
					}
				}
			}
		});
		try {
			helloLayout.removeAllViews();
		} catch (Exception e) {
		}
		helloLayout.addView(rowView);
	}

	public static void loadAdapter() {
		lva = new ListViewAdapterForMapRoute(mContext, city, time);
		listViewRoute.setAdapter(lva);
	}

	@Override
	protected void onResume() {
		super.onResume();
		try {
			Tab1Edited.loadAdapter(1);
			Tab1Edited.lv.setSelectionFromTop(index, firstPosition);
			// Tab1Edited.lv.setSelection(firstPosition);
		} catch (Exception e) {
		}
		try {
			Tab2Edited.loadAdapter(1);
			Tab2Edited.lv.setSelection(firstPosition);
		} catch (Exception e) {
		}
		try {
			Tab3Edited.loadAdapter(1);
			Tab3Edited.lv.setSelection(firstPosition);
		} catch (Exception e) {
		}
	}

	@SuppressLint("SimpleDateFormat")
	public static void loadList(int count) {
		int src_seq = Src_Sequence.get(count);
		int dest_seq = Dest_Sequence.get(count);

		ArrayList<StopTimes> st = new ArrayList<StopTimes>();
		st = databaseMain.getRoute(Trip_id.get(count));
		ArrayList<StopTimes> ft = new ArrayList<StopTimes>();

		for (int i = 0; i < st.size(); i++) {
			if ((Integer) st.get(i).getStop_sequence() >= src_seq
					&& (Integer) st.get(i).getStop_sequence() <= dest_seq) {
				StopTimes finalStops = new StopTimes();
				finalStops.setTrip_id(st.get(i).getTrip_id());
				finalStops.setArrival_time(st.get(i).getArrival_time());
				finalStops.setDeparture_time(st.get(i).getDeparture_time());
				finalStops.setStop_id(st.get(i).getStop_id());
				finalStops.setStop_sequence(st.get(i).getStop_sequence());
				ft.add(finalStops);
			}
		}

		city = new ArrayList<String>();
		time = new ArrayList<String>();
		maproute = new ArrayList<RouteList>();
		maproute.clear();
		boolean twentyFourHour = DateFormat.is24HourFormat(mContext);
		if (twentyFourHour) {
			for (int i = 0; i < ft.size(); i++) {
				RouteList routcord = databaseMain.getCordinates(ft.get(i)
						.getStop_id());
				if (i == 0) {
					city.add(routcord.getCity());
					time.add(ft.get(i).getDeparture_time());
				} else if (i == ft.size() - 1) {
					city.add(routcord.getCity());
					StringTokenizer tk = null;
					// Log.e("ft.get(i).getArrival_time()", ft.get(i)
					// .getArrival_time());
					if (ft.get(i).getArrival_time().length() == 5) {
						tk = new StringTokenizer(ft.get(i).getArrival_time()
								+ ":00");
					} else {
						tk = new StringTokenizer(ft.get(i).getArrival_time());
					}
					String times = tk.nextToken();
					int hourTimes = Integer.parseInt(times.substring(0, 2));
					hourTimes = hourTimes % 24;
					String minuteTimes = times.substring(3, 5);
					String arrivalString = hourTimes + ":" + minuteTimes;
					String hourTimesString = hourTimes + "";
					if (hourTimesString.length() == 1) {
						arrivalString = "0" + hourTimes + ":" + minuteTimes;
					}
					time.add(arrivalString);
					// Log.d("hourTimes : minuteTimes times", hourTimes + ":"
					// + minuteTimes);
				} else {
					city.add(routcord.getCity());
					StringTokenizer tk = null;
					// Log.e("ft.get(i).getArrival_time()", ft.get(i)
					// .getArrival_time());
					if (ft.get(i).getArrival_time().length() == 5) {
						tk = new StringTokenizer(ft.get(i).getArrival_time()
								+ ":00");
					} else {
						tk = new StringTokenizer(ft.get(i).getArrival_time());
					}
					String times = tk.nextToken();
					int hourTimes = Integer.parseInt(times.substring(0, 2));
					hourTimes = hourTimes % 24;
					String minuteTimes = times.substring(3, 5);
					String hourTimesString = hourTimes + "";
					String arrivalString = hourTimes + ":" + minuteTimes;
					if (hourTimesString.length() == 1) {
						arrivalString = "0" + hourTimes + ":" + minuteTimes;
					}

					tk = null;
					// Log.e("ft.get(i).getDeparture_time()", ft.get(i)
					// .getDeparture_time());
					if (ft.get(i).getDeparture_time().length() == 5) {
						tk = new StringTokenizer(ft.get(i).getDeparture_time()
								+ ":00");
					} else {
						tk = new StringTokenizer(ft.get(i).getDeparture_time());
					}
					times = tk.nextToken();
					hourTimes = Integer.parseInt(times.substring(0, 2));
					hourTimes = hourTimes % 24;
					minuteTimes = times.substring(3, 5);
					hourTimesString = hourTimes + "";
					String departureString = hourTimes + ":" + minuteTimes;
					if (hourTimesString.length() == 1) {
						departureString = "0" + hourTimes + ":" + minuteTimes;
					}
					time.add(arrivalString + "-" + departureString);
				}
				maproute.add(routcord);
			}
		} else {
			for (int i = 0; i < ft.size(); i++) {
				timeadd = "--";
				RouteList routcord = databaseMain.getCordinates(ft.get(i)
						.getStop_id());
				if (i == 0) {
					city.add(routcord.getCity());
					StringTokenizer tk = new StringTokenizer(ft.get(i)
							.getDeparture_time() + ":00");
					String times = tk.nextToken();
					SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
					SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
					SimpleDateFormat sdfss = new SimpleDateFormat("hh:mm");
					Date dt;
					try {
						dt = sdf.parse(times);
						timeadd = sdfs.format(dt);
						if (times.length() == 8) {
							if (times.substring(0, 2).equals("12")) {
								timeadd = sdfss.format(dt) + " PM";
							}
						}
					} catch (ParseException e) {
					}
					time.add(timeadd);
				} else if (i == ft.size() - 1) {
					city.add(routcord.getCity());
					StringTokenizer tk = new StringTokenizer(ft.get(i)
							.getArrival_time() + ":00");
					String times = tk.nextToken();
					SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
					SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
					SimpleDateFormat sdfss = new SimpleDateFormat("hh:mm");
					Date dt;
					try {
						dt = sdf.parse(times);
						timeadd = sdfs.format(dt);
						if (times.length() == 8) {
							if (times.substring(0, 2).equals("12")) {
								timeadd = sdfss.format(dt) + " PM";
							}
						}
					} catch (ParseException e) {
					}
					time.add(timeadd);
				} else {
					city.add(routcord.getCity());
					StringTokenizer tk = null;
					// Log.e("ft.get(i).getArrival_time()", ft.get(i)
					// .getArrival_time());
					// Log.e("ft.get(i).getDeparture_time()", ft.get(i)
					// .getDeparture_time());
					if (ft.get(i).getArrival_time().length() == 8
							&& ft.get(i).getDeparture_time().length() == 5) {
						tk = new StringTokenizer(ft.get(i).getArrival_time()
								+ " " + ft.get(i).getDeparture_time() + ":00");
					} else if (ft.get(i).getArrival_time().length() == 5
							&& ft.get(i).getDeparture_time().length() == 8) {
						tk = new StringTokenizer(ft.get(i).getArrival_time()
								+ ":00" + " " + ft.get(i).getDeparture_time());
					} else if (ft.get(i).getArrival_time().length() == 8
							&& ft.get(i).getDeparture_time().length() == 8) {
						tk = new StringTokenizer(ft.get(i).getArrival_time()
								+ " " + ft.get(i).getDeparture_time());
					} else if (ft.get(i).getArrival_time().length() == 5
							&& ft.get(i).getDeparture_time().length() == 5) {
						tk = new StringTokenizer(ft.get(i).getArrival_time()
								+ ":00" + " " + ft.get(i).getDeparture_time()
								+ ":00");
					}
					if (tk != null) {
						timeadd = "";
						if (tk.hasMoreTokens()) {
							String times = tk.nextToken();
							SimpleDateFormat sdf = new SimpleDateFormat(
									"hh:mm:ss");
							SimpleDateFormat sdfs = new SimpleDateFormat(
									"hh:mm a");
							SimpleDateFormat sdfss = new SimpleDateFormat(
									"hh:mm");
							Date dt;
							try {
								dt = sdf.parse(times);
								timeadd = sdfs.format(dt);
								if (times.length() == 8) {
									if (times.substring(0, 2).equals("12")) {
										timeadd = sdfss.format(dt) + " PM";
									}
								}
							} catch (ParseException e) {
							}
						}
						if (tk.hasMoreTokens()) {
							String times = tk.nextToken();
							SimpleDateFormat sdf = new SimpleDateFormat(
									"hh:mm:ss");
							SimpleDateFormat sdfs = new SimpleDateFormat(
									"hh:mm a");
							SimpleDateFormat sdfss = new SimpleDateFormat(
									"hh:mm");
							Date dt;
							String timesdddd;
							try {
								dt = sdf.parse(times);
								if (times.length() == 8) {
									if (times.substring(0, 2).equals("12")) {
										timesdddd = sdfss.format(dt) + " PM";
										timeadd = timeadd + " - " + timesdddd;
									} else {
										timeadd = timeadd + " - "
												+ sdfs.format(dt);
									}
								}
							} catch (ParseException e) {
							}
						}
					}
					time.add(timeadd);
				}
				maproute.add(routcord);
				MainActivity.mapOrListClicked = false;
			}
		}
		loadAdapter();
	}

	public static String getDateFormat(Context context, String string) {
		// 25/12/2013
		String dateToBeSent = "";
		Format dateFormat = android.text.format.DateFormat
				.getDateFormat(context);
		String pattern = ((SimpleDateFormat) dateFormat).toLocalizedPattern();
		// Log.e(pattern, pattern);
		String segments[] = string.split("/");
		int day = 0, month = 0, year = 0;
		if (pattern.equals("dd/MM/yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("d/M/yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("dd/M/yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("d/MM/yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("MM/dd/yyyy")) {
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("M/d/yyyy")) {
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("MM/d/yyyy")) {
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("M/dd/yyyy")) {
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("yyyy/MM/dd")) {
			year = Integer.parseInt(segments[segments.length - 1]);
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3].trim());
			dateToBeSent = year + "/" + month + "/" + day;
		} else if (pattern.equals("E, MMM d, yyyy")) {
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("E, d MMM yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("EE,dd MMM yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("yyyy MMM d, E")) {
			year = Integer.parseInt(segments[segments.length - 1]);
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3].trim());
			dateToBeSent = year + "/" + month + "/" + day;
		} else if (pattern.equals("d/M/y")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("M/d/y")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else {
			Toast.makeText(context, pattern,
					Toast.LENGTH_LONG + Toast.LENGTH_LONG).show();
		}

		// Calendar testDate = Calendar.getInstance();
		// testDate.set(Calendar.YEAR, year);
		// testDate.set(Calendar.MONTH, month - 1);
		// testDate.set(Calendar.DAY_OF_MONTH, day);
		//
		// Log.e("date", day + "--" + month + "--" + year);
		// Format format =
		// android.text.format.DateFormat.getDateFormat(context);
		//
		// String testDateFormat = format.format(testDate.getTime());
		// String[] parts = testDateFormat.split("/");
		// StringBuilder sb = new StringBuilder();
		// for (String s : parts) {
		// if (s.equals(day + "")) {
		// sb.append("d/");
		// }
		// if (s.equals("0" + day + "")) {
		// if (!sb.toString().contains("d/")) {
		// sb.append("d/");
		// }
		// }
		// if (s.equals(month + "")) {
		// sb.append("MM/");
		// }
		// if (s.equals("0" + month + "")) {
		// if (!sb.toString().contains("MM/")) {
		// sb.append("MM/");
		// }
		// }
		// if (s.equals(year + "")) {
		// sb.append("yyyy/");
		// }
		// }
		//
		// String df = "";
		// try {
		// df = sb.toString().substring(0, sb.toString().length() - 1);
		// Log.e("test1", df);
		// } catch (Exception e) {
		// format = android.text.format.DateFormat.getDateFormat(context);
		// sb.append("MM/");
		// sb.append("d/");
		// sb.append("yyyy/");
		//
		// df = sb.toString().substring(0, sb.toString().length() - 1);
		// Log.e("test2", df);
		// }
		//
		// Log.e("test", df);
		//
		// String[] partsd = df.split("/");
		// StringBuilder sbd = new StringBuilder();
		// for (String s : partsd) {
		// if (s.equals("d")) {
		// sbd.append(day + "/");
		// }
		// if (s.equals("MM")) {
		// sbd.append(month + "/");
		// }
		// if (s.equals("yyyy")) {
		// sbd.append(year + "/");
		// }
		// }

		currentDay = day + "";
		currentMonth = month + "";
		// currentDate = day + "/" + (month + 1);
		currentYear = year + "";
		maxDay = day;
		maxMonth = month - 1;
		maxYear = year;
		// Log.e("currentDay", day + "");
		// Log.e("currentMonth", month + "");
		// Log.e("currentYear", year + "");
		// Log.e("dateToBeSent", dateToBeSent);
		return dateToBeSent;
	}

	// private void tryAndCatchAsyncTasks() {
	// try {
	// Tab1Edited.checkAirRoutes1.cancel(true);
	// } catch (Exception e1) {
	// }
	// try {
	// Tab1Edited.checkAirRoutes2.cancel(true);
	// } catch (Exception e2) {
	// }
	// try {
	// Tab1Edited.checkAirRoutes3.cancel(true);
	// } catch (Exception e3) {
	// }
	// try {
	// Tab1Edited.checkAirRoutes4.cancel(true);
	// } catch (Exception e4) {
	// }
	// try {
	// Tab1Edited.checkAirRoutes5.cancel(true);
	// } catch (Exception e5) {
	// }
	// try {
	// Tab1Edited.checkAirRoutes6.cancel(true);
	// } catch (Exception e6) {
	// }
	// try {
	// Tab1Edited.checkAirRoutes7.cancel(true);
	// } catch (Exception e7) {
	// }
	// try {
	// Tab2Edited.checkRailRoutes1.cancel(true);
	// } catch (Exception e11) {
	// }
	// try {
	// Tab2Edited.checkRailRoutes2.cancel(true);
	// } catch (Exception e12) {
	// }
	// try {
	// Tab2Edited.checkRailRoutes3.cancel(true);
	// } catch (Exception e13) {
	// }
	// try {
	// Tab2Edited.checkRailRoutes4.cancel(true);
	// } catch (Exception e14) {
	// }
	// try {
	// Tab2Edited.checkRailRoutes5.cancel(true);
	// } catch (Exception e15) {
	// }
	// try {
	// Tab2Edited.checkRailRoutes6.cancel(true);
	// } catch (Exception e16) {
	// }
	// try {
	// Tab2Edited.checkRailRoutes7.cancel(true);
	// } catch (Exception e17) {
	// }
	// try {
	// Tab3Edited.checkBusRoutes1.cancel(true);
	// } catch (Exception e21) {
	// }
	// try {
	// Tab3Edited.checkBusRoutes2.cancel(true);
	// } catch (Exception e22) {
	// }
	// try {
	// Tab3Edited.checkBusRoutes3.cancel(true);
	// } catch (Exception e23) {
	// }
	// try {
	// Tab3Edited.checkBusRoutes4.cancel(true);
	// } catch (Exception e24) {
	// }
	// try {
	// Tab3Edited.checkBusRoutes5.cancel(true);
	// } catch (Exception e25) {
	// }
	// try {
	// Tab3Edited.checkBusRoutes6.cancel(true);
	// } catch (Exception e26) {
	// }
	// try {
	// Tab3Edited.checkBusRoutes7.cancel(true);
	// } catch (Exception e27) {
	// }
	// }

	public static ArrayList<String> findtopsStartPoint(
			String spinnerOneStation, String like) {

		ArrayList<Stops> stopListings = new ArrayList<Stops>();
		for (int i = 0; i < stopsList.size(); i++) {
			if (stopsList.get(i).getStop_desc().equals(spinnerOneStation)) {
				if (stopsList.get(i).getStop_id().startsWith(like)) {
					stopListings.add(stopsList.get(i));
				}
			}
		}
		// Log.e("stopListings sx", stopListings.size() + "");
		ArrayList<String> nextStopsString = new ArrayList<String>();

		/* stopTimes with stop id */
		if (stopListings.size() > 0) {
			ArrayList<StopTimes> dataList = new ArrayList<StopTimes>();
			for (int stopListingCount = 0; stopListingCount < stopListings
					.size(); stopListingCount++) {
				for (int i = 0; i < stopTimesList.size(); i++) {
					if (stopTimesList
							.get(i)
							.getStop_id()
							.equals(stopListings.get(stopListingCount)
									.getStop_id())) {
						dataList.add(stopTimesList.get(i));
					}
				}
			}

			// Log.e("dataList sx", dataList.size() + "");
			/* next stops */
			ArrayList<String> nextStops = new ArrayList<String>();
			for (int i = 0; i < dataList.size(); i++) {
				StopTimes stopTimesOb = dataList.get(i);

				String trip_id = stopTimesOb.getTrip_id();
				int stop_seq = stopTimesOb.getStop_sequence();

				for (int i1 = 0; i1 < stopTimesList.size(); i1++) {
					if (stopTimesList.get(i1).getTrip_id().equals(trip_id)
							&& stopTimesList.get(i1).getStop_sequence() > stop_seq) {
						nextStops.add(stopTimesList.get(i1).getStop_id());
					}
				}
			}

			// Log.e("nextStops sx", nextStops.size() + "");
			nextStopsString.clear();
			/* get stop name from stop id */
			for (int i = 0; i < nextStops.size(); i++) {
				// Log.e("nextStops.get(j).getStop_id()", nextStops.get(i));
				for (int j = 0; j < stopsList.size(); j++) {

					// Log.e("stopsList.get(j).getStop_id()", stopsList.get(j)
					// .getStop_id());
					// Log.e("like", like);
					// Log.e("stopsList.get(j).getStop_id().startsWith(like)",
					// stopsList.get(j).getStop_id().startsWith(like) + "");

					if (stopsList.get(j).getStop_id().equals(nextStops.get(i))) {
						if (stopsList.get(j).getStop_id().startsWith(like)) {
							if (!nextStopsString.contains(stopsList.get(j)
									.getStop_desc())) {
								nextStopsString.add(stopsList.get(j)
										.getStop_desc());
							}
						}
					}
				}
			}

			Collections.sort(nextStopsString, String.CASE_INSENSITIVE_ORDER);
			if (nextStopsString.contains("Mandalay")) {
				nextStopsString.remove("Mandalay");
				nextStopsString.add(0, "Mandalay");
			}
			if (nextStopsString.contains("Yangon")) {
				nextStopsString.remove("Yangon");
				nextStopsString.add(0, "Yangon");
			}
			nextStopsString.add(0, "Going to...");
			if (nextStopsString.contains(spinnerOneStation)) {
				nextStopsString.remove(spinnerOneStation);
			}
		} else {
			nextStopsString.add(0, "Going to...");
		}
		// Log.e("spinnerOneStation ", spinnerOneStation);
		// Log.e("nextStopsString size", nextStopsString.size() + "<=size");
		return nextStopsString;
	}
}
