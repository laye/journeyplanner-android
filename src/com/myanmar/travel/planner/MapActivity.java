/* 
 #Class name: MapActivity
 #Class description: class to show the map route
 */

package com.myanmar.travel.planner;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapActivity extends FragmentActivity {
	static GoogleMap googleMap;
	final String TAG = "PathGoogleMapActivity";
	LocationClient mLocationClient;
	static Context context;
	String routeColor = "";
	static ArrayList<RouteList> maproute = new ArrayList<RouteList>();
	static ArrayList<LatLng> lonlat = new ArrayList<LatLng>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_main);
		DataWrapper dw = (DataWrapper) getIntent().getSerializableExtra(
				"mapdata");
		routeColor = getIntent().getExtras().getString("routeColor");
		maproute = dw.getList();
		SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		try {
			googleMap = fm.getMap();
		} catch (Exception e) {
		}

		MainActivity.mapIsLoading = false;
		context = this;

		if (googleMap != null) {
			googleMap.setOnMapLoadedCallback(new OnMapLoadedCallback() {
				@Override
				public void onMapLoaded() {
					googleMap.animateCamera(CameraUpdateFactory
							.zoomBy((float) 0.1));
					addMarkers();
					ArrayList<LatLng> points = null;
					PolylineOptions polyLineOptions = null;
					points = new ArrayList<LatLng>();
					polyLineOptions = new PolylineOptions();
					DecimalFormat numberFormat = new DecimalFormat("#.00000");
					for (int arrayCount = 0; arrayCount < maproute.size(); arrayCount++) {
						double lat = maproute.get(arrayCount).getLat();
						String latt = numberFormat.format(lat);
						double lng = maproute.get(arrayCount).getLon();
						String lngg = numberFormat.format(lng);
						lat = Double.parseDouble(latt);
						lng = Double.parseDouble(lngg);
						LatLng position = new LatLng(lat, lng);
						points.add(position);
					}
					polyLineOptions.addAll(points);
					polyLineOptions.width(5);
					if (!routeColor.equals("")) {
						polyLineOptions.color(Integer.parseInt(routeColor, 16) + 0xFF000000);
					} else {
						polyLineOptions.color(Color.BLUE);
					}
					googleMap.addPolyline(polyLineOptions);
					LatLngBounds.Builder builder = new LatLngBounds.Builder();
					for (LatLng marker : lonlat) {
						builder.include(marker);
					}
					centerIncidentRouteOnMaps(points);
				}
			});
		} else {
			Toast.makeText(context, "Map not working????", Toast.LENGTH_LONG)
					.show();
		}
	}

	private void addMarkers() {
		if (googleMap != null) {
			for (int markerCount = 0; markerCount < maproute.size(); markerCount++) {
				googleMap.addMarker(new MarkerOptions()
						.position(
								new LatLng(maproute.get(markerCount).getLat(),
										maproute.get(markerCount).getLon()))
						.title(maproute.get(markerCount).getCity())
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.dt)));
				lonlat.add(new LatLng(maproute.get(markerCount).getLat(),
						maproute.get(markerCount).getLon()));
			}
		}
	}

	public void centerIncidentRouteOnMaps(List<LatLng> copiedPoints) {
		double minLat = Integer.MAX_VALUE;
		double maxLat = Integer.MIN_VALUE;
		double minLon = Integer.MAX_VALUE;
		double maxLon = Integer.MIN_VALUE;
		for (LatLng point : copiedPoints) {
			maxLat = Math.max(point.latitude, maxLat);
			minLat = Math.min(point.latitude, minLat);
			maxLon = Math.max(point.longitude, maxLon);
			minLon = Math.min(point.longitude, minLon);
		}
		final LatLngBounds bounds = new LatLngBounds.Builder()
				.include(new LatLng(maxLat, maxLon))
				.include(new LatLng(minLat, minLon)).build();
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,
				metrics.widthPixels, metrics.heightPixels, 50));
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				try {
					MainActivity.mapToast.cancel();
				} catch (Exception e) {
				}
			}
		}, 2000);

		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				MainActivity.mapIsLoading = false;
				if (MainActivity.mapToast.getView().isShown())
					Toast.makeText(context,
							getResources().getString(R.string.check_internet),
							Toast.LENGTH_LONG).show();
			}
		}, Constant.toastLength);

	}
}