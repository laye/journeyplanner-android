/* 
 #Class name: MapPointActivity
 #Class description: class to show the map point location
 */
package com.myanmar.travel.planner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.bugsense.trace.BugSenseHandler;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapPointActivity extends FragmentActivity {
	GoogleMap googleMap;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_main);
		BugSenseHandler.initAndStartSession(MapPointActivity.this, "5d76adb8");
		Intent i = getIntent();
		String lat = i.getExtras().getString("maplat");
		String longt = i.getExtras().getString("maplongt");
		Log.d("maplat", lat);
		Log.d("maplongt", longt);
		String city = getIntent().getStringExtra("mapcity");
		double latitude = Double.parseDouble(lat);
		double longitude = Double.parseDouble(longt);
		/*
		 * open map with specified lat long
		 */
		SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		try {
			googleMap = fm.getMap();
		} catch (Exception e) {
			Toast.makeText(MapPointActivity.this, "Map not working????",
					Toast.LENGTH_LONG).show();
		}

		MainActivity.mapOrListClicked = false;
		if (googleMap != null) {
			googleMap.addMarker(new MarkerOptions().position(
					new LatLng(latitude, longitude)).title(city));
			googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(
					latitude, longitude)));
			googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));

			googleMap.setOnMapLoadedCallback(new OnMapLoadedCallback() {

				@Override
				public void onMapLoaded() {
					googleMap.animateCamera(CameraUpdateFactory
							.zoomBy((float) 0.1));
				}
			});
		} else {
			Toast.makeText(MapPointActivity.this, "Map not working????",
					Toast.LENGTH_LONG).show();
		}
	}
}
