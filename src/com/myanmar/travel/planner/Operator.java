/* 
 #Class name: Operator
 #Class description: class to hold OperatorObject
 */
package com.myanmar.travel.planner;

public class Operator {
	String operatorName;
	String operatorFullName;
	String address;
	int phone;
	String url;
	String image;

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getOperatorFullName() {
		return operatorFullName;
	}

	public void setOperatorFullName(String operatorFullName) {
		this.operatorFullName = operatorFullName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Operator() {
		super();
	}

	public Operator(String operatorName, String operatorFullName,
			String address, int phone, String url, String image) {
		super();
		this.operatorName = operatorName;
		this.operatorFullName = operatorFullName;
		this.address = address;
		this.phone = phone;
		this.url = url;
		this.image = image;
	}
}
