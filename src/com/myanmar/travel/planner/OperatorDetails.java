/* 
 #Class name: OperatorDetails
 #Class description: class to show OperatorDetails
 */
package com.myanmar.travel.planner;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("DefaultLocale")
public class OperatorDetails extends Activity {

	DatabaseMain databaseMain;
	ActionBar mActionBar;
	Agency agency = new Agency();
	ImageView imageViewOperatorLogo, imageViewOperatorPhoneLogo,
			imageViewOperatorEmail, imageViewOperatorUrlLogo;
	TextView textViewOperatorName, textViewOperatorUrl, textViewOperatorPhone,
			textViewOperatorEmail, textViewOperatorAddress;
	String url;
	String[] separatedUrl;
	LinearLayout addressLayout, mailLayout, phoneLayout, urlLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_operator_detail);
		/*
		 * get the passed position
		 */
		String agency_id = getIntent().getExtras().getString("agency_id");
		databaseMain = new DatabaseMain(getApplicationContext());
		agency = databaseMain.getAgencyDetailsWithId(agency_id);
		mActionBar = getActionBar();
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar
				.setTitle(getResources().getString(R.string.operator_details));

		addressLayout = (LinearLayout) findViewById(R.id.addressLayout);
		mailLayout = (LinearLayout) findViewById(R.id.mailLayout);
		phoneLayout = (LinearLayout) findViewById(R.id.phoneLayout);
		urlLayout = (LinearLayout) findViewById(R.id.urlLayout);

		imageViewOperatorLogo = (ImageView) findViewById(R.id.imageViewOperatorLogo);
		imageViewOperatorPhoneLogo = (ImageView) findViewById(R.id.imageViewOperatorPhoneLogo);
		imageViewOperatorUrlLogo = (ImageView) findViewById(R.id.imageViewOperatorUrlLogo);

		imageViewOperatorEmail = (ImageView) findViewById(R.id.imageViewOperatorEmail);
		textViewOperatorName = (TextView) findViewById(R.id.textViewOperatorName);
		textViewOperatorUrl = (TextView) findViewById(R.id.textViewOperatorUrl);
		textViewOperatorEmail = (TextView) findViewById(R.id.textViewOperatorEmail);
		textViewOperatorAddress = (TextView) findViewById(R.id.textViewOperatorAddress);
		textViewOperatorPhone = (TextView) findViewById(R.id.textViewOperatorPhone);

		/*
		 * set image with agency id from resources
		 */
		String agency_id_string = agency.getAgency_id();
		agency_id_string = agency_id_string.toLowerCase();
		Context mcontext = imageViewOperatorLogo.getContext();
		int id = mcontext.getResources().getIdentifier(agency_id_string,
				"drawable", mcontext.getPackageName());
		try {
			imageViewOperatorLogo.setImageResource(id);
		} catch (Exception e) {
		}

		textViewOperatorName.setText(agency.getAgency_name());
		url = "";
		url = agency.getAgency_url();
		if (!url.equals("")) {
			separatedUrl = url.split("/");
			url = separatedUrl[2];
		}
		textViewOperatorUrl.setText(url);
		if (agency.getAgency_url().length() < 3) {
			urlLayout.setVisibility(View.GONE);
		}
		textViewOperatorUrl.setText(url);
		if (agency.getAgency_address().length() < 3) {
			addressLayout.setVisibility(View.GONE);
		}
		textViewOperatorPhone.setText(agency.getAgency_phone());
		if (agency.getAgency_phone().length() < 3) {
			phoneLayout.setVisibility(View.GONE);
		}
		textViewOperatorEmail.setText(agency.getAgency_email());
		if (agency.getAgency_email().length() < 3) {
			mailLayout.setVisibility(View.GONE);
		}
		textViewOperatorAddress.setText(agency.getAgency_address());
		imageViewOperatorEmail.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				Intent intent = new Intent(android.content.Intent.ACTION_SEND);
				intent.setType("message/rfc822");
				intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
				intent.putExtra(Intent.EXTRA_EMAIL,
						new String[] { textViewOperatorEmail.getText()
								.toString() });
				startActivity(Intent.createChooser(intent, "Send Email"));

			}
		});
		textViewOperatorEmail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				imageViewOperatorEmail.performClick();
			}
		});

		imageViewOperatorPhoneLogo.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					/*
					 * open dialer with the number
					 */
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:"
							+ textViewOperatorPhone.getText().toString()));
					startActivity(callIntent);
				} catch (ActivityNotFoundException activityException) {
					Log.e("Calling a Phone Number", "Call failed",
							activityException);
				}
			}
		});
		textViewOperatorPhone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				imageViewOperatorPhoneLogo.performClick();
			}
		});

		imageViewOperatorUrlLogo.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {/*
					 * open url with the address
					 */
					startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri
							.parse("http://"
									+ textViewOperatorUrl.getText().toString())));
				} catch (ActivityNotFoundException activityException) {
					Log.e("Loading URL", "Loading URL failed",
							activityException);
				}
			}
		});

		textViewOperatorUrl.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				imageViewOperatorUrlLogo.performClick();
			}
		});
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		/*
		 * on selected menu item choose what to do
		 */
		int itemId = item.getItemId();
		switch (itemId) {
		case android.R.id.home:
			onBackPressed();
			break;
		}

		return true;
	}
}
