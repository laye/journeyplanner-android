/* 
 #Class name: Operators
 #Class description: class to show list of Operators
 */
package com.myanmar.travel.planner;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Operators extends Activity {
	static int count;
	static RelativeLayout helloLayout, layoutOperator;
	static ListView listViewOperators;
	static Operators mContext;
	TextView textViewIsEmpty;
	static ArrayList<Agency> listAgency = new ArrayList<Agency>();
	DatabaseMain databaseMain;
	static ActionBar mActionBar;
	static ListViewAdapterForOperators lvaAdapterNew;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.operators);
		mContext = this;
		mActionBar = getActionBar();
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle(getResources().getString(R.string.operators));
		listViewOperators = (ListView) findViewById(R.id.listViewOperator);
		helloLayout = (RelativeLayout) findViewById(R.id.favouriteViewHelloLayout1);
		layoutOperator = (RelativeLayout) findViewById(R.id.layoutOperator);
		textViewIsEmpty = (TextView) findViewById(R.id.textViewIsEmpty);
		databaseMain = new DatabaseMain(getApplicationContext());
		listAgency = databaseMain.getAllAgency();
		lvaAdapterNew = new ListViewAdapterForOperators(Operators.this,
				listAgency);
		listViewOperators.setAdapter(lvaAdapterNew);
		mContext = this;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		int itemId = item.getItemId();
		switch (itemId) {
		case android.R.id.home:
			onBackPressed();
			break;
		}
		return true;
	}
}
