/* 
 #Class name: RouteList
 #Class description: class to hold RouteList_item
 */
package com.myanmar.travel.planner;

import java.io.Serializable;

public class RouteList implements Serializable {
	private static final long serialVersionUID = 1L;
	double lat;
	double lon;
	String city;

	public RouteList(double lat, double lon, String city) {
		this.lat = lat;
		this.lon = lon;
		this.city = city;
	}

	public RouteList() {
	}

	public double getLat() {
		return lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(float lon) {
		this.lon = lon;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
