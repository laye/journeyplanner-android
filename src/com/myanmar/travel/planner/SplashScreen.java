/* 
 #Class name: SplashScreen
 #Class description: class to show splashscreen and load the enc
 */

package com.myanmar.travel.planner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class SplashScreen extends Activity {
	FileInputStream file;
	CSVReader reader;
	DatabaseMain databaseMain;
	ImageView imgSplashView;
	ProgressBar progressBar1;
	Stops stops;
	Agency agency;
	Calender calender;
	Routes routes;
	StopTimes stopTimes;
	Trips trips;

	ArrayList<Stops> stopsList = new ArrayList<Stops>();
	ArrayList<Agency> agencyList = new ArrayList<Agency>();
	ArrayList<Calender> calenderList = new ArrayList<Calender>();
	ArrayList<Routes> routesList = new ArrayList<Routes>();
	ArrayList<StopTimes> stopTimesList = new ArrayList<StopTimes>();
	ArrayList<Trips> tripsList = new ArrayList<Trips>();

	boolean dataSet = false;
	int clicked = 0;

	Context mContext;

	boolean agencyLoaded, calenderLoaded, routesLoaded, stoptimesLoaded,
			stopsLoaded, tripsLoaded, allLoaded;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);
		mContext = SplashScreen.this;
		imgSplashView = (ImageView) findViewById(R.id.imgSplashView);
		progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);
		imgSplashView.setClickable(false);
		imgSplashView.postDelayed(new Runnable() {
			@Override
			public void run() {
				imgSplashView.performClick();
			}
		}, 2000); /*
				 * wait for 2 sec
				 */
		imgSplashView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (progressBar1.getVisibility() == View.INVISIBLE) { /*
																	 * check
																	 * progressbar
																	 * visibility
																	 */
					finish();
					if (clicked == 0) {
						startActivity(new Intent(mContext, MainActivity.class));
						clicked++;
					}
				}
			}
		});

		databaseMain = new DatabaseMain(mContext);
		if (isFirstTime()) { /*
							 * check if app is freshly installed
							 */
			new LoadAgency().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			new LoadCalendar()
					.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			new LoadRoutes().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			new LoadStop_times()
					.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			new LoadStops().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			new LoadTrips().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			Log.d("second time", "hello again");
			if (databaseMain.getDataCount() < 1) {
				new LoadAgency()
						.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				new LoadCalendar()
						.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				new LoadRoutes()
						.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				new LoadStop_times()
						.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				new LoadStops()
						.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				new LoadTrips()
						.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			} else {
				progressBar1.setVisibility(View.INVISIBLE);
				imgSplashView.setClickable(true);
				imgSplashView.postDelayed(new Runnable() {
					@Override
					public void run() {
						imgSplashView.performClick();
					}
				}, 2000);
			}
		}
	}

	public boolean isFirstTime() {
		SharedPreferences preferences = getPreferences(MODE_PRIVATE);
		boolean ranBefore = preferences.getBoolean("RanBefore", false);
		if (!ranBefore) {
			SharedPreferences.Editor editor = preferences.edit();
			editor.putBoolean("RanBefore", true);
			editor.commit();
		}
		return !ranBefore;
	}

	class LoadAgency extends AsyncTask<Void, Integer, String> {
		protected void onPreExecute() {
			Log.d("PreExceute", "On pre Exceute......");
			imgSplashView.setClickable(false);
			progressBar1.setVisibility(View.VISIBLE);
		}

		protected String doInBackground(Void... arg0) {
			Log.d("DoINBackGround", "On doInBackground...");

			Decryptor td = new Decryptor();
			td.FileToDecrypt("agency");

			try {

				FileInputStream fis = openFileInput("agency.csv");
				CSVReader reader = new CSVReader(new InputStreamReader(fis));
				String[] nextLine;
				nextLine = reader.readNext();
				while ((nextLine = reader.readNext()) != null) {

					agency = new Agency(nextLine[0], nextLine[1], nextLine[2],
							nextLine[3], nextLine[4], nextLine[5], nextLine[6],
							nextLine[7], nextLine[8]);
					agencyList.add(agency);
					// databaseMain.addAgency(agency);
					System.out.println(nextLine[0] + "  " + nextLine[1] + "  "
							+ nextLine[2] + "  " + nextLine[3] + "  "
							+ nextLine[4] + "  " + nextLine[5] + "  "
							+ nextLine[6] + "  " + nextLine[7] + "  "
							+ nextLine[8]);
				}
				reader.close();
			} catch (Exception e) {
				e.printStackTrace();
			} /*
			 * load the database
			 */
			return "You are at PostExecute";
		}

		protected void onPostExecute(String result) {
			agencyLoaded = true;
			checkIfAllLoaded();
			if (allLoaded == true) {
				new addItems().execute();
			}
		}
	}

	class LoadCalendar extends AsyncTask<Void, Integer, String> {
		protected void onPreExecute() {
			Log.d("PreExceute", "On pre Exceute......");
			imgSplashView.setClickable(false);
			progressBar1.setVisibility(View.VISIBLE);
		}

		protected String doInBackground(Void... arg0) {
			Log.d("DoINBackGround", "On doInBackground...");
			// DatabaseMain databaseMain = new DatabaseMain(mContext);

			Decryptor td = new Decryptor();
			td.FileToDecrypt("calendar");

			try {

				FileInputStream fis = openFileInput("calendar.csv");
				CSVReader reader = new CSVReader(new InputStreamReader(fis));

				String[] nextLine;
				nextLine = reader.readNext();
				while ((nextLine = reader.readNext()) != null) {
					// System.out.println(nextLine[0] + "  " + nextLine[1] +
					// "  "
					// + nextLine[2] + "  " + nextLine[3] + "  " + nextLine[4]
					// + "  " + nextLine[5] + "  " + nextLine[6] + "  "
					// + nextLine[7]);
					calender = new Calender(nextLine[0],
							Integer.parseInt(nextLine[1]),
							Integer.parseInt(nextLine[2]),
							Integer.parseInt(nextLine[3]),
							Integer.parseInt(nextLine[4]),
							Integer.parseInt(nextLine[5]),
							Integer.parseInt(nextLine[6]),
							Integer.parseInt(nextLine[7]), nextLine[8],
							nextLine[9]);
					calenderList.add(calender);
					// databaseMain.addCalender(calender);
				}
				reader.close();
			} catch (Exception e) {
				e.printStackTrace();
			} /*
			 * load the database
			 */
			return "You are at PostExecute";
		}

		protected void onPostExecute(String result) {
			calenderLoaded = true;
			checkIfAllLoaded();
			if (allLoaded == true) {
				new addItems().execute();
			}
		}
	}

	class LoadRoutes extends AsyncTask<Void, Integer, String> {
		protected void onPreExecute() {
			Log.d("PreExceute", "On pre Exceute......");
			imgSplashView.setClickable(false);
			progressBar1.setVisibility(View.VISIBLE);
		}

		protected String doInBackground(Void... arg0) {
			Log.d("DoINBackGround", "On doInBackground...");
			// DatabaseMain databaseMain = new DatabaseMain(mContext);

			String line = null;
			Decryptor td = new Decryptor();
			td.FileToDecrypt("routes");

			try {

				FileInputStream fis = openFileInput("routes.csv");
				CSVReader reader = new CSVReader(new InputStreamReader(fis));

				String[] nextLine;
				nextLine = reader.readNext();
				while ((nextLine = reader.readNext()) != null) {
					routes = new Routes(nextLine[0], nextLine[1], nextLine[2],
							nextLine[3], nextLine[4],
							Integer.parseInt(nextLine[5]), nextLine[6],
							nextLine[7], nextLine[8]);
					routesList.add(routes);
					// databaseMain.addRoutes(routes);
					line = nextLine[0];
					// System.out.println(nextLine[0] + "  " + nextLine[1] +
					// "  "
					// + nextLine[2] + "  " + nextLine[3] + "  " + nextLine[4]
					// + "  " + nextLine[5] + "  " + nextLine[6] + "  "
					// + nextLine[7] + "  " + nextLine[8]);
				}
				reader.close();
			} catch (Exception e) {
				Log.e("nextLine[0]", line);
				e.printStackTrace();
			} /*
			 * load the database
			 */
			return "You are at PostExecute";
		}

		protected void onPostExecute(String result) {
			routesLoaded = true;
			checkIfAllLoaded();
			if (allLoaded == true) {
				new addItems().execute();
			}
		}
	}

	class LoadStop_times extends AsyncTask<Void, Integer, String> {
		protected void onPreExecute() {
			Log.d("PreExceute", "On pre Exceute......");
			imgSplashView.setClickable(false);
			progressBar1.setVisibility(View.VISIBLE);
		}

		protected String doInBackground(Void... arg0) {
			Log.d("DoINBackGround", "On doInBackground...");
			// DatabaseMain databaseMain = new DatabaseMain(mContext);

			Decryptor td = new Decryptor();
			td.FileToDecrypt("stop_times");

			try {

				FileInputStream fis = openFileInput("stop_times.csv");
				CSVReader reader = new CSVReader(new InputStreamReader(fis));

				String[] nextLine;
				nextLine = reader.readNext();
				while ((nextLine = reader.readNext()) != null) {
					// String arrival_time = nextLine[1];
					// String departure_time = nextLine[2];
					//
					// if (arrival_time.length() == 8) {
					// String segments[] = arrival_time.split(":");
					// String minutes = segments[segments.length - 2];
					// int hour = Integer.parseInt(segments[segments.length - 3]
					// .trim());
					// int hr = hour % 24;
					// String hourTime = hr + "";
					// if (hourTime.length() < 2) {
					// hourTime = "0" + hourTime;
					// }
					// arrival_time = hr + ":" + minutes;
					// }
					//
					// if (departure_time.length() == 8) {
					// String segments[] = departure_time.split(":");
					// String minutes = segments[segments.length - 2];
					// int hour = Integer.parseInt(segments[segments.length - 3]
					// .trim());
					// int hr = hour % 24;
					// String hourTime = hr + "";
					// if (hourTime.length() < 2) {
					// hourTime = "0" + hourTime;
					// }
					// departure_time = hr + ":" + minutes;
					// }
					// if (arrival_time.length() == 4) {
					// arrival_time = "0" + arrival_time;
					// }
					// if (departure_time.length() == 4) {
					// departure_time = "0" + departure_time;
					// }

					// System.out.println(nextLine[0] + "  " + nextLine[1] +
					// "  "
					// + nextLine[2] + "  " + nextLine[3] + "  "
					// + nextLine[4] + "  " + nextLine[5] + "  "
					// + nextLine[6] + "  " + nextLine[7] + "  "
					// + nextLine[8]);
					stopTimes = new StopTimes(nextLine[0], nextLine[1],
							nextLine[2], nextLine[3],
							Integer.parseInt(nextLine[4]), nextLine[5],
							nextLine[6], nextLine[7], nextLine[8]);
					stopTimesList.add(stopTimes);
					// databaseMain.addStopTime(stopTimes);

				}
				reader.close();
			} catch (Exception e) {
				e.printStackTrace();
			} /*
			 * load the database
			 */
			return "You are at PostExecute";
		}

		protected void onPostExecute(String result) {
			stoptimesLoaded = true;
			checkIfAllLoaded();
			if (allLoaded == true) {
				new addItems().execute();
			}
		}
	}

	class LoadStops extends AsyncTask<Void, Integer, String> {
		protected void onPreExecute() {
			Log.d("PreExceute", "On pre Exceute......");
			imgSplashView.setClickable(false);
			progressBar1.setVisibility(View.VISIBLE);
		}

		protected String doInBackground(Void... arg0) {
			Log.d("DoINBackGround", "On doInBackground...");
			// DatabaseMain databaseMain = new DatabaseMain(mContext);

			Decryptor td = new Decryptor();
			td.FileToDecrypt("stops");
			String line = null;

			try {

				FileInputStream fis = openFileInput("stops.csv");
				CSVReader reader = new CSVReader(new InputStreamReader(fis));

				String[] nextLine;
				nextLine = reader.readNext();
				while ((nextLine = reader.readNext()) != null) {

					stops = new Stops(nextLine[0], nextLine[1], nextLine[2],
							nextLine[3], Float.parseFloat(nextLine[4]),
							Float.parseFloat(nextLine[5]), nextLine[6],
							nextLine[7], Integer.parseInt(nextLine[8]),
							nextLine[9], nextLine[10], nextLine[11]);
					stopsList.add(stops);
					// databaseMain.addStops(stops);
					line = nextLine[0];
					// System.out.println(nextLine[0] + "  " + nextLine[1] +
					// "  "
					// + nextLine[2] + "  " + nextLine[3] + "  "
					// + nextLine[4] + "  " + nextLine[5] + "  "
					// + nextLine[6] + "  " + nextLine[7] + "  "
					// + nextLine[8] + "  " + nextLine[9] + "  "
					// + nextLine[10] + "  " + nextLine[11]);
				}
				reader.close();
			} catch (Exception e) {
				Log.e("nextLine[0]", line);
				e.printStackTrace();
			} /*
			 * load the database
			 */
			return "You are at PostExecute";
		}

		protected void onPostExecute(String result) {
			stopsLoaded = true;
			checkIfAllLoaded();
			if (allLoaded == true) {
				new addItems().execute();
			}
		}
	}

	class LoadTrips extends AsyncTask<Void, Integer, String> {
		protected void onPreExecute() {
			Log.d("PreExceute", "On pre Exceute......");
			imgSplashView.setClickable(false);
			progressBar1.setVisibility(View.VISIBLE);
		}

		protected String doInBackground(Void... arg0) {
			Log.d("DoINBackGround", "On doInBackground...");
			// DatabaseMain databaseMain = new DatabaseMain(mContext);

			Decryptor td = new Decryptor();
			td.FileToDecrypt("trips");

			try {

				FileInputStream fis = openFileInput("trips.csv");
				CSVReader reader = new CSVReader(new InputStreamReader(fis));

				String[] nextLine;
				nextLine = reader.readNext();
				while ((nextLine = reader.readNext()) != null) {
					trips = new Trips(nextLine[0], nextLine[1], nextLine[2],
							nextLine[3], nextLine[4], nextLine[5], nextLine[6],
							nextLine[7], nextLine[8], nextLine[10]);
					tripsList.add(trips);
					// databaseMain.addTrips(trips);
					// System.out.println(nextLine[0] + "  " + nextLine[1] +
					// "  "
					// + nextLine[2] + "  " + nextLine[3] + "  " + nextLine[4]
					// + "  " + nextLine[5] + nextLine[6] + "  " + nextLine[7]
					// + "  " + nextLine[8] + "  " + nextLine[9] + "  "
					// + nextLine[10]);
				}
				reader.close();
			} catch (Exception e) {
				e.printStackTrace();
			} /*
			 * load the database
			 */
			return "You are at PostExecute";
		}

		protected void onPostExecute(String result) {
			tripsLoaded = true;
			checkIfAllLoaded();
			if (allLoaded == true) {
				new addItems().execute();
			}
		}
	}

	private void checkIfAllLoaded() {
		if (agencyLoaded == true && calenderLoaded == true
				&& routesLoaded == true && stoptimesLoaded == true
				&& stopsLoaded == true && tripsLoaded == true) {
			allLoaded = true;
		}
	}

	class addItems extends AsyncTask<Void, Integer, String> {
		protected void onPreExecute() {
			Log.d("PreExceute", "On pre Exceute......");
			imgSplashView.setClickable(false);
			progressBar1.setVisibility(View.VISIBLE);
		}

		protected String doInBackground(Void... arg0) {
			Log.d("DoINBackGround", "On doInBackground...");
			Log.e("started all", "started");
			databaseMain.addAllAgency(agencyList);
			databaseMain.addAllCalender(calenderList);
			databaseMain.addAllRoutes(routesList);
			databaseMain.addAllStopTime(stopTimesList);
			databaseMain.addAllStops(stopsList);
			databaseMain.addAllTrips(tripsList);
			Log.e("done all", "done");
			return "You are at PostExecute";
		}

		protected void onPostExecute(String result) {
			progressBar1.setVisibility(View.INVISIBLE);
			imgSplashView.setClickable(true);
			imgSplashView.postDelayed(new Runnable() {
				@Override
				public void run() {
					imgSplashView.performClick();
				}
			}, 2000);
			Log.d("", "" + result);
		}
	}

	class Decryptor {
		private String password;
		private InputStream inFile;
		private FileOutputStream outFile;

		public String FileToDecrypt(String filename) {
			{
				// File to decrypt.

				// System.out.println("FileDecryptor " + filename);

				password = "12345678";

				try {
					inFile = getAssets().open(filename + ".enc");
					outFile = openFileOutput(filename + ".csv",
							Context.MODE_PRIVATE);
					// outFile = new FileOutputStream("temp.csv");

					PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
					SecretKeyFactory keyFactory = SecretKeyFactory
							.getInstance("PBEWithMD5AndDES");
					SecretKey passwordKey = keyFactory.generateSecret(keySpec);

					// Read in the previouly stored salt and set the iteration
					// count.

					byte[] salt = new byte[8];
					inFile.read(salt);
					int iterations = 100;

					PBEParameterSpec parameterSpec = new PBEParameterSpec(salt,
							iterations);

					// Create the cipher and initialize it for decryption.

					Cipher cipher = Cipher.getInstance("PBEWithMD5AndDES");
					cipher.init(Cipher.DECRYPT_MODE, passwordKey, parameterSpec);

					byte[] input = new byte[64];
					int bytesRead;
					while ((bytesRead = inFile.read(input)) != -1) {
						byte[] output = cipher.update(input, 0, bytesRead);
						if (output != null)
							outFile.write(output);
					}

					byte[] output = cipher.doFinal();
					if (output != null)
						outFile.write(output);

					inFile.close();
					outFile.flush();
					outFile.close();

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (IllegalBlockSizeException e) {
					e.printStackTrace();
				} catch (BadPaddingException e) {
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					e.printStackTrace();
				} catch (InvalidAlgorithmParameterException e) {
					e.printStackTrace();
				} catch (InvalidKeySpecException e) {
					e.printStackTrace();
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (NoSuchPaddingException e) {
					e.printStackTrace();
				}
				return null;
			}
		}
	}

}
