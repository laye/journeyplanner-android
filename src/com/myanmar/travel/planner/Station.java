/* 
 #Class name: Station
 #Class description: class to hold Station_item
 */
package com.myanmar.travel.planner;

public class Station {
	String stationName;
	String latitude;
	String longitude;
	String city;
	String transitType;

	public Station() {
		super();
	}

	public Station(String stationName, String latitude, String longitude,
			String city, String transitType) {
		super();
		this.stationName = stationName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.city = city;
		this.transitType = transitType;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTransitType() {
		return transitType;
	}

	public void setTransitType(String transitType) {
		this.transitType = transitType;
	}
}
