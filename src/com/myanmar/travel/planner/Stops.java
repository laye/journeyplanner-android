/* 
 #Class name: Stops
 #Class description: class to hold Stops_item
 */

package com.myanmar.travel.planner;

public class Stops {
	String stop_id;
	String stop_code;
	String stop_name;
	String stop_desc;
	double stop_lat;
	double stop_lon;
	String stop_url;
	int location_type;
	String zone_id;
	String parent_station;
	String stop_timezone;
	String wheelchair_boarding;

	public String getStop_id() {
		return this.stop_id;
	}

	public void setStop_id(String stop_id) {
		this.stop_id = stop_id;
	}

	public String getStop_code() {
		return stop_code;
	}

	public void setStop_code(String stop_code) {
		this.stop_code = stop_code;
	}

	public String getStop_name() {
		return stop_name;
	}

	public void setStop_name(String stop_name) {
		this.stop_name = stop_name;
	}

	public String getStop_desc() {
		return stop_desc;
	}

	public void setStop_desc(String stop_desc) {
		this.stop_desc = stop_desc;
	}

	public double getStop_lat() {
		return stop_lat;
	}

	public void setStop_lat(float stop_lat) {
		this.stop_lat = stop_lat;
	}

	public double getStop_lon() {
		return stop_lon;
	}

	public void setStop_lon(float stop_lon) {
		this.stop_lon = stop_lon;
	}

	public String getStop_url() {
		return stop_url;
	}

	public void setStop_url(String stop_url) {
		this.stop_url = stop_url;
	}

	public int getLocation_type() {
		return location_type;
	}

	public void setLocation_type(int location_type) {
		this.location_type = location_type;
	}

	public String getZone_id() {
		return zone_id;
	}

	public void setZone_id(String zone_id) {
		this.zone_id = zone_id;
	}

	public String getParent_station() {
		return parent_station;
	}

	public void setParent_station(String parent_station) {
		this.parent_station = parent_station;
	}

	public String getStop_timezone() {
		return stop_timezone;
	}

	public void setStop_timezone(String stop_timezone) {
		this.stop_timezone = stop_timezone;
	}

	public String getWheelchair_boarding() {
		return wheelchair_boarding;
	}

	public void setWheelchair_boarding(String wheelchair_boarding) {
		this.wheelchair_boarding = wheelchair_boarding;
	}

	public Stops() {
	}

	public Stops(String stop_id, String stop_code, String stop_name,
			String stop_desc, float stop_lat, float stop_lon, String zone_id,
			String stop_url, int location_type, String parent_station,
			String stop_timezone, String wheelchair_boarding) {
		this.stop_id = stop_id;
		this.stop_code = stop_code;
		this.stop_name = stop_name;
		this.stop_desc = stop_desc;
		this.stop_lat = stop_lat;
		this.stop_lon = stop_lon;
		this.zone_id = zone_id;
		this.stop_url = stop_url;
		this.location_type = location_type;
		this.parent_station = parent_station;
		this.stop_timezone = stop_timezone;
		this.wheelchair_boarding = wheelchair_boarding;
	}
}
