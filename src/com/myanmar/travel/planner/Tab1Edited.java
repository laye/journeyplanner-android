package com.myanmar.travel.planner;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Tab1Edited extends Fragment implements OnClickListener {

	static View view;
	static TextView button1textView1, button2textView1, button3textView1,
			button4textView1, button5textView1, button6textView1,
			button7textView1;
	static TextView button1textView2, button2textView2, button3textView2,
			button4textView2, button5textView2, button6textView2,
			button7textView2, noData;
	static TextView button1textView3, button2textView3, button3textView3,
			button4textView3, button5textView3, button6textView3,
			button7textView3;
	static LinearLayout button1, button2, button3, button4, button5, button6,
			button7;
	static ProgressBar progressBar;
	int prevId, currId;
	static ListView lv;
	ArrayList<String> stop_id;
	ArrayList<String> stop_name;
	ArrayList<String> stop_time;
	static ListViewAdapterNew lvaAdapterNew;
	static String dtaenewfrommain;
	DatabaseMain databaseMain;

	private static boolean recheckDone;
	static ArrayList<StopTimes> stringStop1Idlist = new ArrayList<StopTimes>();
	static ArrayList<StopTimes> stringStop2Idlist = new ArrayList<StopTimes>();
	static ArrayList<StopTimes> finallistscheduleobj1 = new ArrayList<StopTimes>();
	static ArrayList<StopTimes> finallistscheduleobj2 = new ArrayList<StopTimes>();
	static List<Calender> calenderListTab2 = new ArrayList<Calender>();
	static ArrayList<Calender> calenderListTab2Copy = new ArrayList<Calender>();

	static ArrayList<String> calenderStartDate = new ArrayList<String>();
	static ArrayList<String> calenderEndDate = new ArrayList<String>();
	private static ArrayList<Integer> dayList = new ArrayList<Integer>();
	String[] days = new String[] { "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY",
			"THURSDAY", "FRIDAY", "SATURDAY" };
	int str1;
	int str2;
	ArrayList<Calender> list_date = new ArrayList<Calender>();
	ArrayList<Calender> list_dates = new ArrayList<Calender>();
	boolean dateExists;

	ArrayList<Trips> finalTripsList = new ArrayList<Trips>();
	ArrayList<Calender> finalCalenderList = new ArrayList<Calender>();
	ArrayList<Routes> routeList = new ArrayList<Routes>();
	ArrayList<Routes> routesDisplayList = new ArrayList<Routes>();
	ArrayList<StopTimes> stopTimeList = new ArrayList<StopTimes>();
	ArrayList<StopTimes> stopTimesDisplayList = new ArrayList<StopTimes>();
	boolean tripIDexists;
	boolean serviceIDexists;
	boolean routeIDexists;
	boolean entryExists;
	ArrayList<Stops> getAirStops = new ArrayList<Stops>();
	ArrayList<String> getAirStopsString = new ArrayList<String>();
	ArrayList<Stops> getAirStopsTOMAIN = new ArrayList<Stops>();
	// private String stringStopNameDest, stringStopNameSrc, stringStop1Id,
	// stringStop2Id;
	static ArrayList<ListClass> listClassArray = new ArrayList<ListClass>();

	boolean destSource;
	String years;
	private static boolean foundSpinner1;
	private int foundSpinner1Count;
	private static boolean foundSpinner2;
	private int foundSpinner2Count;
	private ArrayList<String> nextStops = new ArrayList<String>();
	private static FragmentActivity mcontext;
	static Resources resourceString;
	static LinearLayout buttonPanel;

	private static boolean button1disabled;
	private static boolean button2disabled;
	private static boolean button3disabled;
	private static boolean button4disabled;
	private static boolean button5disabled;
	private static boolean button6disabled;
	private static boolean button7disabled;

	static ArrayList<String> calenderDateArray = new ArrayList<String>();

	private boolean isLoading;
	private int count;

	public static CheckAirRoutes checkAirRoutes1, checkAirRoutes2,
			checkAirRoutes3, checkAirRoutes4, checkAirRoutes5, checkAirRoutes6,
			checkAirRoutes7;

	// static ProgressDialog dialog;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mcontext = getActivity();
		view = inflater.inflate(R.layout.tab1edited, container, false);

		buttonPanel = (LinearLayout) view.findViewById(R.id.buttonPanel);

		resourceString = getResources();
		databaseMain = new DatabaseMain(mcontext);

		finalTripsList = new ArrayList<Trips>();
		finalTripsList = databaseMain.getAllTrips("R0A");
		finalCalenderList = new ArrayList<Calender>();
		finalCalenderList = databaseMain.getAllCalender();
		noData = (TextView) view.findViewById(R.id.noData);
		button1textView1 = (TextView) view.findViewById(R.id.button1textView1);
		button2textView1 = (TextView) view.findViewById(R.id.button2textView1);
		button3textView1 = (TextView) view.findViewById(R.id.button3textView1);
		button4textView1 = (TextView) view.findViewById(R.id.button4textView1);
		button5textView1 = (TextView) view.findViewById(R.id.button5textView1);
		button6textView1 = (TextView) view.findViewById(R.id.button6textView1);
		button7textView1 = (TextView) view.findViewById(R.id.button7textView1);

		button1textView2 = (TextView) view.findViewById(R.id.button1textView2);
		button2textView2 = (TextView) view.findViewById(R.id.button2textView2);
		button3textView2 = (TextView) view.findViewById(R.id.button3textView2);
		button4textView2 = (TextView) view.findViewById(R.id.button4textView2);
		button5textView2 = (TextView) view.findViewById(R.id.button5textView2);
		button6textView2 = (TextView) view.findViewById(R.id.button6textView2);
		button7textView2 = (TextView) view.findViewById(R.id.button7textView2);

		button1textView3 = (TextView) view.findViewById(R.id.button1textView3);
		button2textView3 = (TextView) view.findViewById(R.id.button2textView3);
		button3textView3 = (TextView) view.findViewById(R.id.button3textView3);
		button4textView3 = (TextView) view.findViewById(R.id.button4textView3);
		button5textView3 = (TextView) view.findViewById(R.id.button5textView3);
		button6textView3 = (TextView) view.findViewById(R.id.button6textView3);
		button7textView3 = (TextView) view.findViewById(R.id.button7textView3);

		button1 = (LinearLayout) view.findViewById(R.id.button1);
		button2 = (LinearLayout) view.findViewById(R.id.button2);
		button3 = (LinearLayout) view.findViewById(R.id.button3);
		button4 = (LinearLayout) view.findViewById(R.id.button4);
		button5 = (LinearLayout) view.findViewById(R.id.button5);
		button6 = (LinearLayout) view.findViewById(R.id.button6);
		button7 = (LinearLayout) view.findViewById(R.id.button7);

		count = 0;
		nextStops.add("Going to...");
		noData.setText("");
		lv = (ListView) view.findViewById(R.id.listOfAir);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBarAir);
		progressBar.setClickable(false);
		stop_id = new ArrayList<String>();
		stop_name = new ArrayList<String>();
		stop_time = new ArrayList<String>();

		getAirStops = databaseMain.getAirStops();

		for (int stopCounts = 0; stopCounts < getAirStops.size(); stopCounts++) {
			if (!getAirStopsString.contains(getAirStops.get(stopCounts)
					.getStop_desc())) {
				getAirStopsString.add(getAirStops.get(stopCounts)
						.getStop_desc());
			}
		}

		MainActivity.setSpinner1(getAirStopsString);

		for (int counterSpinner1 = 0; counterSpinner1 < getAirStopsString
				.size(); counterSpinner1++) {
			if (getAirStopsString.get(counterSpinner1).equals(
					MainActivity.spinnerOneStation)) {
				// Log.e("getAirStopsString.get(counterSpinner1)",
				// getAirStopsString.get(counterSpinner1));
				// Log.e("MainActivity.spinnerOneStation",
				// MainActivity.spinnerOneStation);
				foundSpinner1 = true;
				foundSpinner1Count = counterSpinner1;
				break;
			} else {
				foundSpinner1 = false;
			}
		}

		if (foundSpinner1) {
			MainActivity.spinner1.setSelection(foundSpinner1Count);
		} else {
			MainActivity.spinner1.setSelection(0);
		}

		if (MainActivity.spinnerOnePosition != 0) {
			if (foundSpinner2 == false) {
				progressBarVisibleAndButtonFalse();
				nextStops = MainActivity.findtopsStartPoint(
						MainActivity.spinnerOneStation, "STA");
				if (nextStops.size() == 1
						&& MainActivity.spinnerOnePosition != 0) {
					nextStops.clear();
					nextStops.add("No destinations exist");
				}
				MainActivity.setSpinner2(nextStops);
				MainActivity.spinner2.setEnabled(true);

				for (int counterSpinner1 = 0; counterSpinner1 < nextStops
						.size(); counterSpinner1++) {
					if (nextStops.get(counterSpinner1).equals(
							MainActivity.spinnerTwoStation)) {
						foundSpinner2 = true;
						foundSpinner2Count = counterSpinner1;
						break;
					} else {
						foundSpinner2 = false;
					}
				}

				if (foundSpinner2) {
					MainActivity.spinner2.setSelection(foundSpinner2Count);
					foundSpinner2 = false;
				} else {
					MainActivity.spinner2.setSelection(0);
				}
				progressBarGoneAndButtonTrue(0);
			}
		}

		MainActivity.button1Switch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				callButtonDisabled();

				String swapStation = MainActivity.spinnerOneStation;
				MainActivity.spinnerOneStation = MainActivity.spinnerTwoStation;
				MainActivity.spinnerTwoStation = swapStation;

				for (int counterSpinner1 = 0; counterSpinner1 < getAirStopsString
						.size(); counterSpinner1++) {
					if (getAirStopsString.get(counterSpinner1).equals(
							MainActivity.spinnerOneStation)) {
						// Log.e("getAirStopsString.get(counterSpinner1)",
						// getAirStopsString.get(counterSpinner1));
						// Log.e("MainActivity.spinnerOneStation",
						// MainActivity.spinnerOneStation);
						foundSpinner1 = true;
						foundSpinner1Count = counterSpinner1;
						break;
					} else {
						foundSpinner1 = false;
					}
				}

				if (foundSpinner1) {
					MainActivity.spinner1.setSelection(foundSpinner1Count);
				} else {
					MainActivity.spinner1.setSelection(0);
				}

				// if (!(MainActivity.spinnerOnePosition == 0 &&
				// MainActivity.spinnerTwoPosition == 0)) {
				// if (MainActivity.pressGoSelected == true) {
				//
				// progressBarVisibleAndButtonFalse();
				// if (MainActivity.spinner1.getSelectedItemPosition() == 0
				// || MainActivity.spinner2
				// .getSelectedItemPosition() == 0) {
				// progressBarGoneAndButtonTrue(0);
				// }
				// }
				// listClassArray.clear();
				// Tab2Edited.buttonPanel.setVisibility(View.VISIBLE);
				//
				// int foundpos = 0;
				// for (int i = 0; i < getAirStopsString.size(); i++) {
				// if (getAirStopsString.get(i).equals(
				// MainActivity.spinnerTwoStation)) {
				// foundpos = i;
				// break;
				// }
				// }
				// swap = true;
				// try {
				// MainActivity.spinner1.setSelection(foundpos);
				// } catch (Exception e) {
				// MainActivity.spinner1.setSelection(0);
				// }
				// }
				// progressBarGoneAndButtonTrue(0);
			}
		});
		checkMessage();
		MainActivity.spinner1
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> AdapterView,
							View view1, int position, long arg3) {
						callButtonDisabled();

						// Log.e("MainActivity.spinnerOneStation",
						// MainActivity.spinnerOneStation + " selected");
						// Log.e("MainActivity.spinnerTwoStation",
						// MainActivity.spinnerTwoStation + " selected");

						MainActivity.spinnerOnePosition = position;
						MainActivity.spinnerOneStation = MainActivity.spinner1
								.getItemAtPosition(
										MainActivity.spinnerOnePosition)
								.toString();

						if (foundSpinner2 == false) {
							progressBarVisibleAndButtonFalse();
							nextStops.clear();
							nextStops = MainActivity.findtopsStartPoint(
									MainActivity.spinnerOneStation, "STA");
							if (nextStops.size() == 1
									&& MainActivity.spinnerOnePosition != 0) {
								nextStops.clear();
								nextStops.add("No destinations exist");
							}
							MainActivity.setSpinner2(nextStops);
							progressBarGoneAndButtonTrue(0);
						}

						if (position == 0) {
							MainActivity.spinner2.setEnabled(false);
						} else {
							MainActivity.spinner2.setEnabled(true);
							for (int counterSpinner2 = 0; counterSpinner2 < nextStops
									.size(); counterSpinner2++) {
								if (nextStops.get(counterSpinner2).equals(
										MainActivity.spinnerTwoStation)) {
									foundSpinner2 = true;
									foundSpinner2Count = counterSpinner2;
									break;
								} else {
									foundSpinner2 = false;
								}
							}

							if (foundSpinner2 == true) {
								MainActivity.spinner2
										.setSelection(foundSpinner2Count);
								MainActivity.spinnerTwoPosition = foundSpinner2Count;
								MainActivity.spinnerTwoStation = MainActivity.spinner2
										.getItemAtPosition(foundSpinner2Count)
										.toString();
								foundSpinner2 = false;
							} else {
								MainActivity.spinner2.setSelection(0);
								MainActivity.spinnerTwoPosition = 0;
								MainActivity.spinnerTwoStation = MainActivity.spinner2
										.getItemAtPosition(0).toString();
							}

						}

						listClassArray.clear();
						if (position == 0) {
							if (MainActivity.spinnerTwoPosition == 0) {
								loadAdapter(0);
							} else {
								if (foundSpinner1 == false) {
									loadAdapter(0);
								} else {
									loadAdapter(0);
								}
							}
						} else {
							listClassArray.clear();
							if (MainActivity.spinnerTwoPosition == 0) {
								onClickPressGo();
								loadAdapter(0);
							} else if (MainActivity.spinnerOnePosition == 0) {
								if (foundSpinner1 == false) {
									onClickPressGo();
									loadAdapter(0);
								} else {
									loadAdapter(0);
								}
							} else {
								onClickPressGo();
							}
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> view) {
						listClassArray.clear();
					}
				});

		MainActivity.spinner2
				.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> AdapterView,
							View view2, int position, long arg3) {
						callButtonDisabled();
						if (MainActivity.spinnerTwoPosition != position) {
						}
						MainActivity.spinnerTwoPosition = position;
						MainActivity.spinnerTwoStation = MainActivity.spinner2
								.getItemAtPosition(
										MainActivity.spinnerTwoPosition)
								.toString();
						if (MainActivity.pressGoSelected == true) {
							progressBarVisibleAndButtonFalse();
							if (MainActivity.spinner1.getSelectedItemPosition() == 0
									|| MainActivity.spinner2
											.getSelectedItemPosition() == 0) {
								progressBarGoneAndButtonTrue(0);
							}
						}
						listClassArray.clear();
						if (position == 0) {
							if (MainActivity.spinnerOnePosition == 0) {
								onClickPressGo();
								loadAdapter(0);
							} else {
								if (foundSpinner2 == false) {
									onClickPressGo();
									loadAdapter(0);
								} else {
									onClickPressGo();
									loadAdapter(0);
								}
							}
						} else {
							if (MainActivity.spinnerOnePosition == 0) {
								loadAdapter(0);
							} else if (MainActivity.spinnerTwoPosition == 0) {
								if (foundSpinner2 == false) {
									loadAdapter(0);
								} else {
									loadAdapter(0);
								}
							} else {
								onClickPressGo();
							}
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> view) {
						listClassArray.clear();
					}
				});
		dtaenewfrommain = MainActivity.dtaenew;

		int day = MainActivity.maxDay;/*
									 * Integer.parseInt(MainActivity.maxDay.trim(
									 * ));
									 */
		int month = MainActivity.maxMonth + 1;/*
											 * Integer.parseInt(MainActivity.
											 * currentMonth.trim());
											 */
		int year = MainActivity.maxYear;/*
										 * Integer.parseInt(MainActivity.currentYear
										 * .trim());
										 */

		for (int i = 0; i < calenderListTab2.size(); i++) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(year, month, day);

			String getStart_date = calenderListTab2.get(i).getStart_date();
			Calendar calendarStartDate = Calendar.getInstance();
			calendarStartDate.set(
					Integer.parseInt(getStart_date.substring(0, 3)),
					Integer.parseInt(getStart_date.substring(4, 5)),
					Integer.parseInt(getStart_date.substring(6, 7)));

			String getEnd_date = calenderListTab2.get(i).getEnd_date();
			Calendar calendarEndDate = Calendar.getInstance();
			calendarEndDate.set(Integer.parseInt(getEnd_date.substring(0, 3)),
					Integer.parseInt(getEnd_date.substring(4, 5)),
					Integer.parseInt(getEnd_date.substring(6, 7)));

			if ((calendar.after(calendarStartDate) && calendar
					.before(calendarEndDate))
					|| (calendar.equals(calendarStartDate))
					|| calendar.equals(calendarEndDate)) {
			}
		}
		calenderListTab2.clear();
		calenderListTab2 = calenderListTab2Copy;
		calenderDateArray = new ArrayList<String>();
		for (int i = -3; i < 4; i++) {
			Calendar calendarDateNew = Calendar.getInstance();
			calendarDateNew.set(Calendar.DAY_OF_MONTH, day);
			calendarDateNew.set(Calendar.MONTH, month - 1);
			calendarDateNew.set(Calendar.YEAR, year);
			calendarDateNew.add(Calendar.DATE, i);
			calenderDateArray.add(getDateFormat(mcontext,
					calendarDateNew.get(Calendar.DAY_OF_MONTH) + "/"
							+ (calendarDateNew.get(Calendar.MONTH) + 1) + "/"
							+ calendarDateNew.get(Calendar.YEAR)));
		}

		ArrayList<String> dateArray = new ArrayList<String>();
		ArrayList<String> yearArray = new ArrayList<String>();
		String strings[] = calenderDateArray.get(0).split("/");
		if (strings[strings.length - 3].length() == 4) {
			for (int i = 0; i < calenderDateArray.size(); i++) {
				String segments[] = calenderDateArray.get(i).split("/");
				day = Integer.parseInt(segments[segments.length - 1]);
				month = Integer.parseInt(segments[segments.length - 2]);
				year = Integer.parseInt(segments[segments.length - 3]);
				dateArray.add(month + "/" + day);
				yearArray.add(year + "");
			}
		} else {
			for (int i = 0; i < calenderDateArray.size(); i++) {
				String segments[] = calenderDateArray.get(i).split("/");
				day = Integer.parseInt(segments[segments.length - 3]);
				month = Integer.parseInt(segments[segments.length - 2]);
				year = Integer.parseInt(segments[segments.length - 1]);
				dateArray.add(day + "/" + month);
				yearArray.add(year + "");
			}
		}
		button1textView3.setText(yearArray.get(0));
		button2textView3.setText(yearArray.get(1));
		button3textView3.setText(yearArray.get(2));
		button4textView3.setText(yearArray.get(3));
		button5textView3.setText(yearArray.get(4));
		button6textView3.setText(yearArray.get(5));
		button7textView3.setText(yearArray.get(6));

		button1textView2.setText(dateArray.get(0));
		button2textView2.setText(dateArray.get(1));
		button3textView2.setText(dateArray.get(2));
		button4textView2.setText(dateArray.get(3));
		button5textView2.setText(dateArray.get(4));
		button6textView2.setText(dateArray.get(5));
		button7textView2.setText(dateArray.get(6));

		button1textView1.setText(getdayShort(dayList.get(0)));
		button2textView1.setText(getdayShort(dayList.get(1)));
		button3textView1.setText(getdayShort(dayList.get(2)));
		button4textView1.setText(getdayShort(dayList.get(3)));
		button5textView1.setText(getdayShort(dayList.get(4)));
		button6textView1.setText(getdayShort(dayList.get(5)));
		button7textView1.setText(getdayShort(dayList.get(6)));

		button1.setOnClickListener(this);
		button2.setOnClickListener(this);
		button3.setOnClickListener(this);
		button4.setOnClickListener(this);
		button5.setOnClickListener(this);
		button6.setOnClickListener(this);
		button7.setOnClickListener(this);
		return view;
	}

	private void checkMessage() {
		if (MainActivity.spinner1.getSelectedItemPosition() == 0
				&& MainActivity.spinner2.getSelectedItemPosition() == 0) {
			noData.setText(getResources().getString(
					R.string.select_source_and_destination));
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		if (MainActivity.spinnerOnePosition == 0
				&& MainActivity.spinnerTwoPosition == 0) {
			noData.setText(getResources().getString(
					R.string.select_source_and_destination));
			progressBarGoneAndButtonTrue(0);
		} else if (MainActivity.spinnerOnePosition == 0) {
			noData.setText(getResources().getString(R.string.select_source));
			progressBarGoneAndButtonTrue(0);
		} else if (MainActivity.spinnerTwoPosition == 0) {
			noData.setText(getResources()
					.getString(R.string.select_destination));
			progressBarGoneAndButtonTrue(0);
		} else {
			if (isLoading == false) {
				button1.setClickable(false);
				button2.setClickable(false);
				button3.setClickable(false);
				button4.setClickable(false);
				button5.setClickable(false);
				button6.setClickable(false);
				button7.setClickable(false);
				noData.setText("");
				if (MainActivity.pressGoSelected == false) {
					noData.setText(getResources().getString(R.string.press_go));
				} else {
					progressBarVisibleAndButtonFalse();
					callButtonClickableFalse();
					button1.setBackgroundResource(R.drawable.mybuton);
					button2.setBackgroundResource(R.drawable.mybuton);
					button3.setBackgroundResource(R.drawable.mybuton);
					button4.setBackgroundResource(R.drawable.mybuton);
					button5.setBackgroundResource(R.drawable.mybuton);
					button6.setBackgroundResource(R.drawable.mybuton);
					button7.setBackgroundResource(R.drawable.mybuton);
					switch (v.getId()) {
					case R.id.button1:
						button1.setSelected(true);
						button2.setSelected(false);
						button3.setSelected(false);
						button4.setSelected(false);
						button5.setSelected(false);
						button6.setSelected(false);
						button7.setSelected(false);
						button1.setBackgroundResource(R.drawable.mybutongrey);

						years = button1textView3.getText().toString();
						MainActivity.day_selected = getday(dayList.get(0));
						MainActivity.currentYear = button1textView3.getText()
								.toString();
						MainActivity.currentButton = 1;
						System.out.print("day of button selected "
								+ getday(dayList.get(0)) + "\n");

						new LoadAirRoutes(
								button1textView2.getText().toString(),
								button1textView3.getText().toString())
								.execute();

						break;
					case R.id.button2:
						button1.setSelected(false);
						button2.setSelected(true);
						button3.setSelected(false);
						button4.setSelected(false);
						button5.setSelected(false);
						button6.setSelected(false);
						button7.setSelected(false);
						button2.setBackgroundResource(R.drawable.mybutongrey);

						years = button2textView3.getText().toString();
						MainActivity.day_selected = getday(dayList.get(1));
						MainActivity.currentYear = button2textView3.getText()
								.toString();
						MainActivity.currentButton = 2;

						System.out.print("day of button selected "
								+ getday(dayList.get(1)) + "\n");

						new LoadAirRoutes(
								button2textView2.getText().toString(),
								button2textView3.getText().toString())
								.execute();

						break;
					case R.id.button3:
						button1.setSelected(false);
						button2.setSelected(false);
						button3.setSelected(true);
						button4.setSelected(false);
						button5.setSelected(false);
						button6.setSelected(false);
						button7.setSelected(false);
						button3.setBackgroundResource(R.drawable.mybutongrey);

						years = button3textView3.getText().toString();
						MainActivity.day_selected = getday(dayList.get(2));
						MainActivity.currentYear = button3textView3.getText()
								.toString();
						MainActivity.currentButton = 3;

						System.out.print("day of button selected "
								+ getday(dayList.get(2)) + "\n");

						new LoadAirRoutes(
								button3textView2.getText().toString(),
								button3textView3.getText().toString())
								.execute();

						break;
					case R.id.button4:
						button1.setSelected(false);
						button2.setSelected(false);
						button3.setSelected(false);
						button4.setSelected(true);
						button5.setSelected(false);
						button6.setSelected(false);
						button7.setSelected(false);
						button4.setBackgroundResource(R.drawable.mybutongrey);

						years = button4textView3.getText().toString();
						MainActivity.day_selected = getday(dayList.get(3));
						MainActivity.currentYear = button4textView3.getText()
								.toString();
						MainActivity.currentButton = 4;

						System.out.print("day of button selected "
								+ getday(dayList.get(3)) + "\n");

						new LoadAirRoutes(
								button4textView2.getText().toString(),
								button4textView3.getText().toString())
								.execute();

						break;
					case R.id.button5:
						button1.setSelected(false);
						button2.setSelected(false);
						button3.setSelected(false);
						button4.setSelected(false);
						button5.setSelected(true);
						button6.setSelected(false);
						button7.setSelected(false);
						button5.setBackgroundResource(R.drawable.mybutongrey);

						years = button5textView3.getText().toString();
						MainActivity.day_selected = getday(dayList.get(4));
						MainActivity.currentYear = button5textView3.getText()
								.toString();
						MainActivity.currentButton = 5;

						System.out.print("day of button selected "
								+ getday(dayList.get(4)) + "\n");

						new LoadAirRoutes(
								button5textView2.getText().toString(),
								button5textView3.getText().toString())
								.execute();

						break;
					case R.id.button6:
						button1.setSelected(false);
						button2.setSelected(false);
						button3.setSelected(false);
						button4.setSelected(false);
						button5.setSelected(false);
						button6.setSelected(true);
						button7.setSelected(false);
						button6.setBackgroundResource(R.drawable.mybutongrey);

						years = button6textView3.getText().toString();
						MainActivity.day_selected = getday(dayList.get(5));
						MainActivity.currentYear = button6textView3.getText()
								.toString();
						MainActivity.currentButton = 6;

						System.out.print("day of button selected "
								+ getday(dayList.get(5)) + "\n");

						new LoadAirRoutes(
								button6textView2.getText().toString(),
								button6textView3.getText().toString())
								.execute();

						break;
					case R.id.button7:
						button1.setSelected(false);
						button2.setSelected(false);
						button3.setSelected(false);
						button4.setSelected(false);
						button5.setSelected(false);
						button6.setSelected(false);
						button7.setSelected(true);
						button7.setBackgroundResource(R.drawable.mybutongrey);

						years = button7textView3.getText().toString();
						MainActivity.day_selected = getday(dayList.get(6));
						MainActivity.currentYear = button7textView3.getText()
								.toString();
						MainActivity.currentButton = 7;

						System.out.print("day of button selected "
								+ getday(dayList.get(6)) + "\n");

						new LoadAirRoutes(
								button7textView2.getText().toString(),
								button7textView3.getText().toString())
								.execute();
						break;

					default:
						break;
					}
				}

				MainActivity.textViewForDate.setText(calenderDateArray
						.get(MainActivity.currentButton - 1));
				MainActivity.textViewForViewDate.setText(calenderDateArray
						.get(MainActivity.currentButton - 1));
				// Log.e("currentButton", MainActivity.currentButton + "");
				// Log.e("calenderDateArray",
				// calenderDateArray.get(MainActivity.currentButton - 1)
				// + "");
				setDayMonthAndYear(calenderDateArray
						.get(MainActivity.currentButton - 1));
			}
		}
	}

	private void setDateToTextView(String date, String years) {
		int day = Integer.parseInt(MainActivity.currentDay.trim());
		int month = Integer.parseInt(MainActivity.currentMonth.trim());
		int year = Integer.parseInt(MainActivity.currentYear.trim());
		MainActivity.textViewForViewDate
				.setText(new StringBuilder().append(day).append("/")
						.append(month).append("/").append(year));
	}

	public void getRoutes(String day_selected, String date, String year) {
		stringStop1Idlist = new DatabaseMain(mcontext).getTripSOfRoute(
				MainActivity.spinnerOneStation, day_selected, "STA", date,
				year, 1);

		stringStop1Idlist = filterListOfDataList(stringStop1Idlist, date, 1);

		stringStop2Idlist = new DatabaseMain(mcontext).getTripSOfRoute(
				MainActivity.spinnerTwoStation, day_selected, "STA", date,
				year, 2);

		stringStop2Idlist = filterListOfDataList(stringStop2Idlist, date, 2);
	}

	private ArrayList<StopTimes> filterListOfDataList(
			ArrayList<StopTimes> stop1Idlist, String date, int i) {

		ArrayList<StopTimes> dataListCopy = new ArrayList<StopTimes>();
		String dateConfirm = getDateFormatNew(mcontext, date, years);
		String segments[] = dateConfirm.split("/");

		int day = Integer.parseInt(segments[segments.length - 3]);
		int month = Integer.parseInt(segments[segments.length - 2]);
		int year = Integer.parseInt(segments[segments.length - 1]);

		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day);
		Calendar startdate = Calendar.getInstance();
		Calendar enddate = Calendar.getInstance();
		for (int kal = 0; kal < stop1Idlist.size(); kal++) {
			String serviceId = getServiceIDFromTripID(stop1Idlist.get(kal)
					.getTrip_id());
			if (!serviceId.equals("11")) {
				String a = getStartDateOf(serviceId);
				String b = getEndDateOf(serviceId);
				startdate.set(Integer.parseInt(a.substring(0, 4)),
						Integer.parseInt(a.substring(4, 6)),
						Integer.parseInt(a.substring(6, 8)));

				a = a.trim();

				enddate.set(Integer.parseInt(b.substring(0, 4)),
						Integer.parseInt(b.substring(4, 6)),
						Integer.parseInt(b.substring(6, 8)));

				/*
				 * filter results with the endtime and starttime of the service
				 */
				if (((calendar.after(startdate) && calendar.before(enddate))
						|| (calendar.equals(startdate)) || calendar
							.equals(enddate))) {
					if (i == 2) {
						if (checkExits(stop1Idlist.get(kal), dataListCopy) == false) {
							stop1Idlist.get(kal).setAfterDate(false);
							dataListCopy.add(stop1Idlist.get(kal));
						} else {
							// return dataListCopy;
						}
					} else {
						stop1Idlist.get(kal).setAfterDate(false);
						dataListCopy.add(stop1Idlist.get(kal));
					}
				} else {
					if (i == 2) {
						if (checkExits(stop1Idlist.get(kal), dataListCopy) == false) {
							stop1Idlist.get(kal).setAfterDate(true);
							dataListCopy.add(stop1Idlist.get(kal));
						} else {
							// return dataListCopy;
						}
					} else {
						stop1Idlist.get(kal).setAfterDate(true);
						dataListCopy.add(stop1Idlist.get(kal));
					}
				}
			}
		}
		return dataListCopy;
	}

	private String getStartDateOf(String serviceID) {
		String startDate = null;
		for (int i = 0; i < finalCalenderList.size(); i++) {
			if (finalCalenderList.get(i).getService_id().equals(serviceID)) {
				startDate = finalCalenderList.get(i).getStart_date();
				break;
			}
		}
		return startDate;
	}

	private String getEndDateOf(String serviceID) {
		String endDate = null;
		for (int i = 0; i < finalCalenderList.size(); i++) {
			if (finalCalenderList.get(i).getService_id().equals(serviceID)) {
				endDate = finalCalenderList.get(i).getEnd_date();
				break;
			}
		}
		return endDate;
	}

	private String getServiceIDFromTripID(String trip_id) {
		String serviceID = "11";
		for (int i = 0; i < finalTripsList.size(); i++) {
			if (finalTripsList.get(i).getTrip_id().equals(trip_id)) {
				serviceID = finalTripsList.get(i).getService_id();
				break;
			}
		}
		return serviceID;
	}

	private void filterList(String date, String year) {
		listClassArray = databaseMain.getFilteredDateList(listClassArray, date,
				year);
		lvaAdapterNew = new ListViewAdapterNew(mcontext, listClassArray);
		lv.setAdapter(lvaAdapterNew);
	}

	private static String getday(Integer integer) {
		String dayname = null;
		switch (integer) {
		case 1:
			dayname = "SUNDAY";
			break;
		case 2:
			dayname = "MONDAY";
			break;
		case 3:
			dayname = "TUESDAY";
			break;
		case 4:
			dayname = "WEDNESDAY";
			break;
		case 5:
			dayname = "THURSDAY";
			break;
		case 6:
			dayname = "FRIDAY";
			break;
		case 7:
			dayname = "SATURDAY";

		default:
			break;
		}
		return dayname;
	}

	private static String getdayShort(Integer integer) {
		String dayname = null;
		switch (integer) {
		case 1:
			dayname = "SUN";
			break;
		case 2:
			dayname = "MON";
			break;
		case 3:
			dayname = "TUE";
			break;
		case 4:
			dayname = "WED";
			break;
		case 5:
			dayname = "THU";
			break;
		case 6:
			dayname = "FRI";
			break;
		case 7:
			dayname = "SAT";

		default:
			break;
		}
		return dayname;
	}

	private void doCheckForDisplay(String string, String yearss) {
		Calendar calendar = null;
		calendar = Calendar.getInstance();
		String calendarCheck[] = string.split("/");
		calendar.set(Integer.parseInt(yearss.trim()),
				Integer.parseInt(calendarCheck[calendarCheck.length - 1]),
				Integer.parseInt(calendarCheck[calendarCheck.length - 2]));

		for (int checkCount = 0; checkCount < calenderListTab2.size(); checkCount++) {

			String getStart_date = calenderListTab2.get(checkCount)
					.getStart_date();
			Calendar calendarStartDate = Calendar.getInstance();
			calendarStartDate.set(
					Integer.parseInt(getStart_date.substring(0, 3)),
					Integer.parseInt(getStart_date.substring(4, 5)),
					Integer.parseInt(getStart_date.substring(6, 7)));

			String getEnd_date = calenderListTab2.get(checkCount).getEnd_date();
			Calendar calendarEndDate = Calendar.getInstance();
			calendarEndDate.set(Integer.parseInt(getEnd_date.substring(0, 3)),
					Integer.parseInt(getEnd_date.substring(4, 5)),
					Integer.parseInt(getEnd_date.substring(6, 7)));

			if (calendar.after(calendarStartDate)
					&& calendar.before(calendarEndDate)) {
				if (!list_dates.contains(calenderListTab2.get(checkCount))) {
					list_dates.add(calenderListTab2.get(checkCount));
					dateExists = true;
				}
			}
		}
	}

	public static void setdatenew(String dtaenew) {
		String segmentsdtaenew[] = dtaenew.split("/");
		int day = Integer.parseInt(segmentsdtaenew[segmentsdtaenew.length - 3]);
		int month = Integer
				.parseInt(segmentsdtaenew[segmentsdtaenew.length - 2]);
		int year = Integer
				.parseInt(segmentsdtaenew[segmentsdtaenew.length - 1]);

		dayList.clear();
		Calendar calendar1 = Calendar.getInstance();
		for (int i = (day - 3); i < (day + 4); i++) {
			calendar1.set(Calendar.YEAR, year);
			calendar1.set(Calendar.MONTH, month - 1);
			calendar1.set(Calendar.DATE, i);
			int weekday = calendar1.get(Calendar.DAY_OF_WEEK);
			dayList.add(weekday);
		}
		setDaysToTextView();

		calenderDateArray = new ArrayList<String>();
		for (int i = -3; i < 4; i++) {
			Calendar calendarDateNew = Calendar.getInstance();
			calendarDateNew.set(Calendar.DAY_OF_MONTH, day);
			calendarDateNew.set(Calendar.MONTH, month - 1);
			calendarDateNew.set(Calendar.YEAR, year);
			calendarDateNew.add(Calendar.DATE, i);
			calenderDateArray.add(getDateFormat(mcontext,
					calendarDateNew.get(Calendar.DAY_OF_MONTH) + "/"
							+ (calendarDateNew.get(Calendar.MONTH) + 1) + "/"
							+ calendarDateNew.get(Calendar.YEAR)));
		}

		ArrayList<String> dateArray = new ArrayList<String>();
		ArrayList<String> yearArray = new ArrayList<String>();
		String strings[] = calenderDateArray.get(0).split("/");
		if (strings[strings.length - 3].length() == 4) {
			for (int i = 0; i < calenderDateArray.size(); i++) {
				String segments1[] = calenderDateArray.get(i).split("/");
				day = Integer.parseInt(segments1[segments1.length - 1]);
				month = Integer.parseInt(segments1[segments1.length - 2]);
				year = Integer.parseInt(segments1[segments1.length - 3]);
				dateArray.add(month + "/" + day);
				yearArray.add(year + "");
				// Log.e("dateArray", calenderDateArray.get(i));
				// Log.e("dateArray", month + "/" + day);
				// Log.e("yearArray", year + "");
			}
		} else {
			for (int i = 0; i < calenderDateArray.size(); i++) {
				String segments1[] = calenderDateArray.get(i).split("/");
				day = Integer.parseInt(segments1[segments1.length - 3]);
				month = Integer.parseInt(segments1[segments1.length - 2]);
				year = Integer.parseInt(segments1[segments1.length - 1]);
				dateArray.add(day + "/" + month);
				yearArray.add(year + "");
				// Log.e("dateArray", calenderDateArray.get(i));
				// Log.e("dateArray", day + "/" + month);
				// Log.e("yearArray", year + "");
			}
		}

		button1textView3.setText(yearArray.get(0));
		button2textView3.setText(yearArray.get(1));
		button3textView3.setText(yearArray.get(2));
		button4textView3.setText(yearArray.get(3));
		button5textView3.setText(yearArray.get(4));
		button6textView3.setText(yearArray.get(5));
		button7textView3.setText(yearArray.get(6));

		button1textView2.setText(dateArray.get(0));
		button2textView2.setText(dateArray.get(1));
		button3textView2.setText(dateArray.get(2));
		button4textView2.setText(dateArray.get(3));
		button5textView2.setText(dateArray.get(4));
		button6textView2.setText(dateArray.get(5));
		button7textView2.setText(dateArray.get(6));

		button4.performClick();
	}

	// private static int getDaysOfMonth(int month, int year) {
	// int daysInMonth;
	// if (month == 4 || month == 6 || month == 9 || month == 11) {
	// daysInMonth = 30;
	// } else if (month == 2) {
	// daysInMonth = (year % 4 == 0) ? 29 : 28;
	// } else {
	// daysInMonth = 31;
	// }
	// return daysInMonth;
	// }

	public static void sendList(List<Calender> calenderList, String dtaenew) {
		calenderListTab2 = calenderList;
		dtaenewfrommain = MainActivity.dtaenew;

		int day = Integer.parseInt(MainActivity.currentDay.trim());
		int month = Integer.parseInt(MainActivity.currentMonth.trim());
		int year = Integer.parseInt(MainActivity.currentYear.trim());

		dayList.clear();
		Calendar calendar1 = Calendar.getInstance();
		for (int i = (day - 3); i < (day + 4); i++) {
			calendar1.set(Calendar.YEAR, year);
			calendar1.set(Calendar.MONTH, month - 1);
			calendar1.set(Calendar.DATE, i);
			int weekday = calendar1.get(Calendar.DAY_OF_WEEK);
			dayList.add(weekday);
		}
	}

	public void loadAirSchedule(ArrayList<StopTimes> stringStop1Idlist,
			ArrayList<StopTimes> stringStop2Idlist) {
		if (MainActivity.pressGoSelected == true) {

			progressBarVisibleAndButtonFalse();
			if (MainActivity.spinner1.getSelectedItemPosition() == 0
					|| MainActivity.spinner2.getSelectedItemPosition() == 0) {
				progressBarGoneAndButtonTrue(0);
			}
		}
		finallistscheduleobj1.clear();
		finallistscheduleobj2.clear();
		for (int stoplist1count = 0; stoplist1count < stringStop1Idlist.size(); stoplist1count++) {
			for (int stoplist2count = 0; stoplist2count < stringStop2Idlist
					.size(); stoplist2count++) {
				if ((stringStop1Idlist.get(stoplist1count).getTrip_id()
						.equals(stringStop2Idlist.get(stoplist2count)
								.getTrip_id()))
						&& (stringStop1Idlist.get(stoplist1count)
								.getStop_sequence() < stringStop2Idlist.get(
								stoplist2count).getStop_sequence())) {
					finallistscheduleobj1.add(stringStop1Idlist
							.get(stoplist1count));
					finallistscheduleobj2.add(stringStop2Idlist
							.get(stoplist2count));
				}
			}
		}
		listClassArray.clear();
		ListClass listClass = new ListClass();
		for (int finallistscheduleobj1count = 0; finallistscheduleobj1count < finallistscheduleobj1
				.size(); finallistscheduleobj1count++) {
			listClass = new ListClass(finallistscheduleobj1.get(
					finallistscheduleobj1count).getStop_id(),
					finallistscheduleobj2.get(finallistscheduleobj1count)
							.getStop_id(),
					new DatabaseMain(mcontext)
							.getParentStation(finallistscheduleobj1.get(
									finallistscheduleobj1count).getStop_id()),
					new DatabaseMain(mcontext)
							.getParentStation(finallistscheduleobj2.get(
									finallistscheduleobj1count).getStop_id()),
					finallistscheduleobj1.get(finallistscheduleobj1count)
							.getDeparture_time(), finallistscheduleobj2.get(
							finallistscheduleobj1count).getArrival_time(),
					finallistscheduleobj1.get(finallistscheduleobj1count)
							.getTrip_id(),
					getServiceIDFromTripID(finallistscheduleobj1.get(
							finallistscheduleobj1count).getTrip_id()),
					finallistscheduleobj1.get(finallistscheduleobj1count)
							.getStop_sequence(), finallistscheduleobj2.get(
							finallistscheduleobj1count).getStop_sequence(), "");
			listClass.setAfterDate(finallistscheduleobj1.get(
					finallistscheduleobj1count).isAfterDate());

			listClassArray.add(listClass);
		}
	}

	public static void loadAdapter(int i) {
		lvaAdapterNew = new ListViewAdapterNew(MainActivity.mContext,
				listClassArray);
		lv.setAdapter(lvaAdapterNew);

		if (i == 1) {
			progressBarGoneAndButtonTrue(1);
			callButtonClickableTrue();
		}
	}

	private static void progressBarGoneAndButtonTrue(int i) {
		progressBar.setVisibility(View.GONE);
		MainActivity.button1Switch.setClickable(true);
		MainActivity.mTabHost.setEnabled(true);
		MainActivity.calenderButton.setClickable(true);
		MainActivity.textViewForViewDate.setClickable(true);
		MainActivity.spinner1.setClickable(true);
		MainActivity.spinner2.setClickable(true);
		MainActivity.mTabHost.getTabWidget().getChildTabViewAt(0)
				.setEnabled(true);
		MainActivity.mTabHost.getTabWidget().getChildTabViewAt(1)
				.setEnabled(true);
		MainActivity.mTabHost.getTabWidget().getChildTabViewAt(2)
				.setEnabled(true);
		MainActivity.mTabHost.setClickable(true);
		if (i == 1) {
			if (listClassArray.size() == 0) {
				if (MainActivity.spinner1.getSelectedItemPosition() == 0
						&& MainActivity.spinner2.getSelectedItemPosition() == 0) {
					noData.setText(resourceString
							.getString(R.string.select_source_and_destination));
				} else {
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {

							if (foundSpinner1 == false) {
								noData.setText(mcontext.getResources()
										.getString(R.string.source_not_found));
								loadAdapter(0);
							} else {
								noData.setText(mcontext.getResources()
										.getString(R.string.select_source));
								loadAdapter(0);
							}
							if (foundSpinner2 == false) {
								noData.setText(mcontext.getResources()
										.getString(
												R.string.destination_not_found));
								loadAdapter(0);
							} else {
								noData.setText(mcontext.getResources()
										.getString(R.string.select_destination));
								loadAdapter(0);
							}
							if (MainActivity.spinner1.getSelectedItemPosition() == 0
									&& MainActivity.spinner2
											.getSelectedItemPosition() == 0) {
								noData.setText(resourceString
										.getString(R.string.select_source_and_destination));
								loadAdapter(0);
							}

							if (MainActivity.spinner1.getSelectedItemPosition() != 0
									&& MainActivity.spinner2
											.getSelectedItemPosition() != 0
									&& progressBar.getVisibility() == View.GONE) {
								noData.setText(resourceString
										.getString(R.string.no_routes_found));
								if (!recheckDone) {
									clickButton(MainActivity.currentButton);
									recheckDone = true;
								}
							}
						}
					}, 100);
				}
			} else {
				noData.setText("");
			}
		}
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
	public void spinnerTwoSelect(int position) {
		SpinnersSelected();
	}

	private static void progressBarVisibleAndButtonFalse() {
		progressBar.setVisibility(View.VISIBLE);

		mcontext.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				new Runnable() {
					public void run() {
						noData.setText(" ");
					}
				};
			}
		});
		MainActivity.button1Switch.setClickable(false);
		MainActivity.mTabHost.setEnabled(false);
		MainActivity.calenderButton.setClickable(false);
		MainActivity.textViewForViewDate.setClickable(false);
		MainActivity.spinner1.setClickable(false);
		MainActivity.spinner2.setClickable(false);
		MainActivity.mTabHost.getTabWidget().getChildTabViewAt(0)
				.setEnabled(false);
		MainActivity.mTabHost.getTabWidget().getChildTabViewAt(1)
				.setEnabled(false);
		MainActivity.mTabHost.getTabWidget().getChildTabViewAt(2)
				.setEnabled(false);
		MainActivity.mTabHost.setClickable(false);
	}

	public void SpinnersSelected() {
		if (MainActivity.spinner1.getSelectedItemPosition() != 0
				&& MainActivity.spinner2.getSelectedItemPosition() != 0) {
			if (MainActivity.currentDay == null) {
				button4.performClick();
			}
			if (MainActivity.pressGoSelected == true) {
				progressBarVisibleAndButtonFalse();
				if (MainActivity.spinnerOnePosition == 0
						|| MainActivity.spinnerTwoPosition == 0) {
					progressBarGoneAndButtonTrue(0);
				}
			}
			listClassArray.clear();
			stringStop1Idlist.clear();
			doCheckForDisplay(button4textView2.getText().toString(),
					button4textView3.getText().toString());
			filterList(button4textView2.getText().toString(), button4textView3
					.getText().toString());
			if (!MainActivity.spinnerOneStation
					.equals(MainActivity.spinnerTwoStation)) {
				loadAirSchedule(stringStop1Idlist, stringStop2Idlist);
				destSource = false;
				MainActivity.mActionBar.setTitle("Plan Your Journey");
				MainActivity.helloLayout.removeAllViews();
				MainActivity.helloLayout.removeAllViewsInLayout();
				MainActivity.helloLayout.setVisibility(View.GONE);
				MainActivity.frameLayout.setVisibility(View.VISIBLE);
			} else {
				destSource = true;
			}
		} else
			checkMessage();
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
	public void spinnerOneSelect(int position) {
		SpinnersSelected();
	}

	public static void setDaysToTextView() {
		button1textView1.setText(getdayShort(dayList.get(0)));
		button2textView1.setText(getdayShort(dayList.get(1)));
		button3textView1.setText(getdayShort(dayList.get(2)));
		button4textView1.setText(getdayShort(dayList.get(3)));
		button5textView1.setText(getdayShort(dayList.get(4)));
		button6textView1.setText(getdayShort(dayList.get(5)));
		button7textView1.setText(getdayShort(dayList.get(6)));
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
	private static void clickButton(int currentButton) {
		progressBarVisibleAndButtonFalse();
		switch (currentButton) {
		case 1:
			button1.performClick();
			break;
		case 2:
			button2.performClick();
			break;
		case 3:
			button3.performClick();
			break;
		case 4:
			button4.performClick();
			break;
		case 5:
			button5.performClick();
			break;
		case 6:
			button6.performClick();
			break;
		case 7:
			button7.performClick();
			break;
		default:
			button4.performClick();
			break;
		}
	}

	public class LoadAirRoutes extends AsyncTask<Void, Integer, String> {
		String date, year;

		public LoadAirRoutes(String date, String year) {
			this.date = date;
			this.year = year;
		}

		protected void onPreExecute() {
			isLoading = true;
			count = 0;
			progressBarVisibleAndButtonFalse();
			doCheckForDisplay(date, year);
			setDateToTextView(date, year);
			filterList(date, year);
			noData.setText("");
			recheckDone = false;
		}

		protected String doInBackground(Void... voids) {
			MainActivity.destStopID = getStopIdFromName(
					MainActivity.spinnerTwoStation, "STA");
			getRoutes(MainActivity.day_selected, date, year);
			return "You are at PostExecute";
		}

		private ArrayList<String> getStopIdFromName(String spinnerTwoStation,
				String string) {
			ArrayList<String> stopListings = new ArrayList<String>();
			for (int i = 0; i < getAirStops.size(); i++) {
				if (getAirStops.get(i).getStop_desc().equals(spinnerTwoStation)) {
					stopListings.add(getAirStops.get(i).getStop_id());
				}
			}

			return stopListings;
		}

		protected void onPostExecute(String result) {
			if (!MainActivity.spinnerOneStation
					.equals(MainActivity.spinnerTwoStation)) {
				loadAirSchedule(stringStop1Idlist, stringStop2Idlist);
				destSource = false;
			} else {
				destSource = true;
			}
			MainActivity.helloLayout.removeAllViews();
			MainActivity.helloLayout.removeAllViewsInLayout();
			MainActivity.helloLayout.setVisibility(View.GONE);
			MainActivity.frameLayout.setVisibility(View.VISIBLE);

			callButtonDisabled();
			checkAirRoutes1 = new CheckAirRoutes(button1textView2.getText()
					.toString(), button1textView3.getText().toString(),
					getday(dayList.get(0)), 0);
			checkAirRoutes1.executeOnExecutor(THREAD_POOL_EXECUTOR);
			checkAirRoutes2 = new CheckAirRoutes(button2textView2.getText()
					.toString(), button2textView3.getText().toString(),
					getday(dayList.get(1)), 1);
			checkAirRoutes2.executeOnExecutor(THREAD_POOL_EXECUTOR);
			checkAirRoutes3 = new CheckAirRoutes(button3textView2.getText()
					.toString(), button3textView3.getText().toString(),
					getday(dayList.get(2)), 2);
			checkAirRoutes3.executeOnExecutor(THREAD_POOL_EXECUTOR);
			checkAirRoutes4 = new CheckAirRoutes(button4textView2.getText()
					.toString(), button4textView3.getText().toString(),
					getday(dayList.get(3)), 3);
			checkAirRoutes4.executeOnExecutor(THREAD_POOL_EXECUTOR);
			checkAirRoutes5 = new CheckAirRoutes(button5textView2.getText()
					.toString(), button5textView3.getText().toString(),
					getday(dayList.get(4)), 4);
			checkAirRoutes5.executeOnExecutor(THREAD_POOL_EXECUTOR);
			checkAirRoutes6 = new CheckAirRoutes(button6textView2.getText()
					.toString(), button6textView3.getText().toString(),
					getday(dayList.get(5)), 5);
			checkAirRoutes6.executeOnExecutor(THREAD_POOL_EXECUTOR);
			checkAirRoutes7 = new CheckAirRoutes(button7textView2.getText()
					.toString(), button7textView3.getText().toString(),
					getday(dayList.get(6)), 6);
			checkAirRoutes7.executeOnExecutor(THREAD_POOL_EXECUTOR);

		}
	}

	public class CheckAirRoutes extends AsyncTask<Void, Integer, Boolean> {
		boolean hasRoutes;
		int button;
		String date, year, day_selected;
		String checkStringStopNameDesttringStop1Id;
		String checkStringStopNameDesttringStop2Id;
		ArrayList<StopTimes> checkStringStop1Idlist = new ArrayList<StopTimes>();
		ArrayList<StopTimes> checkStringStop2Idlist = new ArrayList<StopTimes>();
		ArrayList<StopTimes> checkFinallistscheduleobj1 = new ArrayList<StopTimes>();
		ArrayList<StopTimes> checkFinallistscheduleobj2 = new ArrayList<StopTimes>();
		ArrayList<ListClass> checkListClassArray = new ArrayList<ListClass>();

		public CheckAirRoutes(String date, String year, String day_selected,
				int button) {
			this.date = date;
			this.year = year;
			this.day_selected = day_selected;
			this.button = button;
		}

		protected void onPreExecute() {
			doCheckForDisplay(date, year);

			if (button == 0) {
				callButtonDisabled();
				MainActivity.button1Switch.setClickable(false);
				MainActivity.mTabHost.setEnabled(false);
				MainActivity.spinner1.setClickable(false);
				MainActivity.spinner2.setClickable(false);
				MainActivity.mTabHost.getTabWidget().getChildTabViewAt(0)
						.setEnabled(false);
				MainActivity.mTabHost.getTabWidget().getChildTabViewAt(1)
						.setEnabled(false);
				MainActivity.mTabHost.getTabWidget().getChildTabViewAt(2)
						.setEnabled(false);
				MainActivity.mTabHost.setClickable(false);
			}
		}

		protected Boolean doInBackground(Void... voids) {
			if (button == 0) {
				callButtonDisabled();
			}
			checkFinallistscheduleobj1.clear();
			checkFinallistscheduleobj2.clear();
			checkStringStop1Idlist.clear();
			checkStringStop2Idlist.clear();

			if (MainActivity.tabSelectd.equals("Air")) {
				checkStringStop1Idlist = new DatabaseMain(mcontext)
						.getTripSOfRoute(MainActivity.spinnerOneStation,
								day_selected, "STA", date, year, 1);
			}
			if (MainActivity.tabSelectd.equals("Air")) {
				checkStringStop2Idlist = new DatabaseMain(mcontext)
						.getTripSOfRoute(MainActivity.spinnerTwoStation,
								day_selected, "STA", date, year, 2);
			}
			for (int stoplist1count = 0; stoplist1count < checkStringStop1Idlist
					.size(); stoplist1count++) {
				if (MainActivity.tabSelectd.equals("Air")) {
					for (int stoplist2count = 0; stoplist2count < checkStringStop2Idlist
							.size(); stoplist2count++) {
						if (MainActivity.tabSelectd.equals("Air")) {
							if ((checkStringStop1Idlist.get(stoplist1count)
									.getTrip_id().equals(checkStringStop2Idlist
									.get(stoplist2count).getTrip_id()))
									&& (checkStringStop1Idlist.get(
											stoplist1count).getStop_sequence() < checkStringStop2Idlist
											.get(stoplist2count)
											.getStop_sequence())) {
								checkFinallistscheduleobj1
										.add(checkStringStop1Idlist
												.get(stoplist1count));
								checkFinallistscheduleobj2
										.add(checkStringStop2Idlist
												.get(stoplist2count));
							}
						} else {
							break;
						}
					}
				} else {
					break;
				}
			}
			checkListClassArray.clear();
			ListClass listClass = new ListClass();
			for (int finallistscheduleobj1count = 0; finallistscheduleobj1count < checkFinallistscheduleobj1
					.size(); finallistscheduleobj1count++) {
				listClass = new ListClass();
				if (!(getServiceIDFromTripID(checkFinallistscheduleobj1.get(
						finallistscheduleobj1count).getTrip_id()).equals("11"))) {
					checkListClassArray.add(listClass);
				}
			}

			if (checkListClassArray.size() > 0) {
				hasRoutes = true;
			} else {
				hasRoutes = false;
			}
			return hasRoutes;
		}

		protected void onPostExecute(Boolean result) {
			if (!result) {
				switch (button) {
				case 0:
					button1disabled = true;
					break;
				case 1:
					button2disabled = true;
					break;
				case 2:
					button3disabled = true;
					break;
				case 3:
					button4disabled = true;
					break;
				case 4:
					button5disabled = true;
					break;
				case 5:
					button6disabled = true;
					break;
				case 6:
					button7disabled = true;
					break;
				}
			}

			count++;
			if (count > 6) {
				isLoading = false;
				loadAirSchedule(stringStop1Idlist, stringStop2Idlist);
				loadAdapter(1);
			}
		}
	}

	private void callButtonDisabled() {
		button1disabled = false;
		button2disabled = false;
		button3disabled = false;
		button4disabled = false;
		button5disabled = false;
		button6disabled = false;
		button7disabled = false;
	}

	private void callButtonClickableFalse() {
		button1.setClickable(false);
		button2.setClickable(false);
		button3.setClickable(false);
		button4.setClickable(false);
		button5.setClickable(false);
		button6.setClickable(false);
		button7.setClickable(false);
	}

	private static void callButtonClickableTrue() {
		if (button1disabled) {
			button1.setClickable(false);
			button1.setBackgroundResource(R.drawable.disabled);
			button1textView1.setTextColor(Color.parseColor("#D2D2D2"));
			button1textView2.setTextColor(Color.parseColor("#D2D2D2"));
			button1textView3.setTextColor(Color.parseColor("#D2D2D2"));
			button1textView1.setTypeface(null, Typeface.NORMAL);
			button1textView2.setTypeface(null, Typeface.NORMAL);
			button1textView3.setTypeface(null, Typeface.NORMAL);
		} else {
			button1.setClickable(true);
			button1textView1.setTextColor(Color.parseColor("#000000"));
			button1textView2.setTextColor(Color.parseColor("#000000"));
			button1textView3.setTextColor(Color.parseColor("#000000"));
			button1textView1.setTypeface(null, Typeface.BOLD);
			button1textView2.setTypeface(null, Typeface.BOLD);
			button1textView3.setTypeface(null, Typeface.BOLD);
		}
		if (button2disabled) {
			button2.setClickable(false);
			button2.setBackgroundResource(R.drawable.disabled);
			button2textView1.setTextColor(Color.parseColor("#D2D2D2"));
			button2textView2.setTextColor(Color.parseColor("#D2D2D2"));
			button2textView3.setTextColor(Color.parseColor("#D2D2D2"));
			button2textView1.setTypeface(null, Typeface.NORMAL);
			button2textView2.setTypeface(null, Typeface.NORMAL);
			button2textView3.setTypeface(null, Typeface.NORMAL);
		} else {
			button2.setClickable(true);
			button2textView1.setTextColor(Color.parseColor("#000000"));
			button2textView2.setTextColor(Color.parseColor("#000000"));
			button2textView3.setTextColor(Color.parseColor("#000000"));
			button2textView1.setTypeface(null, Typeface.BOLD);
			button2textView2.setTypeface(null, Typeface.BOLD);
			button2textView3.setTypeface(null, Typeface.BOLD);
		}
		if (button3disabled) {
			button3.setClickable(false);
			button3.setBackgroundResource(R.drawable.disabled);
			button3textView1.setTextColor(Color.parseColor("#D2D2D2"));
			button3textView2.setTextColor(Color.parseColor("#D2D2D2"));
			button3textView3.setTextColor(Color.parseColor("#D2D2D2"));
			button3textView1.setTypeface(null, Typeface.NORMAL);
			button3textView2.setTypeface(null, Typeface.NORMAL);
			button3textView3.setTypeface(null, Typeface.NORMAL);
		} else {
			button3.setClickable(true);
			button3textView1.setTextColor(Color.parseColor("#000000"));
			button3textView2.setTextColor(Color.parseColor("#000000"));
			button3textView3.setTextColor(Color.parseColor("#000000"));
			button3textView1.setTypeface(null, Typeface.BOLD);
			button3textView2.setTypeface(null, Typeface.BOLD);
			button3textView3.setTypeface(null, Typeface.BOLD);
		}
		if (button4disabled) {
			button4.setClickable(false);
			button4.setBackgroundResource(R.drawable.disabled);
			button4textView1.setTextColor(Color.parseColor("#D2D2D2"));
			button4textView2.setTextColor(Color.parseColor("#D2D2D2"));
			button4textView3.setTextColor(Color.parseColor("#D2D2D2"));
			button4textView1.setTypeface(null, Typeface.NORMAL);
			button4textView2.setTypeface(null, Typeface.NORMAL);
			button4textView3.setTypeface(null, Typeface.NORMAL);
		} else {
			button4.setClickable(true);
			button4textView1.setTextColor(Color.parseColor("#000000"));
			button4textView2.setTextColor(Color.parseColor("#000000"));
			button4textView3.setTextColor(Color.parseColor("#000000"));
			button4textView1.setTypeface(null, Typeface.BOLD);
			button4textView2.setTypeface(null, Typeface.BOLD);
			button4textView3.setTypeface(null, Typeface.BOLD);
		}
		if (button5disabled) {
			button5.setClickable(false);
			button5.setBackgroundResource(R.drawable.disabled);
			button5textView1.setTextColor(Color.parseColor("#D2D2D2"));
			button5textView2.setTextColor(Color.parseColor("#D2D2D2"));
			button5textView3.setTextColor(Color.parseColor("#D2D2D2"));
			button5textView1.setTypeface(null, Typeface.NORMAL);
			button5textView2.setTypeface(null, Typeface.NORMAL);
			button5textView3.setTypeface(null, Typeface.NORMAL);
		} else {
			button5.setClickable(true);
			button5textView1.setTextColor(Color.parseColor("#000000"));
			button5textView2.setTextColor(Color.parseColor("#000000"));
			button5textView3.setTextColor(Color.parseColor("#000000"));
			button5textView1.setTypeface(null, Typeface.BOLD);
			button5textView2.setTypeface(null, Typeface.BOLD);
			button5textView3.setTypeface(null, Typeface.BOLD);
		}
		if (button6disabled) {
			button6.setClickable(false);
			button6.setBackgroundResource(R.drawable.disabled);
			button6textView1.setTextColor(Color.parseColor("#D2D2D2"));
			button6textView2.setTextColor(Color.parseColor("#D2D2D2"));
			button6textView3.setTextColor(Color.parseColor("#D2D2D2"));
			button6textView1.setTypeface(null, Typeface.NORMAL);
			button6textView2.setTypeface(null, Typeface.NORMAL);
			button6textView3.setTypeface(null, Typeface.NORMAL);
		} else {
			button6.setClickable(true);
			button6textView1.setTextColor(Color.parseColor("#000000"));
			button6textView2.setTextColor(Color.parseColor("#000000"));
			button6textView3.setTextColor(Color.parseColor("#000000"));
			button6textView1.setTypeface(null, Typeface.BOLD);
			button6textView2.setTypeface(null, Typeface.BOLD);
			button6textView3.setTypeface(null, Typeface.BOLD);
		}
		if (button7disabled) {
			button7.setClickable(false);
			button7.setBackgroundResource(R.drawable.disabled);
			button7textView1.setTextColor(Color.parseColor("#D2D2D2"));
			button7textView2.setTextColor(Color.parseColor("#D2D2D2"));
			button7textView3.setTextColor(Color.parseColor("#D2D2D2"));
			button7textView1.setTypeface(null, Typeface.NORMAL);
			button7textView2.setTypeface(null, Typeface.NORMAL);
			button7textView3.setTypeface(null, Typeface.NORMAL);
		} else {
			button7.setClickable(true);
			button7textView1.setTextColor(Color.parseColor("#000000"));
			button7textView2.setTextColor(Color.parseColor("#000000"));
			button7textView3.setTextColor(Color.parseColor("#000000"));
			button7textView1.setTypeface(null, Typeface.BOLD);
			button7textView2.setTypeface(null, Typeface.BOLD);
			button7textView3.setTypeface(null, Typeface.BOLD);
		}
		MainActivity.button1Switch.setClickable(true);
		MainActivity.mTabHost.setEnabled(true);
		MainActivity.spinner1.setClickable(true);
		MainActivity.spinner2.setClickable(true);
		MainActivity.mTabHost.getTabWidget().getChildTabViewAt(0)
				.setEnabled(true);
		MainActivity.mTabHost.getTabWidget().getChildTabViewAt(1)
				.setEnabled(true);
		MainActivity.mTabHost.getTabWidget().getChildTabViewAt(2)
				.setEnabled(true);
		MainActivity.mTabHost.setClickable(true);
	}

	public void onClickPressGo() {
		// dialog.show();
		if (isLoading == false) {
			if (!(MainActivity.spinnerOnePosition == 0 && MainActivity.spinnerTwoPosition == 0)) {
				progressBarVisibleAndButtonFalse();
				MainActivity.pressGoSelected = true;
				Tab1Edited.buttonPanel.setVisibility(View.VISIBLE);
				clickButton(MainActivity.currentButton);
				// Log.e("spinner1 stop", MainActivity.spinnerOneStation);
				// Log.e("spinner2 stop", MainActivity.spinnerTwoStation);
				spinnerOneSelect(MainActivity.spinnerOnePosition);
				spinnerTwoSelect(MainActivity.spinnerTwoPosition);
			}
		}
		// dialog.cancel();
	}

	private void setDayMonthAndYear(String string) {
		String dateToBeSent = "";
		Format dateFormat = android.text.format.DateFormat
				.getDateFormat(mcontext);
		String pattern = ((SimpleDateFormat) dateFormat).toLocalizedPattern();
		String segments[] = string.split("/");
		int day = 0, month = 0, year = 0;
		if (pattern.equals("dd/MM/yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("d/M/yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("dd/M/yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("d/MM/yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("MM/dd/yyyy")) {
			month = Integer.parseInt(segments[segments.length - 3]);
			day = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("M/d/yyyy")) {
			month = Integer.parseInt(segments[segments.length - 3]);
			day = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("MM/d/yyyy")) {
			month = Integer.parseInt(segments[segments.length - 3]);
			day = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("M/dd/yyyy")) {
			month = Integer.parseInt(segments[segments.length - 3]);
			day = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("yyyy/MM/dd")) {
			year = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = year + "/" + month + "/" + day;
		} else if (pattern.equals("E, MMM d, yyyy")) {
			month = Integer.parseInt(segments[segments.length - 3]);
			day = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("E, d MMM yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("EE,dd MMM yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("yyyy MMM d, E")) {
			year = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = year + "/" + month + "/" + day;
		} else if (pattern.equals("d/M/y")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("M/d/y")) {
			day = Integer.parseInt(segments[segments.length - 2]);
			month = Integer.parseInt(segments[segments.length - 3]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else {
			Toast.makeText(mcontext, pattern,
					Toast.LENGTH_LONG + Toast.LENGTH_LONG).show();
		}
		MainActivity.currentDay = day + "";
		MainActivity.currentMonth = month + "";
		// MainActivity.currentDate = day + "/" + (month + 1);
		MainActivity.currentYear = year + "";
		MainActivity.maxDayCal = day;
		MainActivity.maxMonthCal = month - 1;
		MainActivity.maxYearCal = year;
		Log.e("currentDay", day + "");
		Log.e("currentMonth", MainActivity.currentMonth + "");
		Log.e("currentYear", year + "");
		Log.e("dateToBeSent", dateToBeSent);
	}

	public static String getDateFormat(Context context, String string) {
		// 25/12/2013
		String dateToBeSent = "";
		Format dateFormat = android.text.format.DateFormat
				.getDateFormat(context);
		String pattern = ((SimpleDateFormat) dateFormat).toLocalizedPattern();
		String segments[] = string.split("/");
		int day = 0, month = 0, year = 0;
		if (pattern.equals("dd/MM/yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("d/M/yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("dd/M/yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("d/MM/yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("MM/dd/yyyy")) {
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("M/d/yyyy")) {
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("MM/d/yyyy")) {
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("M/dd/yyyy")) {
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("yyyy/MM/dd")) {
			year = Integer.parseInt(segments[segments.length - 1]);
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3].trim());
			dateToBeSent = year + "/" + month + "/" + day;
		} else if (pattern.equals("E, MMM d, yyyy")) {
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else if (pattern.equals("E, d MMM yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("EE,dd MMM yyyy")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("yyyy MMM d, E")) {
			year = Integer.parseInt(segments[segments.length - 1]);
			month = Integer.parseInt(segments[segments.length - 2]);
			day = Integer.parseInt(segments[segments.length - 3].trim());
			dateToBeSent = year + "/" + month + "/" + day;
		} else if (pattern.equals("d/M/y")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("M/d/y")) {
			day = Integer.parseInt(segments[segments.length - 3]);
			month = Integer.parseInt(segments[segments.length - 2]);
			year = Integer.parseInt(segments[segments.length - 1].trim());
			dateToBeSent = month + "/" + day + "/" + year;
		} else {
			Toast.makeText(mcontext, pattern,
					Toast.LENGTH_LONG + Toast.LENGTH_LONG).show();
		}

		return dateToBeSent;
	}

	private boolean checkExits(StopTimes stopTimessss,
			ArrayList<StopTimes> dataListCopy) {
		/*
		 * check if entry already exists
		 */
		boolean exits = false;
		for (int checklist = 0; checklist < dataListCopy.size(); checklist++) {
			if (stopTimessss
					.getTrip_id()
					.toString()
					.trim()
					.equalsIgnoreCase(
							dataListCopy.get(checklist).getTrip_id().toString()
									.trim())
					&& stopTimessss.getStop_sequence() == dataListCopy.get(
							checklist).getStop_sequence()) {
				return true;
			} else {
				exits = false;
			}
		}
		return exits;
	}

	public static String getDateFormatNew(Context context, String date,
			String years) {
		// 25/12/2013
		String dateToBeSent = "";
		Format dateFormat = android.text.format.DateFormat
				.getDateFormat(context);
		String pattern = ((SimpleDateFormat) dateFormat).toLocalizedPattern();
		// Log.e("datepattern", pattern);
		String dateArray[] = date.split("/");
		int day = 0, month = 0, year = 0;
		if (pattern.equals("dd/MM/yyyy")) {
			day = Integer.parseInt(dateArray[dateArray.length - 2]);
			month = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("d/MM/yyyy")) {
			day = Integer.parseInt(dateArray[dateArray.length - 2]);
			month = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("dd/M/yyyy")) {
			day = Integer.parseInt(dateArray[dateArray.length - 2]);
			month = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("d/M/yyyy")) {
			day = Integer.parseInt(dateArray[dateArray.length - 2]);
			month = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("MM/dd/yyyy")) {
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("M/dd/yyyy")) {
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("MM/d/yyyy")) {
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("M/d/yyyy")) {
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("M/d/y")) {
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("yyyy/MM/dd")) {
			year = Integer.parseInt(years.trim());
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("E, MMM d, yyyy")) {
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("E, d MMM yyyy")) {
			day = Integer.parseInt(dateArray[dateArray.length - 2]);
			month = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("EE,dd MMM yyyy")) {
			day = Integer.parseInt(dateArray[dateArray.length - 2]);
			month = Integer.parseInt(dateArray[dateArray.length - 1]);
			year = Integer.parseInt(years.trim());
			dateToBeSent = day + "/" + month + "/" + year;
		} else if (pattern.equals("yyyy MMM d, E")) {
			year = Integer.parseInt(years.trim());
			month = Integer.parseInt(dateArray[dateArray.length - 2]);
			day = Integer.parseInt(dateArray[dateArray.length - 1].trim());
			dateToBeSent = day + "/" + month + "/" + year;
		}

		return dateToBeSent;
	}

}
